import 'package:flutter/material.dart';
import 'package:paytalk_mobile/common/strings.dart';
import 'package:paytalk_mobile/views/pages/auth/enrolment.dart';
import 'package:paytalk_mobile/views/pages/invitation.dart';
import 'package:paytalk_mobile/views/pages/splash.dart';
import 'package:paytalk_mobile/views/pages/home.dart';
import 'package:provider/provider.dart';
import 'package:paytalk_mobile/models/user_data.dart';
import 'package:paytalk_mobile/views/pages/help.dart';

import 'common/theme_config.dart';
import 'package:paytalk_mobile/views/pages/auth/sign_in_page.dart';
import 'package:paytalk_mobile/views/pages/settings.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => UserData(),
      child: MaterialApp(
        title: AppStrings.appName,
        theme: ThemeConfig.lightTheme,
        debugShowCheckedModeBanner: false,
        home: Splash(),
        routes: <String, WidgetBuilder>{
          '/home': (BuildContext context) => Home(),
          '/invitation': (BuildContext context) => InvitationPage(),
          '/help': (BuildContext context) => HelpPage(),
          '/login': (BuildContext context) => SignInPage(),
          '/settings': (BuildContext context) => SettingsPage(),
        },
      ),
    );
  }
}
