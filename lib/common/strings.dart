class AppStrings {
  static String appName = 'PayTalk™';
  static String createProfileUrl =
      'https://us-central1-paytalk-429bf.cloudfunctions.net/app/speaker/independent/create-profile';
  static String voiceEnrolmentUrl =
      'https://us-central1-paytalk-429bf.cloudfunctions.net/app/speaker/independent/create-enrollment';

  static String voicePayUrl =
      'https://us-central1-paytalk-429bf.cloudfunctions.net/app/pay';

  static String speechPayUrl =
      'https://us-central1-paytalk-429bf.cloudfunctions.net/app/pay/novoice';

  static String googlespeechUrl =
      'https://us-central1-paytalk-429bf.cloudfunctions.net/app/gspeech';

  static String voiceVerifyUrl =
      'https://us-central1-paytalk-429bf.cloudfunctions.net/app/speaker/independent/verify-profile';

  static String dialogFlowUrl =
      'https://us-central1-paytalk-429bf.cloudfunctions.net/dialogflowGateway';

  static String recipientsUrl =
      'https://us-central1-paytalk-429bf.cloudfunctions.net/app/pay/get-users';

  //ENGLISH WORDS

  static String done = "DONE";
  static String speakerPrompt =
      '\nPress the DONE button when you finish speaking';
}
