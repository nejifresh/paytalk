import 'package:flutter/material.dart';

class AppIll {
  static const slider1 = 'assets/images/slider1.svg';
  static const slider2 = 'assets/images/slider2.svg';
  static const slider3 = 'assets/images/slider3.svg';

  static const invitation = 'assets/images/invitation.svg';
  static const addHome = 'assets/images/add_home.svg';
  static const viewHome = 'assets/images/view_home.svg';
  static const settings = 'assets/images/settings.svg';
  static const lifeRing = 'assets/images/life_ring.svg';
  static const logout = 'assets/images/logout.svg';

  static const invite = 'assets/images/invite.svg';
  static const invitationSvg = 'assets/images/invitationsvg.svg';
  static const woman = 'assets/images/woman.jpg';
}

class AppAnim {
  static const success = 'assets/anim/success.json';
  static const fingerprint = 'assets/anim/fingerprint.json';
}

class Utils {
  static Widget verticalSpacer({double space = 20.0}) {
    return SizedBox(height: space);
  }

  static Widget horizontalSpacer({double space = 20.0}) {
    return SizedBox(width: space);
  }
}

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  @override
  Size get preferredSize => Size.fromHeight(55);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      iconTheme: IconThemeData(
        color: Colors.black,
      ),
    );
  }
}

class CustomColors {
  static const primaryColor = const Color(0xFF845EC2);
  static const primaryLightColor = const Color(0xFFF5F6F8);
  static const primaryDarkColor = const Color(0xFF515C6F);

  static const statusBarColor = Colors.white;
  static const appBarColor = primaryLightColor;
  static const scaffoldColor = primaryLightColor;

  static const headerColor = primaryDarkColor;

  // Form
  static const formIconColor = const Color(0xFF727C8E);
  static const formLabelTextColor = const Color(0xFFA8ADB7);
}

enum HeaderTextType { big, small }

class HeaderText extends StatelessWidget {
  final String text;
  final HeaderTextType type;

  const HeaderText({
    Key key,
    @required this.text,
    this.type = HeaderTextType.big,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final fontSize = this.type == HeaderTextType.big ? 30.0 : 15.0;
    final fontWeight =
        this.type == HeaderTextType.big ? FontWeight.w900 : FontWeight.w600;
    final color = this.type == HeaderTextType.big
        ? Colors.black.withOpacity(0.7)
        : Colors.grey.withOpacity(0.8);

    return Text(
      text,
      style: TextStyle(
        fontSize: fontSize,
        fontWeight: fontWeight,
        color: color,
      ),
    );
  }
}
