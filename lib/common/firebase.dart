import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';

GoogleSignIn googleSignIn = GoogleSignIn(scopes: ['email', 'profile']);
FirebaseAuth firebaseAuth = FirebaseAuth.instance;
FirebaseFirestore firestore = FirebaseFirestore.instance;

// firebase collection refs
CollectionReference usersRef = firestore.collection("users");
CollectionReference transactionRef = firestore.collection("transactions");
CollectionReference configRef = firestore.collection("config");
CollectionReference orgRef = firestore.collection("charities");
CollectionReference voteRef = firestore.collection("candidates");
