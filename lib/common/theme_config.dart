import 'package:flutter/material.dart';
import 'hex_color.dart';

class ThemeConfig {
  //Colors for theme
  static Color lightPrimary = Color(0xfffcfcff);
  static Color darkPrimary = Color(0xff1f1f1f);
  static Color lightAccent = Colors.blue;
  static Color darkAccent = Colors.blue;
  static Color lightBG = Color(0xfffcfcff);
  static Color darkBG = Color(0xff121212);
  static const Color secondaryAccent = Color(0xffF7C6A4);
  static Color greenBackground =
      HexColor("#77cd53").withOpacity(0.8); //Colors.green;

  //borrowed themes
  static const Color background = Color(0XFFFFFFFF);

  static const Color titleTextColor = const Color(0xff1d2635);
  static const Color subTitleTextColor = const Color(0xff797878);

  static const Color lightBlue1 = Color(0xff375efd);
  static const Color lightBlue2 = Color(0xff3554d3);
  static const Color navyBlue1 = Color(0xff15294a);
  static const Color lightNavyBlue = Color(0xff6d7f99);
  static const Color navyBlue2 = Color(0xff2c405b);

  static const Color yellow = Color(0xfffbbd5c);
  static const Color yellow2 = Color(0xffe7ad03);

  static const Color lightGrey = Color(0xfff1f1f3);
  static const Color grey = Color(0xffb9b9b9);
  static const Color darkgrey = Color(0xff625f6a);

  static const Color black = Color(0xff040405);
  static const Color lightblack = Color(0xff3E404D);

  static const white = Colors.white;
  static const red = Colors.red;

  static const Color kPrimaryColor = Color(0XFF23B88B);

  static ThemeData lightTheme = ThemeData(
    backgroundColor: greenBackground,
    fontFamily: "AvenirNextRegular",
    primaryColor: lightPrimary,
    accentColor: lightAccent,
    scaffoldBackgroundColor: lightBG,
    appBarTheme: AppBarTheme(elevation: 0.0),
    textTheme: TextTheme(
      headline6: TextStyle(color: Color(0xff4D4D4D)),
    ),
    textSelectionTheme: TextSelectionThemeData(cursorColor: lightAccent),
    pageTransitionsTheme: PageTransitionsTheme(
      builders: {
        TargetPlatform.android: ZoomPageTransitionsBuilder(),
        TargetPlatform.iOS: CupertinoPageTransitionsBuilder(),
      },
    ),
  );

  static ThemeData darkTheme = ThemeData(
    brightness: Brightness.dark,
    backgroundColor: darkBG,
    fontFamily: "AvenirNextRegular",
    primaryColor: darkPrimary,
    accentColor: darkAccent,
    scaffoldBackgroundColor: darkBG,
    appBarTheme: AppBarTheme(elevation: 0.0),
    textSelectionTheme: TextSelectionThemeData(cursorColor: lightAccent),
    pageTransitionsTheme: PageTransitionsTheme(
      builders: {
        TargetPlatform.android: ZoomPageTransitionsBuilder(),
        TargetPlatform.iOS: CupertinoPageTransitionsBuilder(),
      },
    ),
  );

  static Color profileCardShadowColor = Colors.grey[200];

  static BoxShadow cardShadow = BoxShadow(
    color: Colors.grey[300].withOpacity(0.8),
    blurRadius: 8.0,
    spreadRadius: 0.0,
    offset: Offset(
      0.0,
      2.0,
    ),
  );
}
