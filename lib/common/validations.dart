class Validations {
  static String validateEmail(String value) {
    Pattern pattern =
        r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]"
        r"{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]"
        r"{0,253}[a-zA-Z0-9])?)*$";
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value) || value == null)
      return 'Enter a valid email address';
    else
      return null;
  }

  static String validateName(String value) {
    if (value.length < 3 || value == null)
      return 'Name must be more than 2 charaters';
    else
      return null;
  }

  static String validatePassword(String value) {
    if (value.length < 5 || value == null)
      return 'Password must be more than 5 charaters';
    else
      return null;
  }

  static String validateMobile(String value) {
    String pattern = r'(^([+][1-9])[0-9]{10,12}$)';
    RegExp regExp = new RegExp(pattern);
    if (value.length == 0) {
      return 'Please enter mobile number';
    } else if (!regExp.hasMatch(value)) {
      return 'Please enter a valid mobile number';
    }
    return null;
  }

  static String validatePhone(String value) {
    if (value.length < 7 || value == null)
      return 'Phone number must be more than 7 charaters';
    else
      return null;
  }

  static String validateSMSCode(String value) {
    if (value.length < 2 || value == null)
      return 'Enter SMS Code ';
    else
      return null;
  }
}
