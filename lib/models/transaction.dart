import 'package:cloud_firestore/cloud_firestore.dart';

class Transaction {
  final String senderId;
  final String transactionType; //Donation, Transfer
  final String recipientId;
  final String recipientName;
  final double amount;
  final Timestamp dateOfTransaction;
  final String transactionRef;
  final String orderRef;
  final String transactionId;
  final String command;
  final String currency;
  final String paymentGateWay;
  final String imageUrl;
  final String organizationId;

  Transaction(
      {this.senderId,
      this.transactionType,
      this.recipientId,
      this.amount,
      this.dateOfTransaction,
      this.transactionRef,
      this.command,
      this.currency,
      this.transactionId,
      this.orderRef,
      this.recipientName,
      this.paymentGateWay,
      this.organizationId,
      this.imageUrl});

  Transaction.fromJson(Map<String, dynamic> json)
      : senderId = json['sender'],
        transactionType = json['type'],
        recipientId = json['recipient'],
        amount = json['amount'],
        dateOfTransaction = json['dateOfTransaction'],
        transactionRef = json['tref'],
        command = json['command'],
        currency = json['currency'],
        orderRef = json['orderRef'],
        transactionId = json['transactionId'],
        organizationId = json['organizationId'] ?? null,
        recipientName = json['recipientName'] ?? '',
        imageUrl = json['imageUrl'] ?? '',
        paymentGateWay = json['gateway'];
}
