class VoiceProfile {
  int remainingEnrollmentsSpeechLength;
  String locale;
  String createdDateTime;
  String enrollmentStatus;
  Null modelVersion;
  String profileId;
  Null lastUpdatedDateTime;
  int enrollmentsCount;
  int enrollmentsLength;
  int enrollmentsSpeechLength;

  VoiceProfile(
      {this.remainingEnrollmentsSpeechLength,
      this.locale,
      this.createdDateTime,
      this.enrollmentStatus,
      this.modelVersion,
      this.profileId,
      this.lastUpdatedDateTime,
      this.enrollmentsCount,
      this.enrollmentsLength,
      this.enrollmentsSpeechLength});

  VoiceProfile.fromJson(Map<String, dynamic> json) {
    remainingEnrollmentsSpeechLength = json['remainingEnrollmentsSpeechLength'];
    locale = json['locale'];
    createdDateTime = json['createdDateTime'];
    enrollmentStatus = json['enrollmentStatus'];
    modelVersion = json['modelVersion'];
    profileId = json['profileId'];
    lastUpdatedDateTime = json['lastUpdatedDateTime'];
    enrollmentsCount = json['enrollmentsCount'];
    enrollmentsLength = json['enrollmentsLength'];
    enrollmentsSpeechLength = json['enrollmentsSpeechLength'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['remainingEnrollmentsSpeechLength'] =
        this.remainingEnrollmentsSpeechLength;
    data['locale'] = this.locale;
    data['createdDateTime'] = this.createdDateTime;
    data['enrollmentStatus'] = this.enrollmentStatus;
    data['modelVersion'] = this.modelVersion;
    data['profileId'] = this.profileId;
    data['lastUpdatedDateTime'] = this.lastUpdatedDateTime;
    data['enrollmentsCount'] = this.enrollmentsCount;
    data['enrollmentsLength'] = this.enrollmentsLength;
    data['enrollmentsSpeechLength'] = this.enrollmentsSpeechLength;
    return data;
  }
}
