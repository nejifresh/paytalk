class DonationResponse {
  int amount;
  String command;
  double score;
  List<Charity> charity;

  DonationResponse({this.amount, this.command, this.score, this.charity});

  DonationResponse.fromJson(Map<String, dynamic> json) {
    amount = json['amount'];
    command = json['command'];
    score = json['score'];
    if (json['charity'] != null) {
      charity = new List<Charity>();
      json['charity'].forEach((v) {
        charity.add(new Charity.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['amount'] = this.amount;
    data['command'] = this.command;
    data['score'] = this.score;
    if (this.charity != null) {
      data['charity'] = this.charity.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Charity {
  DateRegistered dateRegistered;
  String imageUrl;
  String name;
  String userId;
  String country;

  Charity(
      {this.dateRegistered,
      this.imageUrl,
      this.name,
      this.userId,
      this.country});

  Charity.fromJson(Map<String, dynamic> json) {
    dateRegistered = json['dateRegistered'] != null
        ? new DateRegistered.fromJson(json['dateRegistered'])
        : null;
    imageUrl = json['image_url'];
    name = json['name'];
    userId = json['userId'];
    country = json['country'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.dateRegistered != null) {
      data['dateRegistered'] = this.dateRegistered.toJson();
    }
    data['image_url'] = this.imageUrl;
    data['name'] = this.name;
    data['userId'] = this.userId;
    data['country'] = this.country;
    return data;
  }
}

class DateRegistered {
  int iSeconds;
  int iNanoseconds;

  DateRegistered({this.iSeconds, this.iNanoseconds});

  DateRegistered.fromJson(Map<String, dynamic> json) {
    iSeconds = json['_seconds'];
    iNanoseconds = json['_nanoseconds'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_seconds'] = this.iSeconds;
    data['_nanoseconds'] = this.iNanoseconds;
    return data;
  }
}
