import 'dart:convert';

class Candidate {
  final String name;
  final String image;
  Candidate({
    this.name,
    this.image,
  });

  Map<String, dynamic> toMap() {
    return {
      'name': name,
      'image': image,
    };
  }

  factory Candidate.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return Candidate(
      name: map['name'],
      image: map['image'],
    );
  }

  String toJson() => json.encode(toMap());

  factory Candidate.fromJson(String source) =>
      Candidate.fromMap(json.decode(source));
}
