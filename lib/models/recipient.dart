class Recipients {
  List<Organizations> organizations;
  List<Users> users;
  num amount;
  String currency;
  String action;

  Recipients(
      {this.organizations,
      this.users,
      this.amount,
      this.currency,
      this.action});

  Recipients.fromJson(
      Map<String, dynamic> json, num amount, String currency, String action) {
    if (json['organizations'] != null) {
      organizations = new List<Organizations>();
      json['organizations'].forEach((v) {
        organizations.add(new Organizations.fromJson(v));
      });
    }
    if (json['users'] != null) {
      users = new List<Users>();
      json['users'].forEach((v) {
        users.add(new Users.fromJson(v));
      });
    }

    amount = amount;
    currency = currency;
    action = action;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.organizations != null) {
      data['organizations'] =
          this.organizations.map((v) => v.toJson()).toList();
    }
    if (this.users != null) {
      data['users'] = this.users.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Organizations {
  String imageUrl;
  //DateRegistered dateRegistered;
  String userId;
  String name;
  String country;
  String category;
  String organizationId;

  Organizations(
      {this.imageUrl,
      //this.dateRegistered,
      this.organizationId,
      this.userId,
      this.name,
      this.category,
      this.country});

  Organizations.fromJson(Map<String, dynamic> json) {
    imageUrl = json['image_url'];
    // dateRegistered = json['dateRegistered'] != null
    //     ? new DateRegistered.fromJson(json['dateRegistered'])
    //     : null;
    userId = json['userId'];
    organizationId = json['organizationId'];
    name = json['name'];
    category = json['category'];
    country = json['country'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['image_url'] = this.imageUrl;
    // if (this.dateRegistered != null) {
    //   data['dateRegistered'] = this.dateRegistered.toJson();
    // }
    data['userId'] = this.userId;
    data['name'] = this.name;
    data['country'] = this.country;
    data['category'] = this.category;
    return data;
  }
}

class DateRegistered {
  int iSeconds;
  int iNanoseconds;

  DateRegistered({this.iSeconds, this.iNanoseconds});

  DateRegistered.fromJson(Map<String, dynamic> json) {
    iSeconds = json['_seconds'];
    iNanoseconds = json['_nanoseconds'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_seconds'] = this.iSeconds;
    data['_nanoseconds'] = this.iNanoseconds;
    return data;
  }
}

class Users {
  String username;
  String email;
  String imageUrl;
  String lastName;
  DateRegistered dateVoiceEnrolled;
  DateRegistered dateJoined;
  bool voiceEnrolled;
  String uuid;
  String country;
  String firstName;
  String currency;
  String voiceId;
  String symbol;
  bool enrolled;

  Users(
      {this.username,
      this.email,
      this.imageUrl,
      this.lastName,
      this.dateVoiceEnrolled,
      this.dateJoined,
      this.voiceEnrolled,
      this.uuid,
      this.country,
      this.firstName,
      this.currency,
      this.voiceId,
      this.symbol,
      this.enrolled});

  Users.fromJson(Map<String, dynamic> json) {
    username = json['username'];
    email = json['email'];
    imageUrl = json['imageUrl'];
    lastName = json['lastName'];
    dateVoiceEnrolled = json['dateVoiceEnrolled'] != null
        ? new DateRegistered.fromJson(json['dateVoiceEnrolled'])
        : null;
    dateJoined = json['dateJoined'] != null
        ? new DateRegistered.fromJson(json['dateJoined'])
        : null;
    voiceEnrolled = json['voiceEnrolled'];
    uuid = json['uuid'];
    country = json['country'];
    firstName = json['firstName'];
    currency = json['currency'];
    voiceId = json['voice_id'];
    symbol = json['symbol'];
    enrolled = json['enrolled'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['username'] = this.username;
    data['email'] = this.email;
    data['imageUrl'] = this.imageUrl;
    data['lastName'] = this.lastName;
    if (this.dateVoiceEnrolled != null) {
      data['dateVoiceEnrolled'] = this.dateVoiceEnrolled.toJson();
    }
    if (this.dateJoined != null) {
      data['dateJoined'] = this.dateJoined.toJson();
    }
    data['voiceEnrolled'] = this.voiceEnrolled;
    data['uuid'] = this.uuid;
    data['country'] = this.country;
    data['firstName'] = this.firstName;
    data['currency'] = this.currency;
    data['voice_id'] = this.voiceId;
    data['symbol'] = this.symbol;
    data['enrolled'] = this.enrolled;
    return data;
  }
}
