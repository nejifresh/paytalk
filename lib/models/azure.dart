class AzureEnrollment {
  int remainingEnrollmentsCount;
  String passPhrase;
  String profileId;
  String enrollmentStatus;
  int enrollmentsCount;
  double enrollmentsLength;
  double enrollmentsSpeechLength;
  double audioLength;
  double audioSpeechLength;
  int status;

  AzureEnrollment(
      {this.remainingEnrollmentsCount,
      this.passPhrase,
      this.profileId,
      this.enrollmentStatus,
      this.enrollmentsCount,
      this.enrollmentsLength,
      this.enrollmentsSpeechLength,
      this.audioLength,
      this.status,
      this.audioSpeechLength});

  AzureEnrollment.fromJson(Map<String, dynamic> json, int statusCode) {
    remainingEnrollmentsCount = json['remainingEnrollmentsCount'];
    passPhrase = json['passPhrase'];
    profileId = json['profileId'];
    enrollmentStatus = json['enrollmentStatus'];
    enrollmentsCount = json['enrollmentsCount'];
    enrollmentsLength = json['enrollmentsLength'];
    enrollmentsSpeechLength = json['enrollmentsSpeechLength'];
    audioLength = json['audioLength'];
    status = statusCode;
    audioSpeechLength = json['audioSpeechLength'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['remainingEnrollmentsCount'] = this.remainingEnrollmentsCount;
    data['passPhrase'] = this.passPhrase;
    data['profileId'] = this.profileId;
    data['enrollmentStatus'] = this.enrollmentStatus;
    data['enrollmentsCount'] = this.enrollmentsCount;
    data['enrollmentsLength'] = this.enrollmentsLength;
    data['enrollmentsSpeechLength'] = this.enrollmentsSpeechLength;
    data['audioLength'] = this.audioLength;
    data['audioSpeechLength'] = this.audioSpeechLength;
    return data;
  }
}

class AzureProfile {
  int remainingEnrollmentsCount;
  String locale;
  String createdDateTime;
  String enrollmentStatus;
  Null modelVersion;
  String profileId;
  Null lastUpdatedDateTime;
  int enrollmentsCount;
  num enrollmentsLength;
  num enrollmentSpeechLength;

  AzureProfile(
      {this.remainingEnrollmentsCount,
      this.locale,
      this.createdDateTime,
      this.enrollmentStatus,
      this.modelVersion,
      this.profileId,
      this.lastUpdatedDateTime,
      this.enrollmentsCount,
      this.enrollmentsLength,
      this.enrollmentSpeechLength});

  AzureProfile.fromJson(Map<String, dynamic> json) {
    remainingEnrollmentsCount = json['remainingEnrollmentsCount'];
    locale = json['locale'];
    createdDateTime = json['createdDateTime'];
    enrollmentStatus = json['enrollmentStatus'];
    modelVersion = json['modelVersion'];
    profileId = json['profileId'];
    lastUpdatedDateTime = json['lastUpdatedDateTime'];
    enrollmentsCount = json['enrollmentsCount'];
    enrollmentsLength = json['enrollmentsLength'];
    enrollmentSpeechLength = json['enrollmentSpeechLength'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['remainingEnrollmentsCount'] = this.remainingEnrollmentsCount;
    data['locale'] = this.locale;
    data['createdDateTime'] = this.createdDateTime;
    data['enrollmentStatus'] = this.enrollmentStatus;
    data['modelVersion'] = this.modelVersion;
    data['profileId'] = this.profileId;
    data['lastUpdatedDateTime'] = this.lastUpdatedDateTime;
    data['enrollmentsCount'] = this.enrollmentsCount;
    data['enrollmentsLength'] = this.enrollmentsLength;
    data['enrollmentSpeechLength'] = this.enrollmentSpeechLength;
    return data;
  }
}

class AzureRecognitionResult {
  String recognitionResult;
  double score;

  AzureRecognitionResult({this.recognitionResult, this.score});

  AzureRecognitionResult.fromJson(Map<String, dynamic> json) {
    recognitionResult = json['recognitionResult'];
    score = json['score'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['recognitionResult'] = this.recognitionResult;
    data['score'] = this.score;
    return data;
  }
}

class AzurePassPhrase {
  String passPhrase;

  AzurePassPhrase({this.passPhrase});

  AzurePassPhrase.fromJson(Map<String, dynamic> json) {
    passPhrase = json['passPhrase'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['passPhrase'] = this.passPhrase;
    return data;
  }
}
