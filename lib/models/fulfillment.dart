class Fulfillment {
  num amount;
  String action;
  bool allRequiredParamsPresent;
  String text;
  String queryText;
  String currency;
  String payInterval;
  String languageCode;
  String recipient;
  String stock;
  String asset;
  num number;
  bool isPurchase;
  bool isFlight;
  String candidate;
  String gameShow;
  bool isVote;
  String startCity;
  String destinationCity;
  Fulfillment(
      {this.amount,
      this.action,
      this.allRequiredParamsPresent,
      this.text,
      this.queryText,
      this.currency,
      this.languageCode,
      this.recipient,
      this.payInterval,
      this.stock,
      this.asset,
      this.candidate,
      this.gameShow,
      this.isVote,
      this.isFlight,
      this.isPurchase,
      this.startCity,
      this.destinationCity,
      this.number});
}
