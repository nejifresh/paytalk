import 'package:cloud_firestore/cloud_firestore.dart';

class PaymentPlan {
  final String planId;
  final String planName;
  final String interval;
  final String organizationId;
  final Timestamp dateCreated;
  PaymentPlan({
    this.planId,
    this.planName,
    this.interval,
    this.dateCreated,
    this.organizationId,
  });

  PaymentPlan.fromJson(Map<String, dynamic> json, String organizationId)
      : planId = json['planId'],
        planName = json['name'],
        interval = json['interval'],
        dateCreated = json['dateCreated'],
        organizationId = organizationId;
}
