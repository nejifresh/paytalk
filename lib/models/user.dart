import 'package:cloud_firestore/cloud_firestore.dart';

class User {
  final String uuid;
  final String email;
  final String imageUrl;
  final Timestamp dateJoined;
  final String firstName;
  final String lastName;
  final bool isEnrolled;
  final bool voiceEnrolled;

  final Timestamp dateVoiceEnrolled;
  final String userName;

  final String accountType;
  final String country;

  final String voiceId;

  final String voiceLanguage;
  final String currency;
  final bool enableVoiceRecognition;

  User(
      {this.uuid,
      this.email,
      this.imageUrl,
      this.dateJoined,
      this.isEnrolled,
      this.accountType,
      this.country,
      this.userName,
      this.voiceId,
      this.voiceLanguage,
      this.firstName,
      this.currency,
      this.voiceEnrolled,
      this.dateVoiceEnrolled,
      this.enableVoiceRecognition,
      this.lastName});

  User.fromMap(Map<dynamic, dynamic> json)
      : uuid = json['uuid'],
        email = json['email'],
        imageUrl = json['imageUrl'],
        dateJoined = json['dateJoined'],
        firstName = json['firstName'] ?? '',
        lastName = json['lastName'] ?? '',
        isEnrolled = json['enrolled'] ?? false,
        userName = json['username'],
        accountType = json['account_type'] ?? 'user',
        country = json['country'] ?? 'Nigeria',
        voiceId = json['voiceId'],
        voiceLanguage = json['voiceLanguage'] ?? "en-US",
        voiceEnrolled = json['voiceEnrolled'] ?? false,
        enableVoiceRecognition = json['enableVoiceRecognition'] ?? false,
        dateVoiceEnrolled = json['dateVoiceEnrolled'],
        currency = json['currency'];
}
