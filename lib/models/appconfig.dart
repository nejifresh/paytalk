class AppConfig {
  String fPublicKey;
  String fEncKey;
  AppConfig({
    this.fPublicKey,
    this.fEncKey,
  });

  AppConfig.fromJson(Map<String, dynamic> json)
      : fPublicKey = json['flutterwave_pubkey'],
        fEncKey = json['flutterwave_encykey'];
}
