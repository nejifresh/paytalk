class VoicePayError {
  String error;
  double score;

  VoicePayError({this.error, this.score});

  VoicePayError.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    score = json['score'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['error'] = this.error;
    data['score'] = this.score;
    return data;
  }
}
