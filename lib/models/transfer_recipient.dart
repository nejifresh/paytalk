import 'package:contacts_service/contacts_service.dart';
import 'package:paytalk_mobile/models/donationresponse.dart';
import 'package:paytalk_mobile/models/voicepayresponse.dart';
import 'package:paytalk_mobile/models/recipient.dart';
import 'package:paytalk_mobile/models/fulfillment.dart';

class TransferRecipient {
  //DonationResponse donationResponse;
  // VoicePayResponse merchantResponse;
  List<Contact> contacts;
  String command;
  Recipients recipients;
  num amount;
  String currency;
  Fulfillment fulfillment;

  TransferRecipient(
      {this.command,
      this.recipients,
      this.contacts,
      this.amount,
      this.fulfillment,
      this.currency});
}
