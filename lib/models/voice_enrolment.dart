class VoiceEnrolment {
  int remainingEnrollmentsSpeechLength;
  String profileId;
  String enrollmentStatus;
  int enrollmentsCount;
  num enrollmentsLength;
  num enrollmentsSpeechLength;
  num audioLength;
  num audioSpeechLength;

  VoiceEnrolment(
      {this.remainingEnrollmentsSpeechLength,
      this.profileId,
      this.enrollmentStatus,
      this.enrollmentsCount,
      this.enrollmentsLength,
      this.enrollmentsSpeechLength,
      this.audioLength,
      this.audioSpeechLength});

  VoiceEnrolment.fromJson(Map<String, dynamic> json) {
    remainingEnrollmentsSpeechLength = json['remainingEnrollmentsSpeechLength'];
    profileId = json['profileId'];
    enrollmentStatus = json['enrollmentStatus'];
    enrollmentsCount = json['enrollmentsCount'];
    enrollmentsLength = json['enrollmentsLength'];
    enrollmentsSpeechLength = json['enrollmentsSpeechLength'];
    audioLength = json['audioLength'];
    audioSpeechLength = json['audioSpeechLength'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['remainingEnrollmentsSpeechLength'] =
        this.remainingEnrollmentsSpeechLength;
    data['profileId'] = this.profileId;
    data['enrollmentStatus'] = this.enrollmentStatus;
    data['enrollmentsCount'] = this.enrollmentsCount;
    data['enrollmentsLength'] = this.enrollmentsLength;
    data['enrollmentsSpeechLength'] = this.enrollmentsSpeechLength;
    data['audioLength'] = this.audioLength;
    data['audioSpeechLength'] = this.audioSpeechLength;
    return data;
  }
}
