class FlightModel {
  final String startCityCode;
  final String startCity;
  final String startDate;
  final String flightDuration;
  final String destinationCityCode;
  final String destinationCity;
  final String arrivalDate;
  final String carrier;
  final num price;
  final String currency;
  FlightModel(
      {this.startCityCode,
      this.startCity,
      this.startDate,
      this.flightDuration,
      this.destinationCityCode,
      this.destinationCity,
      this.arrivalDate,
      this.price,
      this.currency,
      this.carrier});
}
