import 'package:paytalk_mobile/models/recipient.dart';

class PaymentRecipient {
  final Users user;
  final Organizations organization;
  final bool isOrg;
  PaymentRecipient({this.user, this.organization, this.isOrg});
}
