class BankAccount {
  final String accountNumber;
  final String accountName;
  final String bankName;
  final String country;
  final String bankCode;
  BankAccount({
    this.accountNumber,
    this.accountName,
    this.bankName,
    this.bankCode,
    this.country,
  });

  BankAccount.fromMap(Map<String, dynamic> json)
      : accountNumber = json['account_no'],
        accountName = json['account_name'],
        bankName = json['bank'],
        bankCode = json['bankCode'],
        country = json['country'];
}
