class VoicePayResponse {
  int amount;
  List<Merchant> merchant;
  String command;
  double score;

  VoicePayResponse({this.amount, this.merchant, this.command, this.score});

  VoicePayResponse.fromJson(Map<String, dynamic> json) {
    amount = json['amount'];
    if (json['merchant'] != null) {
      merchant = new List<Merchant>();
      json['merchant'].forEach((v) {
        merchant.add(new Merchant.fromJson(v));
      });
    }
    command = json['command'];
    score = json['score'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['amount'] = this.amount;
    if (this.merchant != null) {
      data['merchant'] = this.merchant.map((v) => v.toJson()).toList();
    }
    data['command'] = this.command;
    data['score'] = this.score;
    return data;
  }
}

class Merchant {
  String username;
  String uuid;
  String currency;
  DateJoined dateJoined;
  String firstName;
  String voiceId;
  bool voiceEnrolled;
  String lastName;
  DateJoined dateVoiceEnrolled;
  bool enrolled;
  String country;
  String imageUrl;
  String email;
  String symbol;

  Merchant(
      {this.username,
      this.uuid,
      this.currency,
      this.dateJoined,
      this.firstName,
      this.voiceId,
      this.voiceEnrolled,
      this.lastName,
      this.dateVoiceEnrolled,
      this.enrolled,
      this.country,
      this.imageUrl,
      this.email,
      this.symbol});

  Merchant.fromJson(Map<String, dynamic> json) {
    username = json['username'];
    uuid = json['uuid'];
    currency = json['currency'];
    dateJoined = json['dateJoined'] != null
        ? new DateJoined.fromJson(json['dateJoined'])
        : null;
    firstName = json['firstName'];
    voiceId = json['voice_id'];
    voiceEnrolled = json['voiceEnrolled'];
    lastName = json['lastName'];
    dateVoiceEnrolled = json['dateVoiceEnrolled'] != null
        ? new DateJoined.fromJson(json['dateVoiceEnrolled'])
        : null;
    enrolled = json['enrolled'];
    country = json['country'];
    imageUrl = json['imageUrl'];
    email = json['email'];
    symbol = json['symbol'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['username'] = this.username;
    data['uuid'] = this.uuid;
    data['currency'] = this.currency;
    if (this.dateJoined != null) {
      data['dateJoined'] = this.dateJoined.toJson();
    }
    data['firstName'] = this.firstName;
    data['voice_id'] = this.voiceId;
    data['voiceEnrolled'] = this.voiceEnrolled;
    data['lastName'] = this.lastName;
    if (this.dateVoiceEnrolled != null) {
      data['dateVoiceEnrolled'] = this.dateVoiceEnrolled.toJson();
    }
    data['enrolled'] = this.enrolled;
    data['country'] = this.country;
    data['imageUrl'] = this.imageUrl;
    data['email'] = this.email;
    data['symbol'] = this.symbol;
    return data;
  }
}

class DateJoined {
  int iSeconds;
  int iNanoseconds;

  DateJoined({this.iSeconds, this.iNanoseconds});

  DateJoined.fromJson(Map<String, dynamic> json) {
    iSeconds = json['_seconds'];
    iNanoseconds = json['_nanoseconds'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_seconds'] = this.iSeconds;
    data['_nanoseconds'] = this.iNanoseconds;
    return data;
  }
}
