import 'dart:convert';
import 'package:cloud_firestore/cloud_firestore.dart';

class MySubscriptions {
  final bool active;
  final num amount;
  final Timestamp dateSubscribed;
  final String organizationId;
  final String paymentInterval;
  final String planId;
  final String planName;
  final String transactionId;
  final String currency;
  final String subscriptionId;
  final String imageUrl;
  MySubscriptions({
    this.active,
    this.amount,
    this.dateSubscribed,
    this.organizationId,
    this.paymentInterval,
    this.currency,
    this.planId,
    this.subscriptionId,
    this.planName,
    this.imageUrl,
    this.transactionId,
  });

  Map<String, dynamic> toMap() {
    return {
      'active': active,
      'amount': amount,
      'currency': currency,
      'dateSubscribed': dateSubscribed,
      'organizationId': organizationId,
      'paymentInterval': paymentInterval,
      'planId': planId,
      'subscriptionId': subscriptionId,
      'planName': planName,
      'transactionId': transactionId,
    };
  }

  factory MySubscriptions.fromMap(
      Map<String, dynamic> map, String id, String image) {
    if (map == null) return null;

    return MySubscriptions(
        active: map['active'],
        amount: map['amount'],
        currency: map['currency'],
        dateSubscribed: (map['dateSubscribed']),
        organizationId: map['organizationId'],
        paymentInterval: map['paymentInterval'],
        planId: map['planId'],
        planName: map['planName'],
        subscriptionId: id,
        imageUrl: image,
        transactionId: map['transactionId']);
  }

  String toJson() => json.encode(toMap());

  factory MySubscriptions.fromJson(String source) =>
      MySubscriptions.fromMap(json.decode(source), "", "");
}
