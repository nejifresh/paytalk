import 'package:flutter/material.dart';
import 'package:paytalk_mobile/common/styles.dart';

class MasterButton extends StatelessWidget {
  final String text;
  final Function onPressed;
  final Color color;

  const MasterButton(
      {Key key,
      @required this.text,
      @required this.onPressed,
      @required this.color})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    final btnText = Text(
      text.toUpperCase(),
      style: AppTextStyles.btnTextStyle,
    );

    return MaterialButton(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(5.0),
      ),
      height: 50.0,
      color: color,
      elevation: 4.0,
      onPressed: onPressed,
      child: Center(
        child: btnText,
      ),
    );
  }
}
