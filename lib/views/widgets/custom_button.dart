import 'package:flutter/material.dart';

class CustomButton extends StatelessWidget {
  final String label;
  final double width;
  final Function onTap;
  final double height;

  CustomButton({this.label, this.width, this.onTap, this.height});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height ?? 40.0,
      width: width ?? 140.0,
      child: FlatButton(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0))),
        color: Theme.of(context).backgroundColor,
        onPressed: onTap,
        child: Text(
          '$label',
          style: TextStyle(
            color: Colors.white,
            fontSize: 13.0,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}
