import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:paytalk_mobile/models/donationresponse.dart';

//import 'package:paystack/success_payment_page.dart';
import 'package:paytalk_mobile/models/voicepayresponse.dart';
import 'package:paytalk_mobile/services/database.dart';
import 'package:paytalk_mobile/views/widgets/buttons.dart';
import 'package:recase/recase.dart';
import 'package:flutter/services.dart';
import 'package:rave_flutter/rave_flutter.dart';
import 'package:paytalk_mobile/views/widgets/dialogs.dart';
import 'package:paytalk_mobile/common/navigate.dart';
import 'package:paytalk_mobile/models/user.dart' as appUser;
import 'package:paytalk_mobile/models/user_data.dart';
import 'package:paytalk_mobile/models/paymentplan.dart';
import 'package:paytalk_mobile/models/appconfig.dart';
import 'package:paytalk_mobile/models/transaction.dart' as trans;
import 'package:provider/provider.dart';
import 'package:contacts_service/contacts_service.dart';
import 'package:paytalk_mobile/models/payment_recipient.dart';
import 'package:paytalk_mobile/models/fulfillment.dart';
import 'package:uuid/uuid.dart';

class ConfirmationPage extends StatefulWidget {
  PaymentRecipient recipient;
  num amount;
  Contact contact;
  String currencyName;
  Fulfillment fulfillment;
  ConfirmationPage(
      {this.recipient,
      this.amount,
      this.contact,
      this.currencyName,
      this.fulfillment});

  @override
  _ConfirmationPageState createState() => _ConfirmationPageState();
}

class _ConfirmationPageState extends State<ConfirmationPage> {
  bool acceptCardPayment = true;
  bool acceptAccountPayment = true;
  bool acceptMpesaPayment = false;
  bool shouldDisplayFee = true;
  bool acceptAchPayments = false;
  bool acceptGhMMPayments = false;
  bool acceptUgMMPayments = false;
  bool acceptMMFrancophonePayments = false;
  bool live = false;
  bool preAuthCharge = false;
  bool addSubAccounts = false;
  List<SubAccount> subAccounts = [];
  String _planId;
  PaymentPlan _plan;
  String email;
  double amount;
  //String publicKey = "FLWPUBK_TEST-f080dd9f1959d29a126928b6dfae1169-X";
  //String encryptionKey = "FLWSECK_TESTd5cf77cdc4f2";
  String txRef;
  String orderRef;
  String narration;
  String currency;
  String country;
  String firstName;
  String lastName;
  var scaffoldKey = GlobalKey<ScaffoldState>();

  appUser.User user;
  AppConfig config;

  var uuid = Uuid();

  @override
  void initState() {
    super.initState();
    _getPlanId();
    setState(() {
      txRef = uuid.v4();
      orderRef = uuid.v1();
    });
  }

  _getPlanId() async {
    if (widget.fulfillment != null &&
        widget.fulfillment.payInterval.isNotEmpty) {
      var result = await DatabaseService.getPaymentPlan(
          widget.fulfillment.payInterval,
          widget.recipient.organization.organizationId);
      if (result != null && result.planId.isNotEmpty) {
        print(
            'Payment plan ID is ${result.planId}, interval is ${widget.fulfillment.payInterval}');
        setState(() {
          _planId = result.planId;
          _plan = result;
        });
      }
    }
  }

  void startPayment(String pubkey, String encKey) async {
    var initializer = RavePayInitializer(
        paymentPlan: _planId != null && _planId.isNotEmpty ? _planId : null,
        amount: amount ?? double.parse(widget.amount.toString()),
        publicKey: 'FLWPUBK_TEST-f080dd9f1959d29a126928b6dfae1169-X',
        encryptionKey: 'FLWSECK_TESTd5cf77cdc4f2',
        subAccounts: subAccounts.isEmpty ? null : null)
      ..country =
          country = country != null && country.isNotEmpty ? country : "NG"
      ..currency = widget.currencyName != null && widget.currencyName.isNotEmpty
          ? widget.currencyName
          : user.currency
      ..email = user.email ?? 'info@paytalk.io'
      ..fName = user.firstName ?? 'Pay'
      ..lName = user.lastName ?? 'Talk'
      ..narration = narration ??
          'Payment of ${widget.amount} ${widget.fulfillment.payInterval} to ${widget.recipient.isOrg ? widget.recipient.organization.name.titleCase : widget.recipient.user != null ? widget.recipient.user.firstName.titleCase + ' ' + widget.recipient.user.lastName.titleCase + ' (' + widget.recipient.user.username + ')' : widget.contact.displayName}'
      ..txRef = txRef ?? 'rave_flutter-${DateTime.now().toString()}'
      ..orderRef = orderRef ?? 'rave_flutter-123'
      ..acceptMpesaPayments = acceptMpesaPayment
      ..acceptAccountPayments = acceptAccountPayment
      ..acceptCardPayments = acceptCardPayment
      ..acceptAchPayments = acceptAchPayments
      ..acceptGHMobileMoneyPayments = acceptGhMMPayments
      ..acceptUgMobileMoneyPayments = acceptUgMMPayments
      ..acceptMobileMoneyFrancophoneAfricaPayments = acceptMMFrancophonePayments
      ..displayEmail = false
      ..displayAmount = false
      ..staging = !live
      ..isPreAuth = preAuthCharge
      ..companyLogo = Image.asset('assets/images/logobackground.png')
      ..displayFee = shouldDisplayFee;

    var response = await RavePayManager()
        .prompt(context: context, initializer: initializer);
    print(response);

    if (response.status == RaveStatus.success) {
      print('Card details are: ${response.rawResponse['data']['card']}');
      //record transaction
      await DatabaseService.createTransaction(trans.Transaction(
          senderId: user.uuid,
          organizationId: widget.recipient.isOrg
              ? widget.recipient.organization.organizationId
              : null,
          imageUrl: widget.recipient.isOrg
              ? widget.recipient.organization.imageUrl
              : widget.recipient.user.imageUrl,
          transactionType: widget.recipient.isOrg
              ? _planId != null && _planId.isNotEmpty
                  ? 'Subscription'
                  : 'Payment'
              : widget.recipient.user != null
                  ? 'Transfer'
                  : 'Transfer-To-Contact',
          recipientId: widget.recipient.isOrg
              ? widget.recipient.organization.userId
              : widget.recipient.user != null
                  ? widget.recipient.user.uuid
                  : '007',
          recipientName: widget.recipient.isOrg
              ? widget.recipient.organization.name
              : widget.recipient.user != null
                  ? '${widget.recipient.user.firstName} ${widget.recipient.user.lastName} (${widget.recipient.user.username})'
                  : widget.contact.displayName,
          amount: double.parse(widget.amount.toString()),
          dateOfTransaction: Timestamp.fromDate(DateTime.now()),
          currency: widget.currencyName ?? user.currency,
          transactionRef: response.rawResponse['data']['txRef'],
          orderRef: response.rawResponse['data']['orderRef'],
          transactionId: response.rawResponse['data']['id'].toString(),
          paymentGateWay: 'Flutterwave'));

      showSuccessDialog(context, 'You transaction was successful. Thank you.',
          'Transaction Successful', nextAction: () {
        if (_planId != null && _planId.isNotEmpty) {
          //use this transaction id to cancel or update subscription on the backend
          _logSubscription(response.rawResponse['data']['id'].toString());
        } else {
          Navigate.pushNamed('/home', context);
        }
      });
    } else {
      scaffoldKey.currentState
          .showSnackBar(SnackBar(content: Text(response?.message)));
    }
  }

  _logSubscription(String transactionId) async {
    await DatabaseService.createUserSubscription(
        transactionId.toString(),
        _planId,
        _plan.planName,
        user.uuid,
        double.parse(widget.amount.toString()),
        _plan.interval,
        widget.recipient.organization.organizationId);
    Navigate.pushNamed('/home', context);
  }

  @override
  Widget build(BuildContext context) {
    user = Provider.of<UserData>(context).appUser;
    config = Provider.of<UserData>(context).config;
    country = user.country;
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      key: scaffoldKey,
      body: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 30.0),
          child: ListView(
            children: [
              SizedBox(
                height: 80,
              ),
              Image.asset('assets/images/logotrans.png'),
              Text(
                'Are you sure?',
                style: TextStyle(fontSize: 25),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 20,
              ),
              RichText(
                textAlign: TextAlign.center,
                text: TextSpan(
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 19,
                      height: 1.5,
                    ),
                    text:
                        'You are about to send ${widget.currencyName}${widget.amount.toStringAsFixed(2)} ${widget.fulfillment != null ? widget.fulfillment.payInterval : ''} to\n',
                    children: [
                      TextSpan(
                          text: widget.recipient.user != null
                              ? '${widget.recipient.user.firstName.titleCase} ${widget.recipient.user.lastName.titleCase}  (${widget.recipient.user.username.titleCase})'
                              : widget.recipient.isOrg
                                  ? '${widget.recipient.organization.name.titleCase}'
                                  : '${widget.contact.displayName.titleCase} \n${widget.contact.phones.first.value.trim()}',
                          style: TextStyle(fontWeight: FontWeight.w700)),
                    ]),
              ),
              SizedBox(
                height: 35,
              ),
              Center(
                child: CircleAvatar(
                  backgroundImage: widget.contact != null
                      ? AssetImage('assets/images/contact.png')
                      : NetworkImage(
                          widget.recipient.isOrg
                              ? widget.recipient.organization.imageUrl
                              : widget.recipient.user.imageUrl,
                        ),
                  radius: 40,
                ),
              ),
              SizedBox(
                height: 35,
              ),
              SizedBox(
                width: double.infinity,
                child: AppRaisedButton(
                  title: 'Yes Please',
                  onPressed: () {
                    startPayment(config.fPublicKey, config.fEncKey);
                  },
                ),
              ),
              SizedBox(
                height: 10,
              ),
              SizedBox(
                width: double.infinity,
                child: AppOutlineButton(
                  title: "No, I'm not",
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
