import 'package:flutter/material.dart';
import 'package:paytalk_mobile/common/theme_config.dart';
import 'package:google_fonts/google_fonts.dart';

class SmallText extends StatelessWidget {
  final String text;
  final double fontSize;
  final Color color;

  const SmallText({
    Key key,
    this.text,
    this.color,
    this.fontSize = 10,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Text(text,
        style: TextStyle(
            fontSize: fontSize, fontWeight: FontWeight.normal, color: color));
  }
}
