import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:paytalk_mobile/models/donationresponse.dart';
import 'package:paytalk_mobile/models/voicepayresponse.dart';
import 'package:paytalk_mobile/views/widgets/confirmation.dart';
import 'package:recase/recase.dart';
import 'package:paytalk_mobile/models/recipient.dart';
import 'package:paytalk_mobile/models/payment_recipient.dart';
import 'package:paytalk_mobile/models/fulfillment.dart';

class UserResultCard extends StatelessWidget {
  final Users user;
  final Organizations organization;
  final String searchTerm;
  final num amount;
  final String currency;
  Fulfillment fulFillment;

  UserResultCard(
      {this.user,
      this.searchTerm,
      this.organization,
      this.amount,
      this.fulFillment,
      this.currency});

  @override
  Widget build(BuildContext context) {
    var recipient = PaymentRecipient(
        user: user,
        organization: organization,
        isOrg: organization != null ? true : false);

    Color kPrimaryColor = Color(0XFF23B88B);
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 5),
      child: InkWell(
        borderRadius: BorderRadius.circular(10),
        splashColor: kPrimaryColor,
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 8, horizontal: 8),
          decoration: BoxDecoration(
              // color: Colors.green[100],
              border: Border.all(color: Colors.grey[200]),
              borderRadius: BorderRadius.circular(10)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                //mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  CircleAvatar(
                    backgroundImage: NetworkImage(
                        user != null ? user.imageUrl : organization.imageUrl),
                    radius: 25,
                  ),
                  SizedBox(width: 15),
                  RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(
                        style: TextStyle(
                            color: searchTerm ==
                                    (user != null
                                        ? user.username
                                        : organization.name)
                                ? kPrimaryColor
                                : Theme.of(context).textTheme.bodyText2.color,
                            fontSize: 18,
                            height: 1.7,
                            fontWeight: FontWeight.bold,
                            wordSpacing: 3),
                        text: (user != null
                            ? user.username.titleCase
                            : organization.name.titleCase),
                        children: [
                          TextSpan(
                              text:
                                  '${user != null ? '-' + user.firstName : ''} ${user != null ? user.lastName : ''}',
                              style: TextStyle(
                                  color: searchTerm ==
                                          (user != null
                                              ? user.lastName
                                              : organization.name)
                                      ? kPrimaryColor
                                      : Theme.of(context)
                                          .textTheme
                                          .bodyText2
                                          .color,
                                  fontWeight: FontWeight.normal)),
                        ]),
                  ),
                ],
              ),
              SizedBox(
                height: 5,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 70),
                child: Text(
                  '${user != null ? 'Individual member' : organization.category.titleCase + ' organization'}',
                  style: TextStyle(color: Colors.grey[600], fontSize: 11),
                ),
              )
              // Padding(
              //   padding: const EdgeInsets.only(left: 50),
              //   child: Text(
              //     'Member since ${user != null ? DateFormat("MMMM dd yyyy ").format(DateTime.fromMicrosecondsSinceEpoch(user.dateJoined.iSeconds)) : DateFormat("MMMM dd yyyy ").format(DateTime.fromMicrosecondsSinceEpoch(organization.dateRegistered.iSeconds))}',
              //     style: TextStyle(color: Colors.grey[600], fontSize: 11),
              //   ),
              // )
            ],
          ),
        ),
        onTap: () {
          Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => ConfirmationPage(
                    recipient: recipient,
                    currencyName: currency,
                    fulfillment: fulFillment,
                    amount: amount,
                  )));
        },
      ),
    );
  }
}

//TODO: DELETE THIS CLASS BELOW

class CharityResultCard extends StatelessWidget {
  final Charity user;
  final String searchTerm;
  final num amount;

  CharityResultCard({this.user, this.searchTerm, this.amount});
  @override
  Widget build(BuildContext context) {
    Color kPrimaryColor = Color(0XFF23B88B);
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 5),
      child: InkWell(
        borderRadius: BorderRadius.circular(10),
        splashColor: kPrimaryColor,
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 8, horizontal: 8),
          decoration: BoxDecoration(
              // color: Colors.green[100],
              border: Border.all(color: Colors.grey[200]),
              borderRadius: BorderRadius.circular(10)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  CircleAvatar(
                    backgroundImage: NetworkImage(user.imageUrl),
                    radius: 17,
                  ),
                  SizedBox(width: 20),
                  RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(
                        style: TextStyle(
                            color: searchTerm == user.name
                                ? kPrimaryColor
                                : Theme.of(context).textTheme.bodyText2.color,
                            fontSize: 18,
                            height: 1.7,
                            fontWeight: FontWeight.bold,
                            wordSpacing: 3),
                        text: (user.name.titleCase),
                        children: [
                          TextSpan(
                              text: '',
                              style: TextStyle(
                                  color: searchTerm == user.name
                                      ? kPrimaryColor
                                      : Theme.of(context)
                                          .textTheme
                                          .bodyText2
                                          .color,
                                  fontWeight: FontWeight.normal)),
                        ]),
                  ),
                ],
              ),
              SizedBox(
                height: 5,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 50),
                child: Text(
                  'Registered since ${DateFormat("MMMM dd yyyy ").format(DateTime.fromMicrosecondsSinceEpoch(user.dateRegistered.iSeconds))}',
                  style: TextStyle(color: Colors.grey[600], fontSize: 11),
                ),
              )
            ],
          ),
        ),
        onTap: () {
          // Navigator.of(context).push(MaterialPageRoute(
          //     builder: (context) => ConfirmationPage(
          //           charity: user,
          //           amount: amount,
          //         )));
        },
      ),
    );
  }
}
