import 'package:animated_dialog/animated_dialog.dart';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:paytalk_mobile/models/donationresponse.dart';
import 'package:paytalk_mobile/models/voicepayresponse.dart';
import 'package:paytalk_mobile/views/widgets/userresultcard.dart';
import 'package:paytalk_mobile/models/transfer_recipient.dart';
import 'package:contacts_service/contacts_service.dart';
import 'confirmation.dart';
import 'package:paytalk_mobile/common/strings.dart';
import 'package:paytalk_mobile/models/recipient.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

showSuccessDialog(BuildContext context, String message, String success,
    {Function nextAction}) async {
  return AwesomeDialog(
    context: context,
    animType: AnimType.SCALE,
    dialogType: DialogType.SUCCES,
    title: success,
    desc: message,
    btnOkOnPress: () {
      nextAction();
    },
  ).show();
}

showQuestionDialog(BuildContext context, String message,
    {Function nextAction}) async {
  return AwesomeDialog(
    context: context,
    animType: AnimType.SCALE,
    dialogType: DialogType.INFO,
    title: 'More information required',
    desc: message,
    btnOkText: 'Tap to speak',
    btnOkOnPress: () {
      if (nextAction != null) nextAction();
    },
  ).show();
}

Widget showSearchResults(BuildContext context, TransferRecipient recipient,
    {List<Contact> contacts}) {
  return SingleChildScrollView(
    child: Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 20),
          // _buildSearchField(),
          //SizedBox(height: 17),
          Center(
            child: Text(
              '${AppStrings.appName} found these results',
              style: TextStyle(fontSize: 15, color: Colors.green[300]),
            ),
          ),
          SizedBox(height: 17),
          // recipient.command.toString().contains('send')
          _buildSearchResults(context, recipient, recipient.contacts,
              recipient.amount, recipient.currency)
          // : _buildDonationResults(context, recipient.donationResponse)
        ],
      ),
    ),
  );
}

_buildSearchField() {
  return Container(
    margin: EdgeInsets.symmetric(vertical: 5),
    decoration: BoxDecoration(
        border: Border.all(color: Color(0XFFe4e4e4)),
        borderRadius: BorderRadius.circular(10)),
    child: TextField(
      decoration: InputDecoration(
          border: InputBorder.none,
          prefixIcon: Icon(Icons.search),
          hintText: 'Enter PayTalk ID',
          hintStyle: TextStyle(color: Color(0XFF999999))),
    ),
  );
}

_buildSearchResults(BuildContext context, TransferRecipient recipient,
    List<Contact> contacts, num amount, String currency) {
  return Container(
    child: Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: <
        Widget>[
      DefaultTabController(
          length: 2, // length of tabs
          initialIndex: 0,
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Container(
                  child: TabBar(
                    labelColor: Colors.green,
                    unselectedLabelColor: Colors.black,
                    tabs: [
                      Tab(text: 'Organizations'),
                      Tab(text: 'People'),
                      // Tab(text: 'Contacts'),
                    ],
                  ),
                ),
                Container(
                    height: 400, //height of TabBarView
                    decoration: BoxDecoration(
                        border: Border(
                            top: BorderSide(color: Colors.grey, width: 0.5))),
                    child: TabBarView(children: <Widget>[
                      Container(
                          child: Visibility(
                        visible: recipient.recipients.organizations != null &&
                            recipient.recipients.organizations.isNotEmpty,
                        replacement: Center(
                            child: Text('No organizations matched your query')),
                        child: ListView.builder(
                          shrinkWrap: true,
                          physics: ScrollPhysics(),
                          itemCount:
                              (recipient.recipients.organizations.length),
                          itemBuilder: (BuildContext context, int index) {
                            print('amount to send is $amount');
                            return UserResultCard(
                              organization:
                                  recipient.recipients.organizations[index],
                              searchTerm: 'Saviour',
                              amount: amount,
                              fulFillment: recipient.fulfillment,
                              currency: currency,
                            );
                          },
                        ),
                      )),
                      Container(
                          child: Visibility(
                        visible: recipient.recipients.users != null &&
                            recipient.recipients.users.isNotEmpty,
                        replacement:
                            Center(child: Text('No users matched your  query')),
                        child: ListView.builder(
                          shrinkWrap: true,
                          physics: ScrollPhysics(),
                          itemCount: (recipient.recipients.users.length),
                          itemBuilder: (BuildContext context, int index) {
                            return UserResultCard(
                              user: recipient.recipients.users[index],
                              searchTerm: 'Saviour',
                              amount: amount,
                              currency: currency,
                            );
                          },
                        ),
                      )),
                      //Will add contacts later
                      // Container(
                      //   child: Visibility(
                      //     visible: contacts != null && contacts.isNotEmpty,
                      //     replacement: Center(
                      //         child:
                      //             Text('None of your contacts matched the query')),
                      //     child: ListView.builder(
                      //       itemCount: contacts != null && contacts.isNotEmpty
                      //           ? contacts.length
                      //           : 0,
                      //       itemBuilder: (BuildContext context, int index) {
                      //         Contact contact = contacts.toList()[index];

                      //         return InkWell(
                      //           onTap: () => Navigator.of(context).push(
                      //               MaterialPageRoute(
                      //                   builder: (context) => ConfirmationPage(
                      //                       contact: contact,
                      //                       amount: double.parse(amount.toString()),
                      //                       currencyName: currency))),
                      //           child: ListTile(
                      //             title: Text(contact.displayName),
                      //             leading: CircleAvatar(
                      //               backgroundImage:
                      //                   AssetImage('assets/images/contact.png'),
                      //             ),
                      //           ),
                      //         );
                      //       },
                      //     ),
                      //   ),
                      // ),
                    ]))
              ])),
    ]),
  );
}

_buildDonationResults(BuildContext context, DonationResponse response) {
  return ListView.builder(
    shrinkWrap: true,
    physics: ScrollPhysics(),
    itemCount: response.charity.length,
    itemBuilder: (BuildContext context, int index) {
      return CharityResultCard(
        user: response.charity[index],
        searchTerm: 'Donate',
        amount: response.amount,
      );
    },
  );
}

showMerchants(BuildContext context, TransferRecipient recipient,
    {List<Contact> contacts}) async {
  showDialog(
      context: context,
      barrierDismissible: true,
      child: AnimatedDialog(
        width: 350,
        height: 550,
        animationTime: Duration(seconds: 1),
        child: showSearchResults(context, recipient),
      ));
}

showNoMerchants(BuildContext context, String message, String success,
    {Function nextAction}) async {
  return AwesomeDialog(
    context: context,
    animType: AnimType.SCALE,
    dialogType: DialogType.NO_HEADER,
    title: success,
    desc: message,
    btnOkOnPress: () {
      if (nextAction != null) nextAction();
    },
  ).show();
}

showAuthFailed(BuildContext context, String message, String success,
    {Function nextAction}) async {
  return AwesomeDialog(
    context: context,
    animType: AnimType.SCALE,
    dialogType: DialogType.ERROR,
    title: success,
    desc: message,
    btnOkOnPress: () {
      if (nextAction != null) nextAction();
    },
  ).show();
}

showGenericFailure(
    BuildContext context, String title, String message, IconData icon) {
  Alert(
      context: context,
      title: title,
      desc: '',
      image: Icon(icon, color: Colors.red, size: 40),
      buttons: [
        DialogButton(
          color: Colors.green[300],
          onPressed: () {
            Navigator.pop(context);
          },
          child: Text(
            "OK",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
        )
      ]).show();
}

showGenericCancel(
    BuildContext context, String title, String message, IconData icon,
    {Function nextAction}) {
  Alert(
      context: context,
      title: title,
      desc: '',
      image: Icon(icon, color: Colors.red, size: 40),
      buttons: [
        DialogButton(
          color: Colors.green[300],
          onPressed: () {
            Navigator.pop(context);
            if (nextAction != null) nextAction();
          },
          child: Text(
            "YES",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
        ),
        DialogButton(
          color: Colors.green[300],
          onPressed: () {
            Navigator.pop(context);
          },
          child: Text(
            "NO",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
        )
      ]).show();
}
