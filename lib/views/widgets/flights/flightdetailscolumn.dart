import 'package:flutter/material.dart';
import 'package:paytalk_mobile/models/flightmodel.dart';

class FlightDetailColumn extends StatelessWidget {
  final Color textColor;
  final FlightModel model;
  final bool isStart;
  const FlightDetailColumn({Key key, this.textColor, this.model, this.isStart})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          "${isStart ? model.startDate : model.arrivalDate}",
          style: Theme.of(context)
              .textTheme
              .subhead
              .apply(color: textColor ?? Colors.white),
        ),
        Text(
          "${isStart ? model.startCityCode : model.destinationCityCode}",
          style: Theme.of(context)
              .textTheme
              .title
              .apply(color: textColor ?? Colors.white, fontWeightDelta: 2),
        ),
        Text(
          "${isStart ? model.startCity : model.destinationCity}",
          style: Theme.of(context)
              .textTheme
              .subhead
              .apply(color: textColor ?? Colors.white),
        ),
      ],
    );
  }
}
