import 'package:flutter/material.dart';
import 'package:paytalk_mobile/models/flightmodel.dart';
import 'flightdetailscolumn.dart';
import '../buttons.dart';
import 'package:uuid/uuid.dart';
import 'package:rave_flutter/rave_flutter.dart';
import 'package:paytalk_mobile/models/user.dart' as appUser;
import 'package:paytalk_mobile/models/user_data.dart';
import 'package:paytalk_mobile/models/paymentplan.dart';
import 'package:paytalk_mobile/models/appconfig.dart';
import 'package:paytalk_mobile/models/transaction.dart' as trans;
import 'package:paytalk_mobile/views/widgets/dialogs.dart';
import 'package:paytalk_mobile/common/navigate.dart';
import 'package:provider/provider.dart';

class DetailsScreen extends StatefulWidget {
  FlightModel model;

  DetailsScreen(this.model);

  @override
  _DetailsScreenState createState() => _DetailsScreenState();
}

class _DetailsScreenState extends State<DetailsScreen> {
  String airplaneImg =
      "https://www.searchpng.com/wp-content/uploads/2019/01/Planes-PNG-image-1-715x241.png";

  String cloudImg = "http://pngimg.com/uploads/cloud/cloud_PNG24.png";

  String boardingpassImg =
      "https://barcode.tec-it.com/barcode.ashx?data=This+is+a+PDF417+by+TEC-IT&code=PDF417&dpi=96&dataseparator=";

  String userimageUrl =
      "https://cdn.pixabay.com/photo/2016/10/09/18/03/smile-1726471_960_720.jpg";

  bool acceptCardPayment = true;

  bool acceptAccountPayment = true;

  bool acceptMpesaPayment = false;

  bool shouldDisplayFee = true;

  bool acceptAchPayments = false;

  bool acceptGhMMPayments = false;

  bool acceptUgMMPayments = false;

  bool acceptMMFrancophonePayments = false;

  bool live = false;

  bool preAuthCharge = false;

  bool addSubAccounts = false;

  List<SubAccount> subAccounts = [];

  String _planId;

  PaymentPlan _plan;

  String email;

  double amount;

  String txRef;

  String orderRef;

  String narration;

  String currency;

  String country;

  String firstName;

  String lastName;

  var scaffoldKey = GlobalKey<ScaffoldState>();

  var price = 1200;

  appUser.User user;

  AppConfig config;

  var uuid = Uuid();

  @override
  void initState() {
    super.initState();

    setState(() {
      txRef = uuid.v4();
      orderRef = uuid.v1();
    });
  }

  void startPayment(String pubkey, String encKey) async {
    var initializer = RavePayInitializer(
        paymentPlan: _planId != null && _planId.isNotEmpty ? _planId : null,
        amount: double.parse(widget.model.price.toString()),
        publicKey: 'FLWPUBK_TEST-f080dd9f1959d29a126928b6dfae1169-X',
        encryptionKey: 'FLWSECK_TESTd5cf77cdc4f2',
        subAccounts: subAccounts.isEmpty ? null : null)
      ..country =
          country = country != null && country.isNotEmpty ? country : "NG"
      ..currency = user.currency
      ..email = user.email ?? 'info@paytalk.io'
      ..fName = user.firstName ?? 'Pay'
      ..lName = user.lastName ?? 'Talk'
      ..narration = narration ??
          'Purchase of fligt ticket from ${widget.model.startCity} to ${widget.model.destinationCity} at ${widget.model.price}'
      ..txRef = txRef ?? 'rave_flutter-${DateTime.now().toString()}'
      ..orderRef = orderRef ?? 'rave_flutter-123'
      ..acceptMpesaPayments = acceptMpesaPayment
      ..acceptAccountPayments = acceptAccountPayment
      ..acceptCardPayments = acceptCardPayment
      ..acceptAchPayments = acceptAchPayments
      ..acceptGHMobileMoneyPayments = acceptGhMMPayments
      ..acceptUgMobileMoneyPayments = acceptUgMMPayments
      ..acceptMobileMoneyFrancophoneAfricaPayments = acceptMMFrancophonePayments
      ..displayEmail = false
      ..displayAmount = false
      ..staging = !live
      ..isPreAuth = preAuthCharge
      ..companyLogo = Image.asset('assets/images/logobackground.png')
      ..displayFee = shouldDisplayFee;

    var response = await RavePayManager()
        .prompt(context: context, initializer: initializer);
    print(response);

    if (response.status == RaveStatus.success) {
      print('Card details are: ${response.rawResponse['data']['card']}');
      //record transaction
      // await DatabaseService.createTransaction(trans.Transaction(
      //     senderId: user.uuid,
      //     organizationId: widget.recipient.isOrg
      //         ? widget.recipient.organization.organizationId
      //         : null,
      //     imageUrl: widget.recipient.isOrg
      //         ? widget.recipient.organization.imageUrl
      //         : widget.recipient.user.imageUrl,
      //     transactionType: widget.recipient.isOrg
      //         ? _planId != null && _planId.isNotEmpty
      //             ? 'Subscription'
      //             : 'Payment'
      //         : widget.recipient.user != null
      //             ? 'Transfer'
      //             : 'Transfer-To-Contact',
      //     recipientId: widget.recipient.isOrg
      //         ? widget.recipient.organization.userId
      //         : widget.recipient.user != null
      //             ? widget.recipient.user.uuid
      //             : '007',
      //     recipientName: widget.recipient.isOrg
      //         ? widget.recipient.organization.name
      //         : widget.recipient.user != null
      //             ? '${widget.recipient.user.firstName} ${widget.recipient.user.lastName} (${widget.recipient.user.username})'
      //             : widget.contact.displayName,
      //     amount: double.parse(widget.amount.toString()),
      //     dateOfTransaction: Timestamp.fromDate(DateTime.now()),
      //     currency: widget.currencyName ?? user.currency,
      //     transactionRef: response.rawResponse['data']['txRef'],
      //     orderRef: response.rawResponse['data']['orderRef'],
      //     transactionId: response.rawResponse['data']['id'].toString(),
      //     paymentGateWay: 'Flutterwave'));

      showSuccessDialog(context, 'You transaction was successful. Thank you.',
          'Transaction Successful', nextAction: () {
        if (_planId != null && _planId.isNotEmpty) {
          //use this transaction id to cancel or update subscription on the backend
          _logSubscription(response.rawResponse['data']['id'].toString());
        } else {
          Navigate.pushNamed('/home', context);
        }
      });
    } else {
      scaffoldKey.currentState
          .showSnackBar(SnackBar(content: Text(response?.message)));
    }
  }

  _logSubscription(String transactionId) async {
    // await DatabaseService.createUserSubscription(
    //     transactionId.toString(),
    //     _planId,
    //     _plan.planName,
    //     user.uuid,
    //     double.parse(widget.amount.toString()),
    //     _plan.interval,
    //     widget.recipient.organization.organizationId);
    // Navigate.pushNamed('/home', context);
  }

  @override
  Widget build(BuildContext context) {
    final green = Colors.green[300];
    user = Provider.of<UserData>(context).appUser;
    config = Provider.of<UserData>(context).config;
    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        backgroundColor: green,
        automaticallyImplyLeading: true,
        centerTitle: true,
        title: Text("FLIGHT BOOKING"),
        elevation: 0.0,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.save),
            onPressed: () {},
          )
        ],
      ),
      body: Stack(
        children: <Widget>[
          Positioned.fill(
            child: Column(
              children: <Widget>[
                Flexible(
                  flex: 1,
                  child: Container(
                    color: green,
                  ),
                ),
                Flexible(
                  flex: 2,
                  child: Container(),
                )
              ],
            ),
          ),
          Positioned.fill(
            child: Container(
              height: double.infinity,
              margin: EdgeInsets.symmetric(horizontal: 35.0, vertical: 15.0),
              decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black45,
                      blurRadius: 7.0,
                      offset: Offset(0, 3),
                    )
                  ],
                  borderRadius: BorderRadius.circular(15.0),
                  color: Color(0xfff7f9ff)),
              child: Column(
                children: <Widget>[
                  Container(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 15.0, vertical: 25.0),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(15.0),
                          topRight: Radius.circular(15.0)),
                      gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [green, Color(0xfff7f9ff)],
                      ),
                    ),
                    child: Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            FlightDetailColumn(
                                textColor: Colors.black,
                                model: widget.model,
                                isStart: true),
                            Expanded(
                              child: Column(
                                // mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Icon(Icons.airplanemode_active,
                                      color: Colors.white),
                                  Text(
                                    "${widget.model.flightDuration}",
                                    textAlign: TextAlign.center,
                                    style: Theme.of(context)
                                        .textTheme
                                        .subtitle
                                        .apply(color: Colors.white),
                                  )
                                ],
                              ),
                            ),
                            FlightDetailColumn(
                                textColor: Colors.black,
                                model: widget.model,
                                isStart: false),
                          ],
                        ),
                        Container(
                          margin: EdgeInsets.symmetric(vertical: 15.0),
                          height: 100,
                          child: Stack(
                            children: <Widget>[
                              Positioned.fill(
                                child: Image.network(cloudImg),
                              ),
                              Positioned.fill(
                                child: Image.network(airplaneImg),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: FlightInfoRow(
                          title: "${widget.model.carrier}",
                          content: "LF713",
                        ),
                      ),
                      Expanded(
                        child: FlightInfoRow(
                          title: "Class",
                          content: "First",
                        ),
                      ),
                      Expanded(
                        child: FlightInfoRow(
                          title: "Boarding",
                          content: "09:11",
                        ),
                      ),
                      Expanded(
                        child: FlightInfoRow(
                          title: "Terminal",
                          content: "12A",
                        ),
                      ),
                    ],
                  ),
                  // Container(
                  //   margin: EdgeInsets.symmetric(vertical: 15.0),
                  //   padding: const EdgeInsets.all(15.0),
                  //   color: Colors.white,
                  //   child: Column(
                  //     children: <Widget>[
                  //       PassengerContainer(
                  //         age: "21",
                  //         imageUrl:
                  //             "https://lh3.googleusercontent.com/-GELaWFBRPnQ/We3KfYBqTuI/AAAAAAAAAqE/wQDXxVI6nFoox1gOKfvIjmH1_5LKUhKzACEwYBhgL/w140-h140-p/20376010_1795833643775120_614181264397520443_n%2B%25281%2529.jpg",
                  //         fullName: "Amazigh Halzoun",
                  //         gender: "MALE",
                  //         seat: "18A",
                  //       ),
                  //       Divider(),
                  //       PassengerContainer(
                  //         age: "21",
                  //         imageUrl: userimageUrl,
                  //         fullName: "CYBDOM TECH",
                  //         gender: "MALE",
                  //         seat: "17A",
                  //       ),
                  //     ],
                  //   ),
                  // ),
                  Image.network(boardingpassImg),
                  SizedBox(height: 40),

                  Text(
                    "${widget.model.currency} ${widget.model.price.toStringAsFixed(2)}",
                    style: Theme.of(context)
                        .textTheme
                        .title
                        .apply(color: Colors.black87),
                  ),

                  SizedBox(height: 30),

                  MaterialButton(
                    onPressed: () {
                      startPayment(config.fPublicKey, config.fEncKey);
                    },
                    child: Text('Book this flight'),
                    color: Colors.green[300],
                    minWidth: 200,
                    textColor: Colors.white,
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}

class FlightInfoRow extends StatelessWidget {
  final String title, content;

  const FlightInfoRow({Key key, this.title, this.content}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Text(
          "$title",
          style: Theme.of(context).textTheme.body1.apply(color: Colors.black45),
        ),
        SizedBox(
          height: 3.0,
        ),
        Text(
          "$content",
          style: Theme.of(context).textTheme.title.apply(color: Colors.black87),
        )
      ],
    );
  }
}
