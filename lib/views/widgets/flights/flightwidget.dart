import 'package:flutter/material.dart';
import 'flightdetailscolumn.dart';
import 'package:paytalk_mobile/models/flightmodel.dart';
import 'package:paytalk_mobile/common/navigate.dart';
import 'details.dart';

class FlightWidget extends StatelessWidget {
  final FlightModel model;
  FlightWidget(this.model);
  @override
  Widget build(BuildContext context) {
    final greenColor = Colors.green[300];
    return GestureDetector(
      onTap: () => Navigate.pushPage(context, DetailsScreen(this.model)),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 21.0, vertical: 31.0),
        margin: EdgeInsets.only(top: 20),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10.0),
          boxShadow: [
            BoxShadow(
              color: Colors.grey[300],
              blurRadius: 11,
              offset: Offset(0, 3),
            ),
          ],
        ),
        child: Row(
          children: <Widget>[
            FlightDetailColumn(
                textColor: Colors.black, model: model, isStart: true),
            Expanded(
              child: Column(
                children: <Widget>[
                  Icon(Icons.airplanemode_active, color: greenColor),
                  Text(
                    "${model.flightDuration}",
                    style: Theme.of(context)
                        .textTheme
                        .subtitle
                        .apply(color: Colors.grey),
                  )
                ],
              ),
            ),
            FlightDetailColumn(
                textColor: Colors.black, model: model, isStart: false),
          ],
        ),
      ),
    );
  }
}
