import 'package:flutter/material.dart';
import 'flightwidget.dart';
import 'package:paytalk_mobile/models/flightmodel.dart';

class FlightList extends StatefulWidget {
  final String startCity;
  final String destinationCity;
  FlightList(this.startCity, this.destinationCity);
  @override
  _FlightListState createState() => _FlightListState();
}

class _FlightListState extends State<FlightList> {
  List<FlightModel> flightList = [
    FlightModel(
        startCity: 'Lagos',
        startCityCode: 'LAG',
        startDate: '23 JAN, 15:18',
        destinationCity: 'Port Harcourt',
        destinationCityCode: 'PHC',
        arrivalDate: '23 JAN, 17:25',
        carrier: 'Air Peace',
        price: 38000,
        currency: 'NGN',
        flightDuration: '1h:10mins'),
    FlightModel(
        startCity: 'Lagos',
        startCityCode: 'LAG',
        startDate: '23 JAN, 07:15',
        destinationCity: 'Abuja',
        destinationCityCode: 'ABJ',
        arrivalDate: '23 JAN, 08:15',
        carrier: 'Arik Air',
        price: 25000,
        currency: 'NGN',
        flightDuration: '1h:05mins'),
    FlightModel(
        startCity: 'Lagos',
        startCityCode: 'LAG',
        startDate: '23 JAN, 07:30',
        destinationCity: 'Abuja',
        destinationCityCode: 'ABJ',
        carrier: 'Ibom Air',
        price: 45000,
        currency: 'NGN',
        arrivalDate: '23 DEC, 08:25',
        flightDuration: '50mins'),
    FlightModel(
        startCity: 'Lagos',
        startCityCode: 'LAG',
        startDate: '23 JAN, 18:40',
        destinationCity: 'Port Harcourt',
        destinationCityCode: 'PHC',
        carrier: 'Air Peace',
        price: 30000,
        currency: 'NGN',
        arrivalDate: '23 DEC, 19:50',
        flightDuration: '1h:10mins'),
    FlightModel(
        startCity: 'Lagos',
        startCityCode: 'LAG',
        startDate: '23 JAN, 07:30',
        destinationCity: 'Port Harcourt',
        destinationCityCode: 'ABJ',
        carrier: 'Ibom Air',
        price: 19800,
        currency: 'NGN',
        arrivalDate: '23 DEC, 08:25',
        flightDuration: '50mins'),
    FlightModel(
        startCity: 'Lagos',
        startCityCode: 'LAG',
        startDate: '23 JAN, 19:30',
        destinationCity: 'Abuja',
        destinationCityCode: 'ABJ',
        carrier: 'Ibom Air',
        price: 55000,
        currency: 'NGN',
        arrivalDate: '23 DEC, 20:25',
        flightDuration: '50mins'),
  ];

  @override
  Widget build(BuildContext context) {
    var realList = flightList
        .where((model) =>
            model.startCity.contains(widget.startCity) &&
            model.destinationCity.contains(widget.destinationCity))
        .toList();
    final title = Container(
      margin: EdgeInsets.only(top: 10.0, bottom: 10.0, left: 20),
      child: Text("Available Flights",
          style: TextStyle(fontSize: 30, color: Colors.white)),
    );

    final subTitle = Container(
      margin: EdgeInsets.only(bottom: 10.0, left: 20),
      child: Text("Here are the results of your flight search",
          style: TextStyle(fontSize: 15, color: Colors.white)),
    );

    final list = Expanded(
        child: ListView.builder(
            padding: EdgeInsets.only(left: 10, right: 10),
            itemCount: realList.length,
            itemBuilder: (context, index) {
              // return transactionItem(_transactionsList[index]);
              return FlightWidget(realList[index]);
            }));
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(backgroundColor: Colors.green[300]),
        //backgroundColor: Theme.of(context).backgroundColor,
        body: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(0.0), topRight: Radius.circular(0.0)),
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [Colors.green[300], Color(0xfff7f9ff)],
            ),
          ),
          padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(Icons.flight_land_rounded,
                      color: Colors.white, size: 40),
                  title,
                ],
              ),
              Center(child: subTitle),
              list
            ],
          ),
        ),
      ),
    );
  }
}
