import 'package:flutter/material.dart';
//import 'package:paystack/utilities/constansts.dart';
import 'package:paytalk_mobile/common/theme_config.dart';

class AppOutlineButton extends StatelessWidget {
  final String title;
  final Function onPressed;
  AppOutlineButton({this.title, this.onPressed});

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
        color: Colors.white,
        textColor: ThemeConfig.kPrimaryColor,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5.0),
            side: BorderSide(color: ThemeConfig.kPrimaryColor)),
        elevation: 0,
        child: Padding(
          padding: const EdgeInsets.all(14.0),
          child: Text(title),
        ),
        onPressed: onPressed);
  }
}

class AppRaisedButton extends StatelessWidget {
  final String title;
  final Function onPressed;
  AppRaisedButton({this.title, this.onPressed});

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
        color: ThemeConfig.kPrimaryColor,
        textColor: Colors.white,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5.0),
        ),
        elevation: 0,
        child: Padding(
          padding: const EdgeInsets.all(14.0),
          child: Text(title),
        ),
        onPressed: onPressed);
  }
}
