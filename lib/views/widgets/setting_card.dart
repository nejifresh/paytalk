import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class SettingCard extends StatefulWidget {
  final String title;
  final bool allowBorder;
  final bool value;
  final Function changeFunction;
  SettingCard({this.title, this.allowBorder, this.value, this.changeFunction});

  @override
  _SettingCardState createState() => _SettingCardState(
      title: title,
      allowBorder: allowBorder,
      value: value,
      changeFunction: changeFunction);
}

class _SettingCardState extends State<SettingCard> {
  final String title;
  final bool allowBorder;
  final bool value;
  final Function changeFunction;

  _SettingCardState(
      {this.title, this.value, this.allowBorder, this.changeFunction});

  @override
  Widget build(BuildContext context) {
    return new Container(
      padding: new EdgeInsets.only(top: 5.0, left: 18.0),
      decoration: new BoxDecoration(
        border: (widget.allowBorder != false
            ? new Border(
                bottom: new BorderSide(color: Colors.grey, width: 1.0),
              )
            : null),
      ),
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          new Text(
            widget.title,
            style: new TextStyle(
                fontSize: 15.0, color: Colors.grey.withOpacity(0.8)),
          ),
//          Theme.of(context).platform == TargetPlatform.iOS
//              ? new CupertinoSwitch(
//                  value: value,
//              onChanged: (value){
//                if(widget.title == 'Use Local Device Authentication' ){
//
//                  //value = !value;
//                  PrefHelper().setPreferences('USELOCALAUTH', value);
//                  print('LOCAL AUTH SET TO ${value}');
//                  //value = !value;
//                }
//
//                if(widget.title == 'Enable Reminders' ){
//
//                  //value = !value;
//                  PrefHelper().setPreferences('REMINDERS', value);
//                  print('REMINDERS SET TO ${value}');
//                  //value = !value;
//                }
//                setState(() {
//
//                });
//              },
//              activeColor: new Color.fromRGBO(107, 85, 153, 1.0),
//                )
//              :

          new Switch(
            value: widget.value,
            onChanged: (value) {
              changeFunction(value);
            },
            activeColor: Colors.green,
          ),
        ],
      ),
    );
  }
}
