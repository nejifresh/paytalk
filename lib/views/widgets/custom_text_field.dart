import 'package:flutter/material.dart';

class CustomTextField extends StatefulWidget {
  final bool enabled;
  final IconData icon;
  final IconData prefix;
  final double boxWidth, boxHeight;
  final String hintText, labelText, initialValue;
  final TextInputType textInputType;
  final TextEditingController controller;
  final TextInputAction textInputAction;
  final FocusNode focusNode, nextFocusNode;
  final VoidCallback submitAction;
  final Widget suffixIcon;
  final Color iconColor;
  final bool obscureText;
  final TextStyle textStyle;
  final FormFieldValidator<String> validateFunction;
  final void Function(String) onSaved, onChange;
  final Widget widgetPrefix;
  final Key key;

  CustomTextField(
      {this.key,
      this.widgetPrefix,
      this.prefix,
      this.boxHeight,
      this.boxWidth,
      this.controller,
      this.labelText,
      this.hintText,
      this.initialValue,
      this.obscureText = false,
      this.textInputType,
      this.textInputAction,
      this.focusNode,
      this.nextFocusNode,
      this.submitAction,
      this.suffixIcon,
      this.icon,
      this.iconColor,
      this.textStyle,
      this.validateFunction,
      this.onChange,
      this.onSaved,
      this.enabled});

  @override
  _CustomTextFieldState createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  bool error = false;

  @override
  Widget build(BuildContext context) {
    double fontSize = 16.0;
    return TextFormField(
      autovalidateMode: AutovalidateMode.onUserInteraction,
      enabled: widget.enabled,
      onChanged: (val) {
        if (widget.validateFunction(val) != null) {
          error = true;
          setState(() {});
        } else {
          error = false;
          setState(() {});
        }
      },
      style: TextStyle(
        color: error ? Colors.red : Theme.of(context).textTheme.bodyText1.color,
        fontSize: fontSize,
        fontWeight: FontWeight.w400,
      ),
      maxLines: 1,
      autocorrect: false,
      key: widget.key,
      initialValue: widget.initialValue,
      controller: widget.controller,
      obscureText: widget.obscureText,
      keyboardType: widget.textInputType,
      validator: widget.validateFunction,
      onSaved: widget.onSaved,
      cursorColor: Theme.of(context).accentColor,
      textInputAction: widget.textInputAction,
      focusNode: widget.focusNode,
      textCapitalization: (widget.hintText.toLowerCase().contains('name'))
          ? TextCapitalization.words
          : TextCapitalization.none,
      onFieldSubmitted: (String term) {
        if (widget.nextFocusNode != null) {
          widget.focusNode.unfocus();
          FocusScope.of(context).requestFocus(widget.nextFocusNode);
        } else {
          widget.submitAction();
        }
      },
      decoration: InputDecoration(
        prefixIcon: widget.widgetPrefix == null ?Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Theme(
              data: ThemeData(primaryColor: Colors.blue),
              child: Icon(widget.prefix),
            ),
          ],
        ):widget.widgetPrefix,
        hintText: widget.hintText,
        isDense: false,
        hintStyle: TextStyle(
          fontSize: fontSize,
          fontWeight: FontWeight.w400,
        ),
        enabledBorder: border(),
        focusedBorder: border(),
      ),
    );
  }

  border({Color color = Colors.grey}) {
    return UnderlineInputBorder(borderSide: BorderSide(color: color));
  }

  screenHeight(BuildContext context) {
    return MediaQuery.of(context).size.height;
  }
}
