import 'package:flutter/material.dart';
import 'package:paytalk_mobile/common/utils.dart';
import 'package:flutter_svg/flutter_svg.dart';

class HelpPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final List<String> questions = [
      "How does PayTalk Work?",
      "What are the accepted PayTalk commands?",
      "How can I make a donation to a charity?",
      "What are the accepted payment methods?",
    ];

    final faqs = ListView(
      shrinkWrap: true,
      children: questions.map((question) {
        return ExpansionTile(
          title: Text(
            question,
            style: TextStyle(color: Colors.green[400]),
          ),
          children: <Widget>[
            ListTile(
              title: Text(
                getAnswers(question),
              ),
            ),
          ],
        );
      }).toList(),
    );

    return BaseView(title: "Help", body: faqs);
  }

  String getAnswers(String question) {
    var answer = '';
    switch (question) {
      case 'What are the accepted PayTalk commands?':
        answer = 'PayTalk accepts the following commands - Send and Donate ';
        break;
      case 'How does PayTalk Work?':
        answer =
            'PayTalk lets you send money, make donations to charities/non-profit organizations and send money using your voice';
        break;
      case 'How can I make a donation to a charity?':
        answer =
            'You make a donation by saying DONATE and specifiying the amount and the the charity organization. For example you can say DONATE 50 DOLLARS TO THE RED CROSS';
        break;
      case 'What are the accepted payment methods?':
        answer =
            'Accepted payment methods include credit card payments and bank account payments in some countries.';
        break;
    }
    return answer;
  }
}

class BaseView extends StatelessWidget {
  final String title;
  final Widget body;
  final EdgeInsetsGeometry padding;

  const BaseView({
    Key key,
    @required this.title,
    @required this.body,
    this.padding = const EdgeInsets.only(
      left: 20.0,
      right: 20.0,
      top: 40.0,
    ),
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final top = Container(
      margin: EdgeInsets.only(bottom: 30.0),
      child: HeaderText(text: title),
    );

    final content = Expanded(child: body);

    final image = SvgPicture.asset(
      AppIll.invite,
      height: 300.0,
    );

    return Scaffold(
      appBar: CustomAppBar(),
      body: Container(
        padding: padding,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[image, top, content],
        ),
      ),
    );
  }
}
