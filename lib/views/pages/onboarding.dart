import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:paytalk_mobile/common/navigate.dart';
import 'package:paytalk_mobile/views/pages/auth/sign_in.dart';
import 'package:paytalk_mobile/views/widgets/custom_button.dart';

class Onboarding extends StatefulWidget {
  @override
  _OnboardingState createState() => _OnboardingState();
}

class _OnboardingState extends State<Onboarding> {
  PageController controller;
  int page = 0;
  int currentPageValue = 0;
  List pages = [
    {
      'img': 'assets/images/transfer1.svg',
      'title': 'Payments Made Super Easy',
      'subtitle':
          'Easily send money, pay bills or make donations just by saying it',
    },
    {
      'img': 'assets/images/transfer2.svg',
      'title': 'Send Payments Securely',
      'subtitle': 'PayTalk authenticates you before making transfers',
    },
    {
      'img': 'assets/images/transfer4.svg',
      'title': 'Get Started',
      'subtitle':
          'Sign up for an account and start transacting using your voice today',
    },
  ];

  @override
  void initState() {
    super.initState();
    controller = PageController(initialPage: 0);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(width: MediaQuery.of(context).size.width),
            SizedBox(height: 10.0),
            Hero(
              tag: 'logo',
              child: Container(
                width: 150,
                height: 150,
              ),
              // child: Image.asset(
              //   'images/truya_logo.png',
              //   width: 150.0,
              //   height: 150.0,
              //   color: Theme.of(context).accentColor,
              // ),
            ),
            SizedBox(height: 80.0),
            Container(
              height: 350.0,
              child: PageView(
                controller: controller,
                children: [
                  for (Map page in pages)
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 40.0),
                      child: Column(
                        children: [
                          SvgPicture.asset(page['img'], height: 200.0),
                          SizedBox(height: 30.0),
                          Text(
                            page['title'],
                            style: TextStyle(
                              color: Color(0xff464855),
                              fontWeight: FontWeight.bold,
                              fontSize: 18.0,
                            ),
                          ),
                          SizedBox(height: 5.0),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 43),
                            child: Text(
                              page['subtitle'],
                              style: TextStyle(
                                color: Color(0xff464855),
                                fontSize: 14.0,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          )
                        ],
                      ),
                    )
                ],
                onPageChanged: pageChangeCallback,
              ),
            ),
            SizedBox(height: 40.0),
            Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                for (int i = 0; i < pages.length; i++)
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 2.0),
                    child: AnimatedContainer(
                      duration: Duration(milliseconds: 300),
                      width: 30.0,
                      height: 6.0,
                      decoration: BoxDecoration(
                        color: i == currentPageValue
                            ? Theme.of(context).backgroundColor
                            : Color(0xffC4C4C4),
                        borderRadius: BorderRadius.circular(5.0),
                      ),
                    ),
                  )
              ],
            ),
            SizedBox(height: 15.0),
            Visibility(
              visible: currentPageValue != 2,
              child: Text(
                'Swipe to learn more',
                style: TextStyle(color: Color(0xff464855), fontSize: 14.0),
              ),
            ),
            SizedBox(height: 10.0),
            Visibility(
              visible: currentPageValue == 2,
              child: CustomButton(
                label: 'Sign In',
                onTap: () {
                  Navigate.pushPage(context, SignIn());
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  pageChangeCallback(int page) {
    print(page);
    setState(() {
      currentPageValue = page;
    });
  }
}
