import 'package:flutter/material.dart';
import 'package:paytalk_mobile/common/theme_config.dart';
import 'package:paytalk_mobile/views/pages/tabs/start.dart';
import 'package:paytalk_mobile/views/pages/tabs/history.dart';
import 'package:paytalk_mobile/views/pages/tabs/subscriptions.dart';
import 'package:paytalk_mobile/views/pages/tabs/profile.dart';
import 'package:line_awesome_icons/line_awesome_icons.dart';
import 'package:paytalk_mobile/views/widgets/custom_bottom_nav/custom_bottom_nav.dart';
import 'package:line_awesome_icons/line_awesome_icons.dart';

class Home extends StatefulWidget {
  Home({Key key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int _currentIndex = 0;
  final List<Widget> _children = [
    Start(),
    History(),
    Subscriptions(),
    ProfileTab()
  ];

  @override
  Widget build(BuildContext context) {
    final bottomNavBar = CustomBottomNav(
      inactiveIconColor: Colors.grey.withOpacity(0.7),
      circleColor: Theme.of(context).backgroundColor,
      activeIconColor: Colors.white,
      textColor:
          Colors.black.withOpacity(0.5), //Theme.of(context).primaryColor,
      tabs: [
        TabData(iconData: LineAwesomeIcons.home, title: 'Home'),
        TabData(iconData: LineAwesomeIcons.history, title: 'History'),
        TabData(iconData: LineAwesomeIcons.dollar, title: 'Subscriptions'),
        TabData(iconData: LineAwesomeIcons.user, title: 'Account'),
      ],
      onTabChangedListener: onTabTapped,
    );

    return Scaffold(
        body: _children[_currentIndex], //
        bottomNavigationBar: bottomNavBar
        // BottomNavigationBar(
        //   selectedItemColor: ThemeConfig.greenBackground,
        //   onTap: onTabTapped, // new
        //   currentIndex: _currentIndex,
        //   items: [
        //     BottomNavigationBarItem(
        //       icon: new Icon(Icons.home),
        //       label: 'Home',
        //     ),
        //     BottomNavigationBarItem(
        //       icon: new Icon(Icons.history),
        //       label: 'History',
        //     ),
        //     BottomNavigationBarItem(
        //       icon: new Icon(LineAwesomeIcons.subscript),
        //       label: 'Subscriptions',
        //     ),
        //     BottomNavigationBarItem(icon: Icon(Icons.person), label: 'Account')
        //   ],
        // ),
        );
  }

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }
}
