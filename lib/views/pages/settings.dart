import 'package:flutter/material.dart';
import 'package:line_awesome_icons/line_awesome_icons.dart';
import 'package:paytalk_mobile/common/images.dart';
import 'package:paytalk_mobile/common/utils.dart';
import '../../services/dialog.service.dart';
import 'package:paytalk_mobile/views/widgets/custom_form_field.dart';
import 'package:paytalk_mobile/views/widgets/custom_button.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'package:paytalk_mobile/services/database.dart';
import 'package:provider/provider.dart';
import 'package:paytalk_mobile/models/user_data.dart';
import 'package:currency_picker/currency_picker.dart';
import 'package:paytalk_mobile/views/widgets/setting_card.dart';
import 'package:paytalk_mobile/common/navigate.dart';
import 'package:paytalk_mobile/views/pages/auth/bio_enrol.dart';
import 'package:firebase_auth/firebase_auth.dart';

class SettingsPage extends StatefulWidget {
  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  TextEditingController name = new TextEditingController();
  TextEditingController email = new TextEditingController();
  TextEditingController phone = new TextEditingController();
  TextEditingController currencyName = new TextEditingController();
  TextEditingController address = new TextEditingController();
  final picker = ImagePicker();
  File image;
  bool loading = false;
  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  bool _voiceRecognition = false;
  bool _voiceEnrolled = false;

  @override
  void dispose() {
    name.dispose();
    email.dispose();
    phone.dispose();
    currencyName.dispose();
    address.dispose();
    super.dispose();
  }

  @override
  void initState() {
    _getUserState();

    super.initState();
  }

  _getUserState() async {
    var user = await DatabaseService.getUserInfo(
        FirebaseAuth.instance.currentUser.uid);
    setState(() {
      _voiceRecognition = user.enableVoiceRecognition;
    });
  }

  @override
  Widget build(BuildContext context) {
    final title = HeaderText(text: "Settings");
    final user = Provider.of<UserData>(context).appUser;
    _voiceEnrolled = user.voiceEnrolled;
    //_voiceRecognition = user.enableVoiceRecognition;

    name.text = "${user.firstName} ${user.lastName}";
    email.text = "${user.email}";
    // phone.text = "${user.}";
    currencyName.text = "${user.currency}";
    address.text = "${user.country}";

    final voice = new SettingCard(
      title: 'Enable Voice Recognition',
      value: _voiceRecognition,
      allowBorder: false,
      changeFunction: _toggleVoiceRecognition,
    );

    final userImage = Container(
      alignment: Alignment.center,
      child: Stack(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(bottom: 10.0, top: 20.0),
            height: 100.0,
            width: 100.0,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: NetworkImage(user.imageUrl),
                fit: BoxFit.cover,
              ),
              shape: BoxShape.circle,
            ),
          ),
          Positioned(
            bottom: 10,
            right: 0,
            child: InkWell(
              onTap: () {
                _updateImage(user.uuid);
              },
              child: Container(
                height: 30.0,
                width: 30.0,
                decoration:
                    BoxDecoration(color: Colors.white, shape: BoxShape.circle),
                child: Icon(
                  LineAwesomeIcons.camera,
                ),
              ),
            ),
          )
        ],
      ),
    );

    final spacer = SizedBox(height: 20.0);

    final nameField = CustomFormField(
      labelText: "Fullname",
      icon: LineAwesomeIcons.user,
      controller: name,
    );

    final emailField = CustomFormField(
      labelText: "Email",
      icon: LineAwesomeIcons.envelope,
      controller: email,
    );

    final phoneField = CustomFormField(
      labelText: "Phone",
      icon: LineAwesomeIcons.mobile,
      controller: phone,
    );

    final currencyField = CustomFormField(
      labelText: "Default Currency",
      icon: LineAwesomeIcons.money,
      controller: currencyName,
      enabled: false,
      suffixText: 'change',
    );

    final addressField = CustomFormField(
      labelText: "Country Code",
      icon: LineAwesomeIcons.map_marker,
      controller: address,
    );

    final button = CustomButton(
      label: "Update",
      onTap: () {
        DialogService().show(
          context: context,
          message: "You account has been updated successfully.",
          type: AlertDialogType.success,
        );
      },
    );

    final form = Column(
      children: <Widget>[
        spacer,
        spacer,
        nameField,
        spacer,
        emailField,
        spacer,
        phoneField,
        spacer,
        InkWell(
            onTap: () {
              _chooseCurrency(user.uuid);
            },
            child: currencyField),
        spacer,
        addressField,
        spacer,
        voice
        //button
      ],
    );

    return Scaffold(
      key: scaffoldKey,
      appBar: CustomAppBar(),
      body: SingleChildScrollView(
        // padding: EdgeInsets.symmetric(horizontal: 20.0),
        padding: EdgeInsets.only(
          left: 20.0,
          right: 20.0,
          top: 20.0,
          bottom: 30.0,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[title, userImage, form],
        ),
      ),
    );
  }

  _updateImage(String userId) async {
    await getImage();
    if (image != null) {
      var url = await AppImages().uploadImage(image, 'userImage');
      if (url.isNotEmpty)
        DatabaseService.updateUserImage(url, userId).then((_) {
          DatabaseService.getUserInfo(userId).then((userInfo) {
            Provider.of<UserData>(context, listen: false).appUser = userInfo;
            setState(() {});
          });
          showInSnackBar('Profile picture updated');
        });
    }
  }

  _toggleVoiceRecognition(bool value) {
    setState(() {
      _voiceRecognition = value;
    });

    DatabaseService.changeVoiceRecognition(value);
    showInSnackBar(
        'Voice recognition is now ${value == true ? 'enabled' : 'disabled'}');
    if (!_voiceEnrolled) Navigate.pushPage(context, BioEnrolment());
  }

  _chooseCurrency(String userId) async {
    showCurrencyPicker(
      context: context,
      showFlag: true,
      currencyFilter: <String>[
        'EUR',
        'GBP',
        'USD',
        'AUD',
        'CAD',
        'JPY',
        'NGN',
        'GHS',
        'RWF',
        'UGX',
        'ZMW',
        'KES'
      ],
      onSelect: (Currency currency) {
        DatabaseService.changeCurrency(currency, userId).then(
            (value) => DatabaseService.getUserInfo(userId).then((userInfo) {
                  Provider.of<UserData>(context, listen: false).appUser =
                      userInfo;
                  setState(() {});
                }));

        showInSnackBar('Default currency updated to ${currency.code}');
      },
    );
  }

  void showInSnackBar(String value) {
    scaffoldKey.currentState.removeCurrentSnackBar();
    scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(value)));
  }

  Future getImage() async {
    loading = true;
    setState(() {});
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    final croppedFile = await AppImages().cropImage(File(pickedFile.path));

    setState(() {
      if (croppedFile != null) {
        image = File(croppedFile.path);
        loading = false;
      } else {
        loading = false;
        showInSnackBar('No image selected.');
      }
    });
  }
}
