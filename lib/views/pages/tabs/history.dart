import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:paytalk_mobile/models/transaction.dart';
import 'package:paytalk_mobile/services/database.dart';
import 'package:paytalk_mobile/views/widgets/smalltext.dart';
import 'package:paytalk_mobile/views/widgets/title_text.dart';
import 'package:intl/intl.dart';
import 'package:recase/recase.dart';

class History extends StatefulWidget {
  @override
  _HistoryState createState() => _HistoryState();
}

class _HistoryState extends State<History> {
  List<Transaction> _transactionsList;
  @override
  void initState() {
    DatabaseService.getTransactions(FirebaseAuth.instance.currentUser.uid)
        .then((transactions) => this.setState(() {
              _transactionsList = transactions;
            }));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final title = Container(
      margin: EdgeInsets.only(top: 10.0, bottom: 10.0, left: 20),
      child: Text("Transaction History",
          style: TextStyle(fontSize: 30, color: Colors.white)),
    );

    final subTitle = Container(
      margin: EdgeInsets.only(bottom: 10.0, left: 20),
      child: Text(
          "Here are your recent payments, subscriptions and other transactions",
          style: TextStyle(fontSize: 15, color: Colors.white)),
    );

    final list = Expanded(
        child: ListView.builder(
            padding: EdgeInsets.only(left: 10, right: 10),
            itemCount: _transactionsList != null ? _transactionsList.length : 0,
            itemBuilder: (context, index) {
              return transactionItem(_transactionsList[index]);
            }));

    return SafeArea(
      child: Scaffold(
        backgroundColor: Theme.of(context).backgroundColor,
        body: Container(
          padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 40),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              title,
              subTitle,
              _transactionsList == null
                  ? Container(
                      child: Center(
                      child: Text('Loading transactions'),
                    ))
                  : _transactionsList.length == 0
                      ? Container(
                          child: Center(
                          child: Text('You have no transactions'),
                        ))
                      : list
            ],
          ),
        ),
      ),
    );

    return SafeArea(
      child: Scaffold(
          appBar: AppBar(
            leading: Icon(Icons.history),
            centerTitle: true,
            title: TitleText(text: 'Transaction History'),
          ),
          body: _transactionsList == null
              ? Container(
                  child: Center(
                  child: Text('Loading transactions'),
                ))
              : _transactionsList.length == 0
                  ? Container(
                      child: Center(
                      child: Text('You have no transactions'),
                    ))
                  : ListView.builder(
                      padding: EdgeInsets.only(top: 10, left: 20, right: 20),
                      itemCount: _transactionsList.length,
                      itemBuilder: (context, index) {
                        return transactionItem(_transactionsList[index]);
                      })),
    );
  }

  Widget transactionItem(Transaction transaction) {
    final item = ListTile(
      title: Padding(
        padding: const EdgeInsets.only(top: 20),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.baseline,
          //mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            CircleAvatar(
                backgroundImage: transaction.imageUrl != null
                    ? NetworkImage(transaction.imageUrl)
                    : AssetImage('assets/images/woman.jpg'),
                radius: 25),
            SizedBox(
              width: 20,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    '${transaction.recipientName.titleCase}',
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  SmallText(
                    color: Colors.black.withOpacity(0.8),
                    fontSize: 12,
                    text:
                        '${transaction.amount.toStringAsFixed(2)} ${transaction.currency}',
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  SmallText(
                    color: Colors.grey.withOpacity(0.8),
                    text:
                        '${DateFormat("MMM dd yyyy").format(transaction.dateOfTransaction.toDate())}',
                  ),
                ],
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                // TitleText(
                //   text:
                //       '${transaction.amount.toStringAsFixed(2)} ${transaction.currency}',
                // ),
                SmallText(
                  color: Colors.green,
                  text: transaction.transactionType,
                )
              ],
            )
          ],
        ),
      ),
    );

    return Padding(
      padding: const EdgeInsets.only(top: 10, bottom: 10),
      child: Container(
        height: 100,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 5,
              blurRadius: 7,
              offset: Offset(0, 3), // changes position of shadow
            ),
          ],
        ),
        child: item,
      ),
    );
  }

  Widget transactionRecord(Transaction transaction) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: ListTile(
        leading: transaction.imageUrl == null
            ? Icon(
                Icons.verified_outlined,
                color: Colors.green[200],
              )
            : CircleAvatar(
                backgroundImage: NetworkImage(transaction.imageUrl),
              ),
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  '${transaction.recipientName.titleCase}',
                  style: TextStyle(
                    fontSize: 15,
                  ),
                ),
                SizedBox(
                  height: 5,
                ),
                SmallText(
                  text: DateFormat("MMM dd yyyy")
                      .format(transaction.dateOfTransaction.toDate())
                      .toString(),
                ),
              ],
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                TitleText(
                  text:
                      '${transaction.amount.toStringAsFixed(2)} ${transaction.currency}',
                ),
                SmallText(
                  text: transaction.transactionType,
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
