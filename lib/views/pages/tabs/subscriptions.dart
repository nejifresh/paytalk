import 'package:flutter/material.dart';
import 'package:paytalk_mobile/models/subscriptions.dart';
import 'package:paytalk_mobile/models/user_data.dart';
import 'package:paytalk_mobile/models/user.dart' as appUser;
import 'package:paytalk_mobile/services/database.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:paytalk_mobile/views/widgets/title_text.dart';
import 'package:paytalk_mobile/views/widgets/smalltext.dart';
import 'package:intl/intl.dart';
import 'package:paytalk_mobile/views/widgets/dialogs.dart';
import 'package:line_awesome_icons/line_awesome_icons.dart';
import 'package:provider/provider.dart';

class Subscriptions extends StatefulWidget {
  @override
  _SubscriptionsState createState() => _SubscriptionsState();
}

class _SubscriptionsState extends State<Subscriptions> {
  List<MySubscriptions> subscriptions;
  appUser.User user;
  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  void initState() {
    var userId = FirebaseAuth.instance.currentUser.uid;
    DatabaseService.getMySubscriptions(userId).then((subs) => this.setState(() {
          subscriptions = subs;
        }));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    user = Provider.of<UserData>(context).appUser;
    final title = Container(
      margin: EdgeInsets.only(top: 30.0, bottom: 10.0),
      child: Text("Subscriptions",
          style: TextStyle(fontSize: 30, color: Colors.white)),
    );

    final subTitle = Container(
      margin: EdgeInsets.only(bottom: 10.0),
      child: Text("These are recurrent payments you subscribed for",
          style: TextStyle(fontSize: 15, color: Colors.white)),
    );

    final list = Expanded(
        child: ListView.builder(
            padding: EdgeInsets.only(top: 10),
            itemCount: subscriptions != null ? subscriptions.length : 0,
            itemBuilder: (context, index) {
              return _subscriptionItem(subscriptions[index]);
            }));

    return Scaffold(
      key: scaffoldKey,
      backgroundColor: Theme.of(context).backgroundColor,
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 40),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            title,
            subTitle,
            subscriptions != null && subscriptions.isNotEmpty
                ? list
                : Center(child: Text('No Available Subscriptions'))
          ],
        ),
      ),
    );
  }

  Widget subItem(MySubscriptions subscription) {
    var image = Container(
        decoration: BoxDecoration(),
        height: 150,
        child: ClipRRect(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(10.0),
            topRight: Radius.circular(10.0),
          ),
          child: Image.asset(
            'assets/images/woman.jpg',
            height: 140.0,
            width: 140.0,
            fit: BoxFit.cover,
          ),
        ));
    return Container(
        padding: EdgeInsets.only(bottom: 20, top: 40),
        height: 250,
        child: ListView(
          children: [
            image,
          ],
        ));
  }

  Widget _subscriptionItem(MySubscriptions subscription) {
    final item = ListTile(
      trailing: IconButton(
          icon: Icon(Icons.cancel, color: Colors.red),
          onPressed: () {
            _cancelSubscription(user.uuid, subscription.subscriptionId);
          }),
      title: Padding(
        padding: const EdgeInsets.only(top: 20),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.baseline,
          //mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            CircleAvatar(
                backgroundImage: subscription.imageUrl != null
                    ? NetworkImage(subscription.imageUrl)
                    : AssetImage('assets/images/woman.jpg'),
                radius: 25),
            SizedBox(
              width: 20,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    '${subscription.planName}',
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  SmallText(
                      text:
                          'Amount: ${subscription.amount.toStringAsFixed(2)} ${subscription.currency}'),
                  SizedBox(
                    height: 5,
                  ),
                  SmallText(
                    color: Colors.grey.withOpacity(0.8),
                    text:
                        'Subscribed On: ${DateFormat("MMM dd yyyy").format(subscription.dateSubscribed.toDate())}',
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );

    return Padding(
      padding: const EdgeInsets.only(top: 10, bottom: 10),
      child: Container(
        height: 100,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 5,
              blurRadius: 7,
              offset: Offset(0, 3), // changes position of shadow
            ),
          ],
        ),
        child: item,
      ),
    );
  }

  _cancelSubscription(String userId, String subscriptionId) {
    showGenericCancel(
        context,
        "Cancel this Subscription?",
        "Are you sure you want to cancel this subscription?",
        LineAwesomeIcons.close, nextAction: () {
      DatabaseService.cancelSubscription(userId, subscriptionId).then((value) =>
          DatabaseService.getMySubscriptions(userId)
              .then((subs) => this.setState(() {
                    subscriptions = subs;
                  })));
    });

    // showInSnackBar('Cancelling your subscription');
  }

  void showInSnackBar(String value) {
    scaffoldKey.currentState.removeCurrentSnackBar();
    scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(value)));
  }
}
