import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import '../../../models/profile_tab_option.dart';
import 'package:paytalk_mobile/common/utils.dart';
import 'package:provider/provider.dart';
import 'package:paytalk_mobile/models/user_data.dart';
import 'package:paytalk_mobile/common/hex_color.dart';
import 'package:paytalk_mobile/services/auth_service.dart';

class ProfileTab extends StatefulWidget {
  @override
  _ProfileTabState createState() => _ProfileTabState();
}

class _ProfileTabState extends State<ProfileTab> {
  @override
  Widget build(BuildContext context) {
    var user = Provider.of<UserData>(context).appUser;
    final double screenHeight = MediaQuery.of(context).size.height;
    final List<ProfileTabOption> options = [
      ProfileTabOption(
        name: "Invite Friends & Family",
        icon: AppIll.invitation,
        onPressed: () => Navigator.of(context).pushNamed('/invitation'),
      ),
      ProfileTabOption(
        name: "Help",
        icon: AppIll.lifeRing,
        onPressed: () => Navigator.of(context).pushNamed('/help'),
      ),
      ProfileTabOption(
          name: "Logout",
          icon: AppIll.logout,
          onPressed: () => AuthService().signOut(context)),
      ProfileTabOption(
        name: "Settings",
        icon: AppIll.settings,
        onPressed: () => Navigator.of(context).pushNamed('/settings'),
      ),
    ];

    final userImage = Container(
      margin: EdgeInsets.only(bottom: 10.0),
      height: 80.0,
      width: 80.0,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: NetworkImage(user.imageUrl),
          fit: BoxFit.cover,
        ),
        shape: BoxShape.circle,
      ),
    );

    final userNameAndLocation = Column(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "${user.firstName} ${user.lastName}",
              style: TextStyle(
                fontSize: 24.0,
                fontWeight: FontWeight.w700,
              ),
            ),
            SizedBox(
              width: 5.0,
            ),
            Icon(
              Icons.check_circle,
              color: Colors.green,
            )
          ],
        ),
        SizedBox(
          height: 1.0,
        ),
        Text(
          " (${user.userName}) ${user.email}",
          style: TextStyle(
            color: Colors.white.withOpacity(0.9),
            fontWeight: FontWeight.w600,
            fontSize: 14.0,
          ),
        ),
      ],
    );

    final top = Container(
      height: screenHeight * 0.3,
      width: double.infinity,
      decoration: BoxDecoration(
          color: Theme.of(context).backgroundColor, //HexColor("EDFFFA"),
          borderRadius: BorderRadius.only(
            bottomRight: Radius.circular(30),
            bottomLeft: Radius.circular(30),
          )),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          userImage,
          userNameAndLocation,
        ],
      ),
    );

    final bottom = Expanded(
      child: Container(
        child: GridView.count(
          padding: EdgeInsets.all(10.0),
          crossAxisCount: 2,
          children: options.map((option) {
            return _buildOption(
              context: context,
              option: option,
            );
          }).toList(),
        ),
      ),
    );

    return Container(
      margin: EdgeInsets.only(top: 20),
      child: Column(children: <Widget>[top, bottom]),
    );
  }

  Widget _buildOption({
    BuildContext context,
    ProfileTabOption option,
  }) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: MaterialButton(
        elevation: 0,
        onPressed: option.onPressed,
        color: Colors.white,
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SvgPicture.asset(
                option.icon,
                height: option.iconSize,
                color: Colors.green[300], //Theme.of(context).primaryColor,
              ),
              SizedBox(
                height: 15.0,
              ),
              Text(
                option.name,
                style: TextStyle(
                  fontSize: 16.0,
                  fontWeight: FontWeight.bold,
                ),
                textAlign: TextAlign.center,
              )
            ],
          ),
        ),
      ),
    );
  }
}
