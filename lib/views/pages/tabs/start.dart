import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:circular_countdown_timer/circular_countdown_timer.dart';
import 'package:contacts_service/contacts_service.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sound/flutter_sound.dart';
import 'package:flutter_sound/public/flutter_sound_player.dart';
import 'package:flutter_sound/public/flutter_sound_recorder.dart';
// import 'package:flutter_tts/flutter_tts_web.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:liquid_progress_indicator/liquid_progress_indicator.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:path_provider/path_provider.dart';
import 'package:paytalk_mobile/common/hex_color.dart';
import 'package:paytalk_mobile/common/strings.dart';
import 'package:paytalk_mobile/models/transfer_recipient.dart';
import 'package:paytalk_mobile/services/network.dart';
import 'package:paytalk_mobile/views/widgets/dialogs.dart';
import 'package:paytalk_mobile/views/widgets/title_text.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:paytalk_mobile/models/user.dart' as appUser;
import 'package:paytalk_mobile/models/user_data.dart';
import 'package:recase/recase.dart';
import 'package:paytalk_mobile/models/transaction.dart';
import 'package:paytalk_mobile/services/database.dart';
import 'package:intl/intl.dart';
import 'package:currency_picker/currency_picker.dart';

import 'package:avatar_glow/avatar_glow.dart';

import 'package:highlight_text/highlight_text.dart';
import 'package:speech_to_text/speech_to_text.dart' as stt;
import 'package:flutter_tts/flutter_tts.dart';
import 'package:auto_search/auto_search.dart';
import 'package:searchfield/searchfield.dart';
import 'package:paytalk_mobile/models/recipient.dart';
import 'package:paytalk_mobile/views/widgets/confirmation.dart';
import 'package:paytalk_mobile/models/payment_recipient.dart';
import 'package:group_radio_button/group_radio_button.dart';
import 'package:line_awesome_icons/line_awesome_icons.dart';
import 'package:paytalk_mobile/common/navigate.dart';
import 'package:paytalk_mobile/views/widgets/confirmshares.dart';
import 'package:paytalk_mobile/views/widgets/confirmvote.dart';

import 'package:paytalk_mobile/views/widgets/flights/flightlist.dart';

class Start extends StatefulWidget {
  @override
  _StartState createState() => _StartState();
}

class _StartState extends State<Start> {
  File _voiceFile;
  String _filePath = '';
  String uid;
  List<int> _voiceBytes;
  bool _recording = false;
  String _token;
  FlutterSoundRecorder _frecorder = FlutterSoundRecorder();
  FlutterSoundPlayer _mPlayer = FlutterSoundPlayer();
  bool _saving = false;
  bool _recorderInit = false;

  List<Transaction> _transactionsList;

  appUser.User user;

  TextEditingController _textEditingController = TextEditingController();

  //Speech to text libariries ****************************************************************
  FlutterTts flutterTts;
  dynamic languages;
  String language;
  double volume = 0.6;
  double pitch = 1.0;
  double rate = 1;
  bool isCurrentLanguageInstalled = false;
  bool _shouldSpeak = true;

  String _newVoiceText;

  bool _acceptVoice = false;

  // TtsState ttsState = TtsState.stopped;

  // get isPlaying => ttsState == TtsState.playing;
  // get isStopped => ttsState == TtsState.stopped;
  // get isPaused => ttsState == TtsState.paused;
  // get isContinued => ttsState == TtsState.continued;

  bool get isIOS => !kIsWeb && Platform.isIOS;
  bool get isAndroid => !kIsWeb && Platform.isAndroid;
  bool get isWeb => kIsWeb;

  //***************************************************************************************** */

  final Map<String, HighlightedWord> _highlights = {
    'send': HighlightedWord(
      onTap: () => print('flutter'),
      textStyle: const TextStyle(
        color: Colors.blue,
        fontWeight: FontWeight.bold,
      ),
    ),
    'donate': HighlightedWord(
      onTap: () => print('voice'),
      textStyle: const TextStyle(
        color: Colors.green,
        fontWeight: FontWeight.bold,
      ),
    ),
    'to': HighlightedWord(
      onTap: () => print('subscribe'),
      textStyle: const TextStyle(
        color: Colors.red,
        fontWeight: FontWeight.bold,
      ),
    ),
    'dollars': HighlightedWord(
      onTap: () => print('like'),
      textStyle: const TextStyle(
        color: Colors.blueAccent,
        fontWeight: FontWeight.bold,
      ),
    ),
    'naira': HighlightedWord(
      onTap: () => print('comment'),
      textStyle: const TextStyle(
        color: Colors.green,
        fontWeight: FontWeight.bold,
      ),
    ),
  };

  stt.SpeechToText _speech;
  bool _isListening = false;
  String _text = 'Press the button and start speaking';
  double _confidence = 1.0;

  List<Contact> contacts;
  List<Contact> unFilteredContacts;
  List<Contact> foundContacts = List();

  String sessionId;
  String _questionText = '';

  List<String> _organizationsList;
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  TextEditingController _amountTextController = TextEditingController();

  String _selectedFrequency = "One time payment";
  List<String> _frequency = ["One time payment", "Weekly", "Monthly", "Yearly"];

  @override
  void initState() {
    getAllOrganizations();
    getToken(FirebaseAuth.instance.currentUser);
    initTts();
    setSessionId();
    getContacts();
    _initFRecorder();
    _mPlayer.openAudioSession().then((value) {});
    DatabaseService.getTransactions(FirebaseAuth.instance.currentUser.uid)
        .then((transactions) => this.setState(() {
              _transactionsList = transactions;
            }));
    // _speech = stt.SpeechToText();

    super.initState();
  }

  setSessionId() {
    setState(() {
      sessionId = DateTime.now().toString();
    });
  }

  getAllOrganizations() async {
    DatabaseService.getOrganizations().then((orgs) => this.setState(() {
          _organizationsList = (orgs);
        }));
  }

  getToken(User user) async {
    var token = await user.getIdToken(false);

    setState(() {
      uid = user.uid;
      _token = token;
      print("TOKEN IS $_token");
    });
  }

  initTts() {
    flutterTts = FlutterTts();

    _getLanguages();

    if (isAndroid) {
      _getEngines();
    }

    // flutterTts.setStartHandler(() {
    //   setState(() {
    //     print("Playing");
    //     ttsState = TtsState.playing;
    //   });
    // });

    // flutterTts.setCompletionHandler(() {
    //   setState(() {
    //     print("Complete");
    //     ttsState = TtsState.stopped;
    //   });
    // });

    // flutterTts.setCancelHandler(() {
    //   setState(() {
    //     print("Cancel");
    //     ttsState = TtsState.stopped;
    //   });
    // });

    if (isWeb || isIOS) {
      flutterTts.setPauseHandler(() {
        setState(() {
          print("Paused");
          //ttsState = TtsState.paused;
        });
      });

      flutterTts.setContinueHandler(() {
        setState(() {
          print("Continued");
          //ttsState = TtsState.continued;
        });
      });
    }

    flutterTts.setErrorHandler((msg) {
      setState(() {
        print("error: $msg");
        // ttsState = TtsState.stopped;
      });
    });
  }

  Future _getLanguages() async {
    languages = await flutterTts.getLanguages;
    if (languages != null) setState(() => languages);
    // var voices = await flutterTts.getVoices;
    // print(voices);
    // await flutterTts.setVoice({"name": "en-NG-language", "locale": "en-NG"});
    //await flutterTts.setVoice({"name": "en-CA-language", "locale": "en-CA"});
  }

  Future _getEngines() async {
    var engines = await flutterTts.getEngines;
    if (engines != null) {
      for (dynamic engine in engines) {
        print(engine);
      }
    }
  }

  Future _speak(String text) async {
    await flutterTts.setVolume(volume);
    await flutterTts.setSpeechRate(rate);
    await flutterTts.setPitch(pitch);

    if (text != null) {
      if (text.isNotEmpty) {
        await flutterTts.awaitSpeakCompletion(true);
        await flutterTts.speak(text);
      }
    }
  }

  Future _stop() async {
    var result = await flutterTts.stop();
    //if (result == 1) setState(() => ttsState = TtsState.stopped);
  }

  Future _pause() async {
    var result = await flutterTts.pause();
    // if (result == 1) setState(() => ttsState = TtsState.paused);
  }

  _listen() async {
    if (!_isListening) {
      bool available = await _speech.initialize(
        onStatus: (val) => print('onStatus: $val'),
        onError: (val) => print('onError: $val'),
      );
      if (available) {
        setState(() => _isListening = true);
        _speech.listen(onResult: (val) {
          setState(() {
            _text = val.recognizedWords;
            if (val.hasConfidenceRating && val.confidence > 0) {
              _confidence = val.confidence;
            }
          });

          print("Done listening/////");
        });
      }
    }

    // else {
    //   setState(() => _isListening = false);
    //   _speech.stop();

    // }
  }

  getContacts() async {
    contacts =
        (await ContactsService.getContacts(withThumbnails: false)).toList();
    unFilteredContacts = contacts;
    setState(() {});
  }

  searchContacts(String firstName, String lastName, String userName) async {
    // await getContacts();
    contacts = unFilteredContacts;
    //print(contacts);
    setState(() {
      contacts.retainWhere((element) =>
          element.displayName.toLowerCase().contains(firstName.toLowerCase()) ||
          element.displayName.toLowerCase().contains(lastName.toLowerCase()) ||
          element.displayName.toLowerCase().contains(userName.toLowerCase()));
    });

    print(
        'Found contacts ${firstName}, ${lastName}, contact list contains ${contacts.length}');

    contacts.forEach((contact) {
      foundContacts.add(contact);
    });

    return contacts.length;
  }

  searchContactsByLetters(
      String firstLetter, TransferRecipient recipient) async {
    await getContacts();
    contacts = unFilteredContacts;
    print(contacts.length);
    contacts.retainWhere((element) => element.displayName
        .toLowerCase()
        .startsWith(firstLetter.toLowerCase()));
    print(
        'Special Search Found contacts for $firstLetter, contact list containing ${contacts.length} contacts');

    setState(() {});

    //showMerchants(context, recipient, contacts: contacts);
  }

  _showMic() async {
    //await _listen();
    await _onRecord();
    Alert(
      style: AlertStyle(
        titleStyle: TextStyle(color: Colors.green[300], fontSize: 20),
      ),
      context: context,
      content: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          // CircularCountDownTimer(
          //   // Countdown duration in Seconds
          //   duration: Platform.isAndroid ? 6 : 6,

          //   // Width of the Countdown Widget
          //   width: MediaQuery.of(context).size.width / 2.5,

          //   // Height of the Countdown Widget
          //   height: MediaQuery.of(context).size.height / 5,

          //   // Default Color for Countdown Timer
          //   color: Colors.white,

          //   // Filling Color for Countdown Timer
          //   fillColor: Colors.red,

          //   // Border Thickness of the Countdown Circle
          //   strokeWidth: 13.0,

          //   // Text Style for Countdown Text
          //   textStyle: TextStyle(
          //       fontSize: 22.0,
          //       color: Colors.green,
          //       fontWeight: FontWeight.bold),

          //   // true for reverse countdown (max to 0), false for forward countdown (0 to max)
          //   isReverse: false,

          //   // Function which will execute when the Countdown Ends
          //   onComplete: () {
          //     // Here, do whatever you want
          //     // print('Countdown Ended');
          //     this.setState(() {
          //       _isListening = false;
          //       //_speech.stop();
          //     });

          //     _onStop();

          //     // _sendSpeech();

          //     Navigator.pop(context);
          //   },
          // ),
          AvatarGlow(
            animate: _isListening,
            glowColor: Colors.green[200], // Theme.of(context).primaryColor,
            endRadius: 75.0,
            duration: const Duration(milliseconds: 2000),
            repeatPauseDuration: const Duration(milliseconds: 100),
            repeat: true,
            child: FloatingActionButton(
              backgroundColor: Colors.green[600],
              onPressed: () {},
              child: Icon(Icons.mic_none_outlined),
            ),
          ),
        ],
      ),
      title: "${AppStrings.appName}",
      desc: "${AppStrings.speakerPrompt}",
      buttons: [
        DialogButton(
          color: Colors.red[300],
          child: Text(
            "${AppStrings.done}",
            style: TextStyle(color: Colors.white, fontSize: 16),
          ),
          onPressed: () {
            _onStop();
            this.setState(() {
              _isListening = false;
              // _speech.stop();
            });
            Navigator.pop(context);
          },
          width: 200,
        )
      ],
    ).show();
  }

  _initFRecorder() async {
    if (await Permission.microphone.request().isGranted) {
      // Either the permission was already granted before or the user just granted it.
    } else {
      Map<Permission, PermissionStatus> statuses = await [
        Permission.microphone,
        Permission.storage,
      ].request();
    }

    await openTheRecorder();
  }

  Future<void> openTheRecorder() async {
    var status = await Permission.microphone.request();
    if (status != PermissionStatus.granted) {
      throw RecordingPermissionException('Microphone permission not granted');
    }

    var tempDir = await getTemporaryDirectory();
    _filePath = '${tempDir.path}/soundfile.wav';
    var outputFile = File(_filePath);
    if (outputFile.existsSync()) {
      await outputFile.delete();
    }
    await _frecorder.openAudioSession();
    print('Recorder Initialized');
    setState(() {
      _recorderInit = true;
      _voiceFile = outputFile;
    });
  }

  //FlutterSound recorder
  void _onRecord() async {
    if (!_recording) {
      // showModalBottomSheet(
      //     backgroundColor:
      //         Colors.green[200], //Color(0xFF212121).withOpacity(0.95),
      //     isDismissible: false,
      //     context: context,
      //     builder: (BuildContext bc) {
      //       return buildFirst();
      //     });

      await _frecorder.startRecorder(
        toFile: _filePath,
        sampleRate: 16000,
        numChannels: 1,
        codec: Platform.isAndroid ? Codec.pcm16WAV : Codec.pcm16WAV,
      );

      setState(() {
        _recording = true;
        _isListening = true;
      });
    }
  }

  void _onStop() async {
    await _frecorder.stopRecorder();

    print(_filePath);

    File voiceFile = File(_filePath);
    List<int> theBytes = await voiceFile.readAsBytes();
    print('File Size is ${_voiceFile.lengthSync()}');
    setState(() {
      _voiceFile = voiceFile;
      _recording = false;
      _saving = true;
      _voiceBytes = theBytes;
    });

    //_sendPayment();
    if (user.enableVoiceRecognition) _verifyVoice();
    _sendGoogleStt();
    // await _mPlayer.startPlayer(
    //     fromURI: _filePath,
    //     codec: Codec.pcm16WAV,
    //     whenFinished: () {
    //       setState(() {
    //         print('Done playing');
    //       });
    //     });
  }

  // _sendSpeech() async {
  //   getContacts();
  //   try {
  //     var result =
  //         await Network.speechPayment(AppStrings.speechPayUrl, _token, _text);
  //     setState(() {
  //       _saving = true;
  //     });

  //     if (result.command.toString().contains('donate')) {
  //       //this is a donation, add the donation response and the contact list to it.
  //       TransferRecipient recipient = TransferRecipient(
  //           command: result.command, donationResponse: result);

  //       if (result.charity.length > 0) {
  //         showMerchants(context, recipient);
  //       } else {
  //         showNoMerchants(context, 'No Charities found with this name',
  //             'No Charities Found');
  //       }
  //     } else {
  //       //this is a transfer
  //       TransferRecipient recipient = TransferRecipient(
  //           command: result.command, merchantResponse: result);
  //       //   _showResults(recipient);
  //       if (result.merchant.length > 0) {
  //         print(recipient.merchantResponse.merchant[0].firstName);

  //         searchContacts(
  //             recipient.merchantResponse.merchant[0].firstName,
  //             recipient.merchantResponse.merchant[0].lastName,
  //             recipient.merchantResponse.merchant[0].username);
  //         // searchContacts(
  //         //     getLastWord(_text), getLastWord(_text), getLastWord(_text));

  //         showMerchants(context, recipient, contacts: contacts);
  //       } else {
  //         if (getLastWord(_text).isNotEmpty) {
  //           //lets do a contacts only search
  //           print('Last word in query is ${getLastWord(_text)}');
  //           int contactCount = searchContacts(
  //               getLastWord(_text), getLastWord(_text), getLastWord(_text));

  //           //if no contacts exist with for this name, lets do some magic search
  //           if (contactCount > 0) {
  //             showMerchants(context, recipient, contacts: contacts);
  //           } else {
  //             //now do the magic
  //             var lastWord = getLastWord(_text);
  //             var chars = lastWord.split("");
  //             print(chars.first);
  //             searchContactsByLetters(chars.first, recipient);
  //           }
  //         } else {
  //           showAuthFailed(context, 'No Results',
  //               'No results were returned or an invalid command was used');
  //         }

  //         // showNoMerchants(context, 'No users or merchants found with this name',
  //         //     'No User Accounts');
  //       }
  //     }

  //     // else {
  //     //   showAuthFailed(context, 'Authentication Failed',
  //     //       'We were unable to authenticate you');
  //     // }

  //     setState(() {
  //       _saving = false;
  //     });
  //   } catch (err) {
  //     setState(() {
  //       _saving = false;
  //     });
  //     print(err.toString());
  //     // if (err.toString().contains('Voice does not match'))
  //     showAuthFailed(context, 'No Results',
  //         'No results were returned or an invalid command was used');
  //   }

  // print('Voice enrolment status is ${result.enrollmentStatus}');
  // if (result.enrollmentStatus == 'Enrolled') {
  //   String message =
  //       "Congratulations. You have successfully created your PayTalk VoicePrint. You can now issue commands using your voice";
  //   Function nextAction = () => updateVoiceEnrolment();

  //   showSuccessDialog(context, message, 'Success', nextAction: nextAction);
  // }
  //}

  String getLastWord(String text) {
    String s = text.trim();
    var words = s.split(" ");
    String lastWord = words[words.length - 1];
    return lastWord;
  }

  _showResults(TransferRecipient recipient) {
    showModalBottomSheet(
        backgroundColor: Color(0xFF212121).withOpacity(0.95),
        isDismissible: false,
        context: context,
        builder: (BuildContext bc) {
          return _buildRecipients(recipient);
        });
  }

  _getOrgInfo(String name) async {
    var org = await DatabaseService.getOrgByName(name);
    setState(() {
      _saving = false;
    });

    print(org.category);
    _showRequestForPaymentDetails(org);
  }

  _showRequestForPaymentDetails(Organizations org) {
    Alert(
        context: context,
        title: "Amount Required",
        content: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            TextField(
              controller: _amountTextController,
              decoration: InputDecoration(
                  icon: Icon(Icons.money_outlined),
                  labelText: 'Enter the amount',
                  suffixText: '${user.currency}'),
            ),
            SizedBox(
              height: 10,
            ),
            //TODO: FINISH THE WIDGET OR FREQUENCY
            // Text('Payment Frequency'),
            // SizedBox(
            //   height: 10,
            // ),
            // ListTile(
            //   title: Text('${_frequency[0]}'),
            //   leading: Radio(
            //     value: _frequency[0],
            //     groupValue: _selectedFrequency,
            //     onChanged: (value) {
            //       setState(() {
            //         _selectedFrequency = value;
            //       });
            //     },
            //   ),
            // ),
            // ListTile(
            //   title: Text('${_frequency[1]}'),
            //   leading: Radio(
            //     value: _frequency[1],
            //     groupValue: _selectedFrequency,
            //     onChanged: (value) {
            //       setState(() {
            //         _selectedFrequency = value;
            //       });
            //     },
            //   ),
            // ),
          ],
        ),
        buttons: [
          DialogButton(
            color: Colors.green[300],
            onPressed: () {
              if (_amountTextController.text.isEmpty) {
                showNoMerchants(context, 'Please enter an amount', 'Error');
              } else {
                //go to confirmation page
                var recipient =
                    PaymentRecipient(organization: org, isOrg: true);

                Navigator.pop(context);

                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => ConfirmationPage(
                          recipient: recipient,
                          currencyName: user.currency,
                          amount: double.parse(_amountTextController.text),
                        )));
              }
            },
            child: Text(
              "NEXT",
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
          )
        ]).show();
  }

  _buildSearchField() {
    final List<String> names = [
      "Retrieving organizations..",
    ];
    // print('Total organizations in list ${_organizationsList.length}');
    return Container(
        width: MediaQuery.of(context).size.width - 50,
        //margin: EdgeInsets.symmetric(vertical: 5),
        padding: const EdgeInsets.all(10.0),
        child: SearchField(
          suggestions:
              _organizationsList != null && _organizationsList.isNotEmpty
                  ? _organizationsList
                  : names,
          hint: 'Search by name here..',
          searchStyle:
              TextStyle(fontSize: 15, color: Colors.white.withOpacity(0.9)),
          searchInputDecoration: InputDecoration(
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.black38, width: 1.0),
                borderRadius: BorderRadius.circular(20),
              ),
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(
                    color: Colors.white.withOpacity(0.5), width: 1.0),
                borderRadius: BorderRadius.circular(20),
              ),
              prefixIcon: Icon(
                Icons.search,
                color: Colors.green,
              ),
              hintText: 'Search by name here..',
              hintStyle: TextStyle(color: Colors.white.withOpacity(0.5))),
          maxSuggestionsInViewPort: 3,
          itemHeight: 50,
          onTap: (org) {
            setState(() {
              _saving = true;
            });
            _getOrgInfo(org);
          },
        ));
    return Container(
      width: MediaQuery.of(context).size.width - 100,
      margin: EdgeInsets.symmetric(vertical: 5),
      decoration: BoxDecoration(
          border: Border.all(color: Color(0XFFe4e4e4)),
          borderRadius: BorderRadius.circular(10)),
      child: TextField(
        controller: _textEditingController,
        decoration: InputDecoration(
            border: InputBorder.none,
            prefixIcon: Icon(
              Icons.search,
              color: Colors.green,
            ),
            hintText: 'Search by name here..',
            hintStyle: TextStyle(color: Color(0XFF999999))),
      ),
    );
  }

  Widget _buildRecipients(TransferRecipient recipient) {
    return Container(
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            SizedBox(height: 20.0),
            Text('Search Results',
                textAlign: TextAlign.center, style: TextStyle(fontSize: 22)),
            DefaultTabController(
                length: 2, // length of tabs
                initialIndex: 0,
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      Container(
                        child: TabBar(
                          labelColor: Colors.green,
                          unselectedLabelColor: Colors.black,
                          tabs: [
                            Tab(text: 'Members'),
                            Tab(text: 'Contacts Found'),
                          ],
                        ),
                      ),
                      Container(
                          height: 300, //height of TabBarView
                          decoration: BoxDecoration(
                              border: Border(
                                  top: BorderSide(
                                      color: Colors.grey, width: 0.5))),
                          child: TabBarView(children: <Widget>[
                            Container(
                              child: Center(
                                child: Text('Display Members',
                                    style: TextStyle(
                                        fontSize: 22,
                                        fontWeight: FontWeight.bold)),
                              ),
                            ),
                            Container(
                              child: Center(
                                child: Text('Display Contacts',
                                    style: TextStyle(
                                        fontSize: 22,
                                        fontWeight: FontWeight.bold)),
                              ),
                            ),
                          ]))
                    ])),
          ]),
    );
  }

  _verifyVoice() async {
    var voiceVerificationResult = await Network.verifyVoice(
      AppStrings.voiceVerifyUrl,
      _token,
      _voiceBytes,
      _voiceFile,
    );

    if (jsonDecode(voiceVerificationResult)["score"] > 0.45) {
      print('Result is accepted');
      setState(() {
        _acceptVoice = true;
      });
    } else {
      print('rejected result');
      setState(() {
        _acceptVoice = false;
      });
    }
  }

  _sendGoogleStt() async {
    try {
      var fulfillment = await Network.sendGoogleSpeech(
          AppStrings.googlespeechUrl,
          _token,
          _voiceBytes,
          _voiceFile,
          sessionId);

      setState(() {
        _text = fulfillment.text;
        _saving = false;
      });

      if (fulfillment.allRequiredParamsPresent) {
        if (fulfillment.isPurchase && !fulfillment.isVote) {
          //this is a purchase request
          print(
              'Stocks to buy is ${fulfillment.stock}, amount is ${fulfillment.number}');
          Navigate.pushPage(
              context,
              ConfirmShares(
                  total: fulfillment.number, stock: fulfillment.stock));
        } else if (fulfillment.isVote) {
          //this is a vote
          print('You are voting for ${fulfillment.candidate}, on bbn');
          Navigate.pushPage(
              context,
              ConfirmVote(
                  candidate: fulfillment.candidate,
                  gameShow: 'big brother naija'));
        } else if (fulfillment.isFlight) {
          //this is a vote
          print(
              'You are flying from  ${fulfillment.startCity} to ${fulfillment.destinationCity}');
          Navigate.pushPage(context,
              FlightList(fulfillment.startCity, fulfillment.destinationCity));
        } else {
          //pass the recipient to the Api

          var recipients = await Network.getRecipients(
              AppStrings.recipientsUrl,
              fulfillment.recipient,
              fulfillment.amount,
              fulfillment.currency,
              fulfillment.action);
          print('Done getting recipients');
          //now lets alos find contacts with this information
          //print('Recipient amount is ${recipients.amount}');

          if (recipients.users.isNotEmpty ||
              recipients.organizations.isNotEmpty) {
            print('there are ${recipients.organizations.length} organizations');
            // recipients.organizations.forEach((organization) async {
            //   await searchContacts(
            //       organization.name, organization.name, organization.country);
            // });

            recipients.users.forEach((user) async {
              await searchContacts(
                  user.firstName, user.username, user.lastName);
            });

            TransferRecipient recipient = TransferRecipient(
                command: recipients.action,
                recipients: recipients,
                amount: fulfillment.amount,
                fulfillment: fulfillment,
                currency: fulfillment.currency,
                contacts: contacts);

            if (user.enableVoiceRecognition && !_acceptVoice) {
              showGenericFailure(
                  context,
                  'Voice Authentication Failed!',
                  'Voice Authentication was not successful',
                  LineAwesomeIcons.microphone_slash);
              // showNoMerchants(
              //     context,
              //     'We were unable to authenticate your voice.',
              //     'Authentication Failure!');
              return;
            }

            showMerchants(context, recipient, contacts: foundContacts);
          } else {
            showNoMerchants(context,
                'No organizations or users matched your query', 'No Accounts');
          }
        }

        // else {
        //   if (getLastWord(_text).isNotEmpty) {
        //     //lets do a contacts only search
        //     print('Last word in query is ${getLastWord(_text)}');
        //     int contactCount = searchContacts(
        //         getLastWord(_text), getLastWord(_text), getLastWord(_text));

        //     //if no contacts exist with for this name, lets do some magic search
        //     if (contactCount > 0) {
        //       showMerchants(context, recipient, contacts: contacts);
        //     } else {
        //       //now do the magic
        //       var lastWord = getLastWord(_text);
        //       var chars = lastWord.split("");
        //       print(chars.first);
        //       searchContactsByLetters(chars.first, recipient);
        //     }
        //   } else {
        //     showAuthFailed(context, 'No Results',
        //         'No results were returned or an invalid command was used');
        //   }

        //   // showNoMerchants(context, 'No users or merchants found with this name',
        //   //     'No User Accounts');
        // }

        // showMerchants(context, recipients);

      } else {
        setState(() {
          _questionText = fulfillment.text;
          _acceptVoice = true;
        });
        _showQuestionAlert(fulfillment.text);
      }
    } catch (e) {
      print(e.toString());
      var errorMessage = jsonDecode(e.toString());
      setState(() {
        _saving = false;
      });
      showNoMerchants(
          context, '${errorMessage.message}', 'Unable to process transaction');
      throw Exception(e.toString());
    }
  }

  _answerBotQuestion() async {
    await _onRecord();
    return Alert(
      style: AlertStyle(
        titleStyle: TextStyle(color: Colors.green[300], fontSize: 20),
      ),
      context: context,
      content: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          AvatarGlow(
            animate: _isListening,
            glowColor: Colors.green[200], // Theme.of(context).primaryColor,
            endRadius: 75.0,
            duration: const Duration(milliseconds: 2000),
            repeatPauseDuration: const Duration(milliseconds: 100),
            repeat: true,
            child: FloatingActionButton(
              backgroundColor: Colors.green[600],
              onPressed: () {},
              child: Icon(Icons.mic_none_outlined),
            ),
          ),
        ],
      ),
      title: "${AppStrings.appName}",
      desc:
          "Please answer the question below. Then ${AppStrings.speakerPrompt}\n\n$_questionText",
      buttons: [
        DialogButton(
          color: Colors.red[300],
          child: Text(
            "${AppStrings.done}",
            style: TextStyle(color: Colors.white, fontSize: 16),
          ),
          onPressed: () {
            _onStop();
            this.setState(() {
              _isListening = false;
              // _speech.stop();
            });
            Navigator.pop(context);
          },
          width: 200,
        )
      ],
    ).show();
  }

  _showQuestionAlert(String text) async {
    if (_shouldSpeak) _speak(text);
    showQuestionDialog(context, text, nextAction: _answerBotQuestion);
  }

  _sendPayment() async {
    try {
      var result = await Network.voicePayment(
          AppStrings.voicePayUrl, _token, _voiceBytes, _voiceFile);
      setState(() {
        _saving = true;
      });

      if (result.command.toString().contains('donate')) {
        //this is a donation
        if (result.score > 0.4 && result.charity.length > 0) {
          showMerchants(context, result);
        } else if (result.score > 0.4 && result.charity.length == 0) {
          showNoMerchants(context, 'No Charities found with this name',
              'No Charities Found');
        }
      } else {
        //this is a transfer
        if (result.score > 0.4 && result.merchant.length > 0) {
          showMerchants(context, result);
        } else if (result.score > 0.4 && result.merchant.length == 0) {
          showNoMerchants(context, 'No users or merchants found with this name',
              'No User Accounts');
        }
      }

      // else {
      //   showAuthFailed(context, 'Authentication Failed',
      //       'We were unable to authenticate you');
      // }

      setState(() {
        _saving = false;
      });
    } catch (err) {
      setState(() {
        _saving = false;
      });
      print(err.toString());
      if (err.toString().contains('Voice does not match'))
        showAuthFailed(context, 'Authentication Failed',
            'Voice Authentication Failed or Invalid User');
    }

    // print('Voice enrolment status is ${result.enrollmentStatus}');
    // if (result.enrollmentStatus == 'Enrolled') {
    //   String message =
    //       "Congratulations. You have successfully created your PayTalk VoicePrint. You can now issue commands using your voice";
    //   Function nextAction = () => updateVoiceEnrolment();

    //   showSuccessDialog(context, message, 'Success', nextAction: nextAction);
    // }
  }

  _showTransfer() {
    _onRecord();
  }

  Widget buildFirst() {
    return Center(
        child: Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 20, bottom: 20),
          child: Text(
            '${AppStrings.appName}',
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
        ),
        Icon(
          Icons.mic,
          color: Colors.red,
          size: 40,
        ),
        Center(
          child: Text(
            'Say the magic words before the timer completes',
            style: GoogleFonts.abel(
                textStyle: TextStyle(color: Colors.white, fontSize: 14)),
          ),
        ),
        SizedBox(height: 5),
        Center(
          child: Text(
            'SEND/DONATE <AMOUNT> TO <RECIPIENT>',
            style: GoogleFonts.abel(
                textStyle: TextStyle(color: Colors.white, fontSize: 20)),
          ),
        ),
        CircularCountDownTimer(
          // Countdown duration in Seconds
          duration: Platform.isAndroid ? 6 : 6,

          // Width of the Countdown Widget
          width: MediaQuery.of(context).size.width / 2.5,

          // Height of the Countdown Widget
          height: MediaQuery.of(context).size.height / 3,

          // Default Color for Countdown Timer
          color: Colors.white,

          // Filling Color for Countdown Timer
          fillColor: Colors.red,

          // Border Thickness of the Countdown Circle
          strokeWidth: 13.0,

          // Text Style for Countdown Text
          textStyle: TextStyle(
              fontSize: 22.0, color: Colors.white, fontWeight: FontWeight.bold),

          // true for reverse countdown (max to 0), false for forward countdown (0 to max)
          isReverse: false,

          // Function which will execute when the Countdown Ends
          onComplete: () {
            // Here, do whatever you want
            // print('Countdown Ended');
            this.setState(() {
              _recording = false;
              _onStop();
            });

            Navigator.pop(context);
          },
        ),
      ],
    ));
  }

  @override
  void dispose() {
    // Be careful : you must `close` the audio session when you have finished with it.
    _frecorder.closeAudioSession();
    _frecorder = null;
    flutterTts.stop();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    user = Provider.of<UserData>(context).appUser;

    return ModalProgressHUD(
      inAsyncCall: _saving,
      progressIndicator: LiquidLinearProgressIndicator(
        value: 0.25, // Defaults to 0.5.
        valueColor: AlwaysStoppedAnimation(
            Colors.green[300]), // Defaults to the current Theme's accentColor.
        backgroundColor:
            Colors.white, // Defaults to the current Theme's backgroundColor.
        borderColor: Colors.green,
        borderWidth: 5.0,
        borderRadius: 12.0,
        direction: Axis
            .vertical, // The direction the liquid moves (Axis.vertical = bottom to top, Axis.horizontal = left to right). Defaults to Axis.horizontal.
        center: Text("Processing request..."),
      ),
      child: GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
          _textEditingController.clear();
        },
        child: Container(
            child: Stack(
          children: [
            Container(
              height: 310,
              decoration: BoxDecoration(
                  color:
                      Theme.of(context).backgroundColor, //HexColor("EDFFFA"),
                  borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(30),
                    bottomLeft: Radius.circular(30),
                  )),
            ),
            SafeArea(
              child: ListView(
                children: [
                  SizedBox(
                    height: 60,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      CircleAvatar(
                        radius: 40,
                        backgroundImage: NetworkImage('${user.imageUrl}'),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Center(
                      child: Text(
                    '${user.firstName.titleCase} ${user.lastName.titleCase}\n(${user.userName.titleCase}) ${user.email}',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.white.withOpacity(0.9)),
                  )),
                  SizedBox(
                    height: 10,
                  ),
                  Center(
                      child: _organizationsList != null &&
                              _organizationsList.isNotEmpty
                          ? _buildSearchField()
                          : Container()),
                  SizedBox(
                    height: 20,
                  ),
                  // Padding(
                  //   padding: const EdgeInsets.only(left: 25.0, right: 10),
                  //   child: TitleText(
                  //     text: "Operations",
                  //   ),
                  // ),
                  // SizedBox(
                  //   height: 10,
                  // ),
                  // _operationsWidget(),
                  // SizedBox(
                  //   height: 40,
                  // ),
                  Padding(
                    padding: const EdgeInsets.only(left: 25.0, right: 10),
                    child: TitleText(
                      text: "Recent Transactions",
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 25.0, right: 10),
                    child: _transectionList,
                  ),
                ],
              ),
            ),
            Positioned(
              bottom: 30,
              right: 30,
              child: FloatingActionButton(
                backgroundColor: Colors.red[400],
                onPressed: () {
                  _showMic();
                  //_showTransfer();
                },
                child: Icon(Icons.mic_none_outlined),
              ),
            ),
          ],
        )),
      ),
    );
  }

  Widget _operationsWidget() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        _icon(Icons.transfer_within_a_station, "Transfer"),
        _icon(Icons.phone, "Airtime"),
        _icon(Icons.payment, "Pay Bills"),
        _icon(Icons.healing, "Donate"),
      ],
    );
  }

  Widget get _transectionList {
    return _transactionsList == null || _transactionsList.length == 0
        ? Container(
            child: Center(
            child: Text('You have no recent transactions'),
          ))
        : Container(
            height: 400,
            child: ListView.builder(
                padding: EdgeInsets.only(top: 10),
                itemCount: _transactionsList.isNotEmpty ? 5 : 1,
                itemBuilder: (context, index) {
                  return _transactionRecord(_transactionsList[index]);
                }),
          );

    // return Column(
    //   children: <Widget>[
    //     _transection("Flight Ticket", "23 Feb 2020"),
    //     _transection("Electricity Bill", "25 Feb 2020"),
    //     _transection("Flight Ticket", "03 Mar 2020"),
    //   ],
    // );
  }

  Widget _transactionRecord(Transaction transaction) {
    return ListTile(
      leading: Container(
        height: 50,
        width: 50,
        // decoration: BoxDecoration(
        //   color: Colors.green[300],
        //   borderRadius: BorderRadius.all(Radius.circular(10)),
        // ),
        child: transaction.imageUrl == null || transaction.imageUrl.isEmpty
            ? Icon(Icons.hd, color: Colors.white)
            : CircleAvatar(
                backgroundImage: NetworkImage(transaction.imageUrl),
              ),
      ),
      contentPadding: EdgeInsets.symmetric(),
      title: TitleText(
        text: transaction.recipientName.titleCase,
        fontSize: 14,
      ),
      subtitle: Text(DateFormat("MMM dd yyyy")
          .format(transaction.dateOfTransaction.toDate())
          .toString()),
      trailing: Container(
          height: 30,
          width: 60,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            color: Colors.transparent,
            borderRadius: BorderRadius.all(Radius.circular(10)),
          ),
          child: Text(
              '${transaction.amount.toStringAsFixed(2)} ${transaction.currency}',
              style: GoogleFonts.muli(
                  fontSize: 12,
                  fontWeight: FontWeight.bold,
                  color: Colors.green))),
    );
  }

  Widget _icon(IconData icon, String text) {
    return Column(
      children: <Widget>[
        GestureDetector(
          onTap: () {
            Navigator.pushNamed(context, '/transfer');
          },
          child: Container(
            height: 80,
            width: 80,
            margin: EdgeInsets.symmetric(vertical: 10),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(20)),
                boxShadow: <BoxShadow>[
                  BoxShadow(
                      color: Color(0xfff3f3f3),
                      offset: Offset(5, 5),
                      blurRadius: 10)
                ]),
            child: Icon(
              icon,
              color: Colors.green,
            ),
          ),
        ),
        Text(text,
            style: GoogleFonts.muli(
                textStyle: Theme.of(context).textTheme.display1,
                fontSize: 15,
                fontWeight: FontWeight.w600,
                color: Color(0xff76797e))),
      ],
    );
  }
}
