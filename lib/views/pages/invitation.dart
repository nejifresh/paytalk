import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:paytalk_mobile/common/utils.dart';
import 'package:paytalk_mobile/views/widgets/dialogs.dart';

class InvitationPage extends StatefulWidget {
  @override
  _InvitationPageState createState() => _InvitationPageState();
}

class _InvitationPageState extends State<InvitationPage> {
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  String email;

  @override
  Widget build(BuildContext context) {
    final image = SvgPicture.asset(
      AppIll.invitationSvg,
      height: 300.0,
    );

    final title = Container(
      margin: EdgeInsets.only(top: 40.0),
      child: HeaderText(text: "Invite your friends"),
    );

    // final subtitle = Container(
    //   alignment: Alignment.center,
    //   margin: EdgeInsets.only(top: 20.0, bottom: 20.0),
    //   child: RichText(
    //     textAlign: TextAlign.center,
    //     text: TextSpan(
    //       text: "You can earn ",
    //       style: TextStyle(
    //         color: CustomColors.primaryDarkColor,
    //         //fontFamily: AppFonts.primaryFont,
    //         fontWeight: FontWeight.w600,
    //         fontSize: 17.0,
    //       ),
    //       children: <TextSpan>[
    //         TextSpan(
    //           text: 'money',
    //           style: TextStyle(
    //             color: Theme.of(context).primaryColor,
    //           ),
    //         ),
    //         TextSpan(
    //           text: ' when you invite 3 friends.',
    //           style: TextStyle(),
    //         ),
    //       ],
    //     ),
    //   ),
    // );

    final emailField = Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 5.0),
      decoration: BoxDecoration(
        color: Colors.grey.withOpacity(0.2),
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: TextFormField(
        onSaved: (String val) {
          this.setState(() {
            email = val;
          });
        },
        decoration: InputDecoration(
          hintText: "EMAIL ADDRESS",
          hintStyle: TextStyle(
            color: Colors.grey.withOpacity(0.8),
            fontWeight: FontWeight.w600,
          ),
          border: InputBorder.none,
        ),
      ),
    );

    final inviteCode = Container(
      padding: EdgeInsets.only(top: 20.0, bottom: 20.0),
      child: Column(
        children: [
          Text(
            "4854WAF",
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 18.0,
                color: Colors.black38),
          ),
          Text(
            "Your invite code",
            style: TextStyle(
              fontWeight: FontWeight.w600,
              color: Colors.grey.withOpacity(0.8),
            ),
          ),
        ],
      ),
    );

    final button = MaterialButton(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(5.0),
      ),
      height: 50.0,
      color: Colors.green[300], //Theme.of(context).primaryColor,
      elevation: 4.0,
      onPressed: () {
        //invite someone
        FormState form = formKey.currentState;
        form.save();
        if (email.isNotEmpty)
          showSuccessDialog(
              context, 'You have successfully invited a friend', 'success');
      },
      child: Center(
        child: Text(
          'SEND INVITE',
          style: TextStyle(color: Colors.white),
        ),
      ),
    );

    return Scaffold(
      appBar: CustomAppBar(),
      body: Container(
        child: SingleChildScrollView(
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          child: Column(
            children: [
              image,
              title,
              SizedBox(
                height: 10,
              ),
              //subtitle,
              Form(key: formKey, child: emailField),
              inviteCode,
              button,
              Utils.verticalSpacer(space: 40.0),
            ],
          ),
        ),
      ),
    );
  }
}
