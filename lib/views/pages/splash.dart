import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:paytalk_mobile/common/navigate.dart';
import 'package:paytalk_mobile/common/strings.dart';
import 'package:paytalk_mobile/services/database.dart';
import 'package:paytalk_mobile/views/pages/auth/bio_enrol.dart';
import 'package:paytalk_mobile/views/pages/auth/create_profile.dart';
import 'package:paytalk_mobile/views/pages/onboarding.dart';
import 'package:paytalk_mobile/views/widgets/flights/flightlist.dart';
import 'package:provider/provider.dart';
import 'package:paytalk_mobile/models/user.dart' as appUser;
import 'package:paytalk_mobile/models/user_data.dart';
import 'package:paytalk_mobile/models/appconfig.dart';
import 'package:permission_handler/permission_handler.dart';

import 'home.dart';

class Splash extends StatefulWidget {
  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  startTimeout() {
    return new Timer(Duration(seconds: 2), handleTimeout);
  }

  void handleTimeout() {
    changeScreen();
  }

  changeScreen() async {
    if (FirebaseAuth.instance.currentUser == null)
      Navigate.pushPageReplacement(context, Onboarding());

    var config = await DatabaseService.getAppConfig();

    var userInfo = await DatabaseService.getUserInfo(
        FirebaseAuth.instance.currentUser.uid);
    if (userInfo != null) {
      userInfo.isEnrolled
          ? goHome(userInfo, config) //this is home
          : userInfo.isEnrolled &&
                  !userInfo.voiceEnrolled &&
                  userInfo.enableVoiceRecognition
              ? Navigate.pushPage(context, BioEnrolment())
              : Navigate.pushPage(context, CreateProfile());
    }
  }

  goHome(appUser.User user, AppConfig config) {
    Provider.of<UserData>(context, listen: false).appUser = user;
    Provider.of<UserData>(context, listen: false).config = config;
    Navigate.pushPage(context, Home());
  }

  @override
  void initState() {
    getPermissions();
    super.initState();
    startTimeout();
  }

  getPermissions() async {
    if (await Permission.contacts.request().isGranted) {
      // Either the permission was already granted before or the user just granted it.
    } else {
      Map<Permission, PermissionStatus> statuses = await [
        Permission.contacts,
        Permission.microphone,
        Permission.storage,
      ].request();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: Center(
        child: Image.asset('assets/images/logotrans.png'),
        // child: Text(
        //   AppStrings.appName,
        //   style: TextStyle(
        //     color: Colors.white,
        //     fontSize: 30.0,
        //     fontWeight: FontWeight.normal,
        //   ),
        // ),
      ),
    );
  }
}
