import 'dart:io';

import 'package:circular_countdown_timer/circular_countdown_timer.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sound/flutter_sound.dart';
import 'package:flutter_sound/public/flutter_sound_recorder.dart';
import 'package:flutter_sound/public/tau.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:path_provider/path_provider.dart';
import 'package:paytalk_mobile/common/navigate.dart';
import 'package:paytalk_mobile/common/strings.dart';
import 'package:paytalk_mobile/services/database.dart';
import 'package:paytalk_mobile/services/network.dart';
import 'package:paytalk_mobile/views/widgets/custom_button.dart';
import 'package:paytalk_mobile/views/widgets/dialogs.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';
import 'package:paytalk_mobile/models/user_data.dart';
import 'package:paytalk_mobile/views/widgets/master_button.dart';
import 'package:avatar_glow/avatar_glow.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:paytalk_mobile/services/dialog.service.dart';

class BioEnrolment extends StatefulWidget {
  BioEnrolment({Key key}) : super(key: key);

  @override
  _BioEnrolmentState createState() => _BioEnrolmentState();
}

class _BioEnrolmentState extends State<BioEnrolment> {
  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  String uid;
  File _voiceFile;
  String _filePath = '';
  List<int> _voiceBytes;
  bool _recording = false;
  bool _recorderInit = false;
  String _token;
  FlutterSoundRecorder _frecorder = FlutterSoundRecorder();
  FlutterSoundPlayer _mPlayer = FlutterSoundPlayer();
  bool _saving = false;
  String _phrase = 'The world is full of beautiful places';
  bool _isListening = false;

  getToken(User user) async {
    var token = await user.getIdToken(true);

    setState(() {
      uid = user.uid;
      _token = token;
      print("TOKEN IS $_token");
    });
  }

  @override
  void initState() {
    getToken(FirebaseAuth.instance.currentUser);
    _initFRecorder();
    _mPlayer.openAudioSession().then((value) {});
    super.initState();
  }

  _initFRecorder() async {
    if (await Permission.microphone.request().isGranted) {
      // Either the permission was already granted before or the user just granted it.
    } else {
      Map<Permission, PermissionStatus> statuses = await [
        Permission.microphone,
        Permission.storage,
      ].request();
    }

    await openTheRecorder();
  }

  Future<void> openTheRecorder() async {
    var status = await Permission.microphone.request();
    if (status != PermissionStatus.granted) {
      throw RecordingPermissionException('Microphone permission not granted');
    }

    var tempDir = await getTemporaryDirectory();
    _filePath = '${tempDir.path}/flutter_sound_example.wav';
    var outputFile = File(_filePath);
    if (outputFile.existsSync()) {
      await outputFile.delete();
    }
    await _frecorder.openAudioSession();
    print('Recorder Initialized');
    setState(() {
      _recorderInit = true;
      _voiceFile = outputFile;
    });
  }

  //FlutterSound recorder
  void _onRecord() async {
    if (!_recording) {
      await _frecorder.startRecorder(
        toFile: _filePath,
        sampleRate: 16000,
        numChannels: 1,
        codec: Platform.isAndroid ? Codec.pcm16WAV : Codec.pcm16WAV,
      );

      setState(() {
        _recording = true;
      });

      // showModalBottomSheet(
      //     backgroundColor:
      //         Colors.green[200], // Color(0xFF212121).withOpacity(0.95),
      //     isDismissible: false,
      //     context: context,
      //     builder: (BuildContext bc) {
      //       return buildFirst();
      //     });

      //Log.d(widget.audio.track.identity);
    }
  }

  void _onStop() async {
    await _frecorder.stopRecorder();
    // _frecorder.closeAudioSession();
    print(_filePath);

    File voiceFile = File(_filePath);
    List<int> theBytes = await voiceFile.readAsBytes();
    print('File Size is ${_voiceFile.lengthSync()}');
    setState(() {
      // _voiceFile = voiceFile;
      _recording = false;
      _saving = true;
      _voiceBytes = theBytes;
    });

    _enrolVoice();
    // await _mPlayer.startPlayer(
    //     fromURI: _filePath,
    //     codec: Codec.pcm16WAV,
    //     whenFinished: () {
    //       setState(() {
    //         print('Done playing');
    //       });
    //     });
  }

  @override
  void dispose() {
    // Be careful : you must `close` the audio session when you have finished with it.
    _frecorder.closeAudioSession();
    _frecorder = null;
    super.dispose();
  }

  Widget buildFirst() {
    return Center(
        child: Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 20, bottom: 20),
          child: Text(
            '${AppStrings.appName}',
            style: TextStyle(color: Colors.white, fontSize: 25),
          ),
        ),
        Icon(
          Icons.mic,
          color: Colors.red,
          size: 40,
        ),
        Center(
          child: Text(
            'Say the following words before the timer completes',
            style: GoogleFonts.abel(
                textStyle: TextStyle(color: Colors.white, fontSize: 14)),
          ),
        ),
        SizedBox(height: 5),
        Center(
          child: Text(
            '$_phrase',
            style: GoogleFonts.abel(
                textStyle: TextStyle(color: Colors.white, fontSize: 20)),
          ),
        ),
        CircularCountDownTimer(
          // Countdown duration in Seconds
          duration: Platform.isAndroid ? 7 : 7,

          // Width of the Countdown Widget
          width: MediaQuery.of(context).size.width / 2.5,

          // Height of the Countdown Widget
          height: MediaQuery.of(context).size.height / 3,

          // Default Color for Countdown Timer
          color: Colors.white,

          // Filling Color for Countdown Timer
          fillColor: Colors.red,

          // Border Thickness of the Countdown Circle
          strokeWidth: 13.0,

          // Text Style for Countdown Text
          textStyle: TextStyle(
              fontSize: 22.0, color: Colors.white, fontWeight: FontWeight.bold),

          // true for reverse countdown (max to 0), false for forward countdown (0 to max)
          isReverse: false,

          // Function which will execute when the Countdown Ends
          onComplete: () {
            // Here, do whatever you want
            print('Countdown Ended');
            this.setState(() {
              _recording = false;
              _onStop();
            });

            Navigator.pop(context);
          },
        ),
      ],
    ));
  }

  _enrolVoice() async {
    try {
      var result = await Network.voiceEnrolment(
          AppStrings.voiceEnrolmentUrl, _token, _voiceBytes, _voiceFile);

      setState(() {
        _saving = false;
      });

      print('Voice enrolment status is ${result.enrollmentStatus}');
      if (result.enrollmentStatus == 'Enrolled') {
        String message =
            "Congratulations. You have successfully created your PayTalk VoicePrint. You can now issue commands using your voice";
        Function nextAction = () => updateVoiceEnrolment();

        // showSuccessDialog(context, message, 'Success', nextAction: nextAction);

        DialogService().show(
          nextAction: nextAction,
          context: context,
          message: "Your voice enrollment was completed successfully.",
          type: AlertDialogType.success,
        );
      }
    } catch (err) {
      setState(() {
        _saving = false;
      });
      showAuthFailed(context, 'Enrollment Failed',
          'An error occurred during voice enrollment. Please try again');
    }
  }

  updateVoiceEnrolment() async {
    await DatabaseService.updateVoiceEnrolment(uid);
    var userInfo = await DatabaseService.getUserInfo(uid);
    Provider.of<UserData>(context, listen: false).appUser = userInfo;
    Navigate.pushNamed('/home', context);
  }

  @override
  Widget build(BuildContext context) {
    return ModalProgressHUD(
      inAsyncCall: _saving,
      child: Scaffold(
          key: scaffoldKey,
          body: ListView(
              padding: EdgeInsets.symmetric(horizontal: 20.0),
              children: [
                SizedBox(height: 120.0),
                Image.asset('assets/images/voicereg.jpg'),
                // SvgPicture.asset(
                //   'assets/images/sound.svg',
                //   height: 130.0,
                // ),
                SizedBox(height: 30.0),
                Center(
                  child: Text(
                    'Enable Voice Recognition',
                    style:
                        TextStyle(fontSize: 25.0, fontWeight: FontWeight.bold),
                  ),
                ),
                SizedBox(height: 15.0),
                Center(
                  child: Text(
                    '${AppStrings.appName} can be configured to recognize only your voice. You can turn this off or on in the settings page.',
                    style: TextStyle(fontSize: 15.0),
                    textAlign: TextAlign.center,
                  ),
                ),
                // SizedBox(height: 30.0),
                // Center(
                //   child: Text(
                //     '$_phrase',
                //     style: TextStyle(
                //         fontSize: 20.0,
                //         fontWeight: FontWeight.bold,
                //         color: Colors.green),
                //     textAlign: TextAlign.center,
                //   ),
                // ),
                SizedBox(height: 50.0),
                MasterButton(
                    text: 'Enrol My Voice',
                    color: Colors.green[300],
                    onPressed: () {
                      _showMic();
                    }),

                SizedBox(height: 30.0),
                OutlineButton(
                    child:
                        Text('No, I won\'t restrict access to just my voice'),
                    onPressed: () {
                      _dontEnrolVoice();
                    }),
                // CustomButton(
                //   label: 'Enroll my voice',
                //   onTap: () {
                //     _onRecord();
                //   },
                // ),
              ])),
    );
  }

  _dontEnrolVoice() async {
    DatabaseService.changeVoiceRecognition(false);
    var userInfo = await DatabaseService.getUserInfo(uid);

    Provider.of<UserData>(context, listen: false).appUser = userInfo;
    Navigate.pushNamed('/home', context);
  }

  _showMic() async {
    //await _listen();
    await _onRecord();
    Alert(
      style: AlertStyle(
        titleStyle: TextStyle(color: Colors.green[300], fontSize: 15),
      ),
      context: context,
      content: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          CircularCountDownTimer(
            // Countdown duration in Seconds
            duration: Platform.isAndroid ? 10 : 10,

            // Width of the Countdown Widget
            width: MediaQuery.of(context).size.width / 2.5,

            // Height of the Countdown Widget
            height: MediaQuery.of(context).size.height / 5,

            // Default Color for Countdown Timer
            color: Colors.white,

            // Filling Color for Countdown Timer
            fillColor: Colors.green[300],

            // Border Thickness of the Countdown Circle
            strokeWidth: 13.0,

            // Text Style for Countdown Text
            textStyle: TextStyle(
                fontSize: 22.0,
                color: Colors.green,
                fontWeight: FontWeight.bold),

            // true for reverse countdown (max to 0), false for forward countdown (0 to max)
            isReverse: false,

            // Function which will execute when the Countdown Ends
            onComplete: () {
              // Here, do whatever you want
              // print('Countdown Ended');
              this.setState(() {
                _isListening = false;
              });

              _onStop();

              // _sendSpeech();

              Navigator.pop(context);
            },
          ),
          AvatarGlow(
            animate: _isListening,
            glowColor: Colors.green[200], // Theme.of(context).primaryColor,
            endRadius: 75.0,
            duration: const Duration(milliseconds: 2000),
            repeatPauseDuration: const Duration(milliseconds: 100),
            repeat: true,
            child: FloatingActionButton(
              backgroundColor: Colors.green[600],
              onPressed: () {},
              child: Icon(Icons.mic_none_outlined),
            ),
          ),
        ],
      ),
      title: "Please recite the following phrase before the timer elapses:",
      desc: "$_phrase",
      buttons: [
        DialogButton(
          color: Colors.red[300],
          child: Text(
            "${AppStrings.done}",
            style: TextStyle(color: Colors.white, fontSize: 16),
          ),
          onPressed: () {
            _onStop();
            this.setState(() {
              _isListening = false;
              // _speech.stop();
            });
            Navigator.pop(context);
          },
          width: 200,
        )
      ],
    ).show();
  }
}
