import 'dart:io';

import 'package:currency_picker/currency_picker.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:paytalk_mobile/common/images.dart';
import 'package:paytalk_mobile/common/navigate.dart';
import 'package:paytalk_mobile/common/strings.dart';
import 'package:paytalk_mobile/common/validations.dart';
import 'package:paytalk_mobile/services/auth_service.dart';
import 'package:paytalk_mobile/services/network.dart';
import 'package:paytalk_mobile/views/widgets/custom_button.dart';
import 'package:paytalk_mobile/views/widgets/master_button.dart';
import 'package:paytalk_mobile/views/widgets/custom_text_field.dart';
import 'package:line_awesome_icons/line_awesome_icons.dart';

import 'bio_enrol.dart';

class CreateProfile extends StatefulWidget {
  @override
  _CreateProfileState createState() => _CreateProfileState();
}

class _CreateProfileState extends State<CreateProfile> {
  File image;
  bool loading = false;
  String userName,
      firstName,
      lastName,
      phone,
      country = 'NG',
      currencyName = 'Nigeria Naira';
  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  AuthService authService = AuthService();
  final picker = ImagePicker();
  String code = '+234';
  String selectedCurrency = 'NGN';
  String currencySymbol = 'N';
  String _token;
  String _locale = "en-CA";

  @override
  void initState() {
    getToken(FirebaseAuth.instance.currentUser);
    super.initState();
  }

  getToken(User user) async {
    var token = await user.getIdToken(true);

    setState(() {
      _token = token;
      print("TOKEN IS $_token");
    });
  }

  Future getImage() async {
    loading = true;
    setState(() {});
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    final croppedFile = await AppImages().cropImage(File(pickedFile.path));

    setState(() {
      if (croppedFile != null) {
        image = File(croppedFile.path);
        loading = false;
      } else {
        loading = false;
        showInSnackBar('No image selected.');
      }
    });
  }

  submit() async {
    if (image == null) {
      showInSnackBar('Please select a profile picture');
      return;
    }
    FormState form = formKey.currentState;
    form.save();
    if (!form.validate()) {
      showInSnackBar('Please fix the errors in red before submitting.');
    } else {
      loading = true;
      setState(() {});
      try {
        var userExists = await authService.checkUserName(userName: userName);
        if (!userExists) {
          var url = await AppImages().uploadImage(image, userName);
          await authService.saveUserInfo(firstName, lastName, userName, country,
              url, selectedCurrency, currencySymbol);
          await Network.createVoiceProfile(
              AppStrings.createProfileUrl, _token, _locale);
          Navigate.pushPage(context, BioEnrolment());
        } else {
          showInSnackBar('Oops! this username already exists. ');
        }
      } catch (e) {
        loading = false;
        setState(() {});
        print(e);
        showInSnackBar('${authService.handleFirebaseAuthError(e.toString())}');
      }
      loading = false;
      setState(() {});
    }
  }

  void showInSnackBar(String value) {
    scaffoldKey.currentState.removeCurrentSnackBar();
    scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(value)));
  }

  @override
  Widget build(BuildContext context) {
    return ModalProgressHUD(
      inAsyncCall: loading,
      child: Scaffold(
        key: scaffoldKey,
        appBar: AppBar(),
        body: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: ListView(
            padding: EdgeInsets.symmetric(horizontal: 20.0),
            children: [
              SizedBox(height: 20.0),
              // SvgPicture.asset(
              //   'assets/images/transfer2.svg',
              //   height: 130.0,
              // ),
              // SizedBox(height: 50.0),
              Center(
                child: Text(
                  'Create your profile',
                  style: TextStyle(fontSize: 25.0, fontWeight: FontWeight.bold),
                ),
              ),
              SizedBox(height: 15.0),
              Center(
                child: Text(
                  'Select a profile image and enter your details',
                  style: TextStyle(fontSize: 15.0),
                  textAlign: TextAlign.center,
                ),
              ),
              SizedBox(height: 15.0),
              GestureDetector(
                onTap: () => getImage(),
                child: CircleAvatar(
                  radius: 80.0,
                  child: Icon(
                    Icons.add,
                    size: 30,
                    color: Colors.white,
                  ),
                  backgroundImage: image == null
                      ? AssetImage('assets/images/female.png')
                      : FileImage(image),
                ),
              ),
              SizedBox(height: 40.0),
              Form(
                key: formKey,
                child: Column(
                  children: [
                    CustomTextField(
                      hintText: 'Enter a Username',
                      prefix: LineAwesomeIcons.user,
                      enabled: true,
                      textInputAction: TextInputAction.next,
                      validateFunction: Validations.validateName,
                      onSaved: (String val) {
                        // viewModel.setName(val);
                        userName = val;
                        setState(() {});
                      },
                      // focusNode: viewModel.nameFN,
                      // nextFocusNode: viewModel.emailFN,
                    ),
                    SizedBox(height: 20.0),
                    CustomTextField(
                      hintText: 'First Name',
                      prefix: LineAwesomeIcons.user_secret,
                      enabled: true,
                      textInputAction: TextInputAction.next,
                      validateFunction: Validations.validateName,
                      onSaved: (String val) {
                        // viewModel.setName(val);
                        firstName = val;
                        setState(() {});
                      },
                      // focusNode: viewModel.nameFN,
                      // nextFocusNode: viewModel.emailFN,
                    ),
                    SizedBox(height: 20.0),
                    CustomTextField(
                      hintText: 'Last Name',
                      prefix: LineAwesomeIcons.certificate,
                      enabled: true,
                      textInputAction: TextInputAction.next,
                      validateFunction: Validations.validateName,
                      onSaved: (String val) {
                        // viewModel.setName(val);
                        lastName = val;
                        setState(() {});
                      },
                      // focusNode: viewModel.nameFN,
                      // nextFocusNode: viewModel.emailFN,
                    ),
                    SizedBox(height: 20.0),

                    FlatButton(
                        textColor: Colors.red,
                        onPressed: () {
                          showCurrencyPicker(
                            context: context,
                            showFlag: true,
                            currencyFilter: <String>[
                              'EUR',
                              'GBP',
                              'USD',
                              'AUD',
                              'CAD',
                              'JPY',
                              'NGN',
                              'GHS',
                              'RWF',
                              'UGX',
                              'ZMW',
                              'KES'
                            ],
                            onSelect: (Currency currency) {
                              print(
                                  'Select currency: ${currency.name}, ${getCountryCode(currency.code)}');
                              setState(() {
                                selectedCurrency = currency.code;
                                currencySymbol = currency.symbol;
                                country = getCountryCode(currency.code);
                                currencyName = currency.name;
                              });
                            },
                          );
                        },
                        child: Text('Select your currency')),
                    SizedBox(
                      height: 10,
                    ),

                    Text(
                      '$currencyName, $selectedCurrency',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    )

                    // Container(
                    //   width: MediaQuery.of(context).size.width,
                    //   child: CountryCodePicker(
                    //     onChanged: (val) {
                    //       country = val.name;
                    //     },
                    //     initialSelection: 'NG',
                    //     favorite: ['+1', 'US', '+1', 'CA', '+234', 'NG'],
                    //     showCountryOnly: true,
                    //     showOnlyCountryWhenClosed: true,
                    //     alignLeft: false,
                    //   ),
                    // ),
                  ],
                ),
              ),
              SizedBox(height: 30.0),
              MasterButton(
                  text: 'Create my profile',
                  color: Colors.green[300],
                  onPressed: () {
                    submit();
                  }),
            ],
          ),
        ),
      ),
    );
  }

  String getCountryCode(String myCurrency) {
    return myCurrency.substring(0, myCurrency.length - 1);
  }
}
