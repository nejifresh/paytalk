import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:paytalk_mobile/common/navigate.dart';
import 'package:paytalk_mobile/common/validations.dart';
import 'package:paytalk_mobile/models/user.dart';
import 'package:paytalk_mobile/services/auth_service.dart';
import 'package:paytalk_mobile/services/database.dart';
import 'package:paytalk_mobile/views/pages/auth/enrolment.dart';
import 'package:paytalk_mobile/views/pages/auth/phone_signup.dart';
import 'package:paytalk_mobile/views/pages/auth/sign_up.dart';
import 'package:paytalk_mobile/views/widgets/custom_button.dart';
import 'package:paytalk_mobile/views/widgets/custom_text_field.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_signin_button/flutter_signin_button.dart';
import 'package:flutter_signin_button/button_builder.dart';
import 'package:flutter_signin_button/flutter_signin_button.dart';
import 'package:paytalk_mobile/views/pages/home.dart';
import 'package:paytalk_mobile/models/user_data.dart';
import 'package:provider/provider.dart';
import 'bio_enrol.dart';

import 'create_profile.dart';

class SignInPage extends StatefulWidget {
  @override
  _SignInPageState createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  bool loading = false;
  AuthService authService = AuthService();
  String email, password;
  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  googleLogin() async {
    loading = true;
    setState(() {});
    try {
      var result = await authService.googleLogin();
      loading = false;
      setState(() {});
      changeScreen(result.uid);
    } catch (e) {
      print(e);
      loading = false;
      setState(() {});
    }
  }

  submit() async {
    FormState form = formKey.currentState;
    form.save();
    if (!form.validate()) {
      showInSnackBar('Please fix the errors in red before submitting.');
    } else {
      loading = true;
      setState(() {});
      try {
        String userId = await authService.signIn(
          email: email,
          password: password,
        );
        print(userId);
        if (userId != null) {
          changeScreen(userId);
        }
      } catch (e) {
        loading = false;
        setState(() {});
        print(e);
        showInSnackBar('${authService.handleFirebaseAuthError(e.toString())}');
      }
      loading = false;
      setState(() {});
    }
  }

  void showInSnackBar(String value) {
    scaffoldKey.currentState.removeCurrentSnackBar();
    scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(value)));
  }

  changeScreen(String userId) async {
    //now check if this user has been enrolled
    var userInfo = await DatabaseService.getUserInfo(userId);
    if (userInfo != null) {
      userInfo.isEnrolled && userInfo.voiceEnrolled
          ? goHome(userInfo)
          : userInfo.isEnrolled && !userInfo.voiceEnrolled
              ? Navigate.pushPage(context, BioEnrolment())
              : Navigate.pushPage(context, CreateProfile());
    } else {
      showInSnackBar('An error occured ');
    }
  }

  goHome(User user) {
    Provider.of<UserData>(context, listen: false).appUser = user;
    Navigate.pushPage(context, Home());
  }

  @override
  Widget build(BuildContext context) {
    return ModalProgressHUD(
      inAsyncCall: loading,
      child: Scaffold(
        key: scaffoldKey,
        body: ListView(
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          children: [
            SizedBox(height: 100.0),
            SvgPicture.asset(
              'assets/images/transfer2.svg',
              height: 130.0,
            ),
            SizedBox(height: 50.0),
            Center(
              child: Text(
                'Sign In',
                style: TextStyle(fontSize: 25.0, fontWeight: FontWeight.bold),
              ),
            ),
            SizedBox(height: 5.0),
            Center(
              child: Text(
                'Sign in to start sending payments the smart way',
                style: TextStyle(fontSize: 15.0),
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox(height: 30.0),
            Form(
              key: formKey,
              child: Column(
                children: [
                  CustomTextField(
                    hintText: 'Enter your email',
                    prefix: Icons.email,
                    enabled: true,
                    textInputAction: TextInputAction.next,
                    validateFunction: Validations.validateEmail,
                    onSaved: (String val) {
                      // viewModel.setName(val);
                      email = val;
                    },
                    // focusNode: viewModel.nameFN,
                    // nextFocusNode: viewModel.emailFN,
                  ),
                  SizedBox(height: 20.0),
                  CustomTextField(
                    hintText: 'Password',
                    prefix: Icons.lock,
                    enabled: true,
                    textInputAction: TextInputAction.next,
                    validateFunction: Validations.validatePassword,
                    onSaved: (String val) {
                      // viewModel.setName(val);
                      password = val;
                    },
                    obscureText: true,
                    // focusNode: viewModel.nameFN,
                    // nextFocusNode: viewModel.emailFN,
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "Don't have an account? ",
                ),
                InkWell(
                  onTap: () {
                    // Navigate(context).pushNavigate(Login());
                  },
                  child: Text("Register",
                      style: TextStyle(fontFamily: "Poppins-Bold")),
                )
              ],
            ),
            SizedBox(height: 20.0),
            // Align(
            //   alignment: AlignmentDirectional.centerEnd,
            //   child: FlatButton(
            //     onPressed: () {},
            //     child: Text('Forgot password?'),
            //   ),
            // ),
            // SizedBox(height: 20.0),
            CustomButton(
              label: 'Sign in',
              onTap: () => submit(),
            ),
            SizedBox(height: 20.0),
            Divider(),
            SizedBox(height: 10.0),
            Center(
              child: Text('Or'),
            ),
            // SizedBox(height: 10.0),
            // CustomButton(
            //   label: 'Email',
            //   onTap: () {
            //     Navigate.pushPage(context, SignUp());
            //   },
            // ),
            SizedBox(height: 10.0),
            SignInButtonBuilder(
              height: 40,
              text: 'Sign in with Phone',
              icon: Icons.phone,
              onPressed: () {
                Navigate.pushPage(context, PhoneSignUp());
              },
              backgroundColor: Theme.of(context).backgroundColor,
            ),

            SizedBox(height: 20.0),
            SignInButton(
              Buttons.Google,
              text: "Sign in with Google",
              onPressed: () => googleLogin(),
            ),
            // CustomButton(
            //   label: 'Google',
            //   onTap: () => googleLogin(),
            // ),
            SizedBox(height: 30.0),
          ],
        ),
      ),
    );
  }
}
