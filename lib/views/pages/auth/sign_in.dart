import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:paytalk_mobile/common/navigate.dart';
import 'package:paytalk_mobile/common/validations.dart';
import 'package:paytalk_mobile/services/auth_service.dart';
import 'package:paytalk_mobile/views/pages/auth/phone_signup.dart';
import 'package:paytalk_mobile/views/pages/auth/sign_in_page.dart';
import 'package:paytalk_mobile/views/pages/auth/sign_up.dart';
import 'package:paytalk_mobile/views/widgets/custom_button.dart';
import 'package:paytalk_mobile/views/widgets/custom_text_field.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_signin_button/button_builder.dart';
import 'package:flutter_signin_button/flutter_signin_button.dart';

import 'create_profile.dart';

class SignIn extends StatefulWidget {
  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  bool loading = false;
  AuthService authService = AuthService();
  String email, password;
  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  googleLogin() async {
    loading = true;
    setState(() {});
    try {
      await authService.googleLogin();
      loading = false;
      setState(() {});
      changeScreen();
    } catch (e) {
      print(e);
      loading = false;
      setState(() {});
    }
  }

  submit() async {
    FormState form = formKey.currentState;
    form.save();
    if (!form.validate()) {
      showInSnackBar('Please fix the errors in red before submitting.');
    } else {
      loading = true;
      setState(() {});
      try {
        bool success = await authService.createUser(
          email: email,
          password: password,
        );
        print(success);
        if (success) {
          changeScreen();
        }
      } catch (e) {
        loading = false;
        setState(() {});
        print(e);
        showInSnackBar('${authService.handleFirebaseAuthError(e.toString())}');
      }
      loading = false;
      setState(() {});
    }
  }

  void showInSnackBar(String value) {
    scaffoldKey.currentState.removeCurrentSnackBar();
    scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(value)));
  }

  changeScreen() {
    Navigate.pushPage(context, CreateProfile());
  }

  @override
  Widget build(BuildContext context) {
    return ModalProgressHUD(
      inAsyncCall: loading,
      child: Scaffold(
        key: scaffoldKey,
        body: ListView(
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          children: [
            SizedBox(height: 100.0),
            SvgPicture.asset(
              'assets/images/transfer2.svg',
              height: 130.0,
            ),
            SizedBox(height: 50.0),
            Center(
              child: Text(
                'Get Started',
                style: TextStyle(fontSize: 25.0, fontWeight: FontWeight.bold),
              ),
            ),
            SizedBox(height: 5.0),
            Center(
              child: Text(
                'Sign up or log in to start sending payments the smart way',
                style: TextStyle(fontSize: 15.0),
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox(height: 30.0),
            Form(
              key: formKey,
              child: Column(
                children: [
                  CustomTextField(
                    hintText: 'Enter your email',
                    prefix: Icons.email,
                    enabled: true,
                    textInputAction: TextInputAction.next,
                    validateFunction: Validations.validateEmail,
                    onSaved: (String val) {
                      // viewModel.setName(val);
                      email = val;
                    },
                    // focusNode: viewModel.nameFN,
                    // nextFocusNode: viewModel.emailFN,
                  ),
                  SizedBox(height: 20.0),
                  CustomTextField(
                    hintText: 'Password',
                    prefix: Icons.lock,
                    enabled: true,
                    textInputAction: TextInputAction.next,
                    validateFunction: Validations.validatePassword,
                    onSaved: (String val) {
                      // viewModel.setName(val);
                      password = val;
                    },
                    obscureText: true,
                    // focusNode: viewModel.nameFN,
                    // nextFocusNode: viewModel.emailFN,
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "Already have an account? ",
                ),
                InkWell(
                  onTap: () {
                    Navigate.pushPage(context, SignInPage());
                  },
                  child: Text("Sign In",
                      style: TextStyle(fontFamily: "Poppins-Bold")),
                )
              ],
            ),
            SizedBox(height: 20.0),
            // Align(
            //   alignment: AlignmentDirectional.centerEnd,
            //   child: FlatButton(
            //     onPressed: () {},
            //     child: Text('Forgot password?'),
            //   ),
            // ),
            // SizedBox(height: 20.0),
            CustomButton(
              label: 'Sign up',
              onTap: () => submit(),
            ),
            SizedBox(height: 20.0),
            Divider(),
            SizedBox(height: 10.0),
            Center(
              child: Text('Or'),
            ),
            // SizedBox(height: 10.0),
            // CustomButton(
            //   label: 'Email',
            //   onTap: () {
            //     Navigate.pushPage(context, SignUp());
            //   },
            // ),
            SizedBox(height: 10.0),
            SignInButtonBuilder(
              height: 40,
              text: 'Sign up with Phone',
              icon: Icons.phone,
              onPressed: () {
                Navigate.pushPage(context, PhoneSignUp());
              },
              backgroundColor: Theme.of(context).backgroundColor,
            ),

            SizedBox(height: 20.0),
            SignInButton(
              Buttons.Google,
              text: "Sign up with Google",
              onPressed: () => googleLogin(),
            ),
            // CustomButton(
            //   label: 'Google',
            //   onTap: () => googleLogin(),
            // ),
            SizedBox(height: 30.0),
          ],
        ),
      ),
    );
  }
}
