// import 'package:flutter/material.dart';

// class CreateBankAccount extends StatefulWidget {
//   @override
//   _CreateBankAccountState createState() => _CreateBankAccountState();
// }

// class _CreateBankAccountState extends State<CreateBankAccount> {

//    @override
//   Widget build(BuildContext context) {
//     final title = HeaderText(text: "Settings");
//     final user = Provider.of<UserData>(context).appUser;
//     _voiceEnrolled = user.voiceEnrolled;
//     //_voiceRecognition = user.enableVoiceRecognition;

//     name.text = "${user.firstName} ${user.lastName}";
//     email.text = "${user.email}";
//     // phone.text = "${user.}";
//     currencyName.text = "${user.currency}";
//     address.text = "${user.country}";

//     final voice = new SettingCard(
//       title: 'Enable Voice Recognition',
//       value: _voiceRecognition,
//       allowBorder: false,
//       changeFunction: _toggleVoiceRecognition,
//     );

//     final userImage = Container(
//       alignment: Alignment.center,
//       child: Stack(
//         children: <Widget>[
//           Container(
//             margin: EdgeInsets.only(bottom: 10.0, top: 20.0),
//             height: 100.0,
//             width: 100.0,
//             decoration: BoxDecoration(
//               image: DecorationImage(
//                 image: NetworkImage(user.imageUrl),
//                 fit: BoxFit.cover,
//               ),
//               shape: BoxShape.circle,
//             ),
//           ),
//           Positioned(
//             bottom: 10,
//             right: 0,
//             child: InkWell(
//               onTap: () {
//                 _updateImage(user.uuid);
//               },
//               child: Container(
//                 height: 30.0,
//                 width: 30.0,
//                 decoration:
//                     BoxDecoration(color: Colors.white, shape: BoxShape.circle),
//                 child: Icon(
//                   LineAwesomeIcons.camera,
//                 ),
//               ),
//             ),
//           )
//         ],
//       ),
//     );

//     final spacer = SizedBox(height: 20.0);

//     final nameField = CustomFormField(
//       labelText: "Fullname",
//       icon: LineAwesomeIcons.user,
//       controller: name,
//     );

//     final emailField = CustomFormField(
//       labelText: "Email",
//       icon: LineAwesomeIcons.envelope,
//       controller: email,
//     );

//     final phoneField = CustomFormField(
//       labelText: "Phone",
//       icon: LineAwesomeIcons.mobile,
//       controller: phone,
//     );

//     final currencyField = CustomFormField(
//       labelText: "Default Currency",
//       icon: LineAwesomeIcons.money,
//       controller: currencyName,
//       enabled: false,
//       suffixText: 'change',
//     );

//     final addressField = CustomFormField(
//       labelText: "Country Code",
//       icon: LineAwesomeIcons.map_marker,
//       controller: address,
//     );

//     final button = CustomButton(
//       label: "Update",
//       onTap: () {
//         DialogService().show(
//           context: context,
//           message: "You account has been updated successfully.",
//           type: AlertDialogType.success,
//         );
//       },
//     );

//     final form = Column(
//       children: <Widget>[
//         spacer,
//         spacer,
//         nameField,
//         spacer,
//         emailField,
//         spacer,
//         phoneField,
//         spacer,
//         InkWell(
//             onTap: () {
//               _chooseCurrency(user.uuid);
//             },
//             child: currencyField),
//         spacer,
//         addressField,
//         spacer,
//         voice
//         //button
//       ],
//     );

//     return Scaffold(
//       key: scaffoldKey,
//       appBar: CustomAppBar(),
//       body: SingleChildScrollView(
//         // padding: EdgeInsets.symmetric(horizontal: 20.0),
//         padding: EdgeInsets.only(
//           left: 20.0,
//           right: 20.0,
//           top: 20.0,
//           bottom: 30.0,
//         ),
//         child: Column(
//           crossAxisAlignment: CrossAxisAlignment.start,
//           children: <Widget>[title, userImage, form],
//         ),
//       ),
//     );
//   }
// }
