import 'package:flutter/material.dart';
import 'package:paytalk_mobile/common/validations.dart';
import 'package:paytalk_mobile/views/widgets/custom_button.dart';
import 'package:paytalk_mobile/views/widgets/custom_text_field.dart';

class SignUp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 20.0),
        children: [
          SizedBox(height: 100.0),
          Center(
            child: Text(
              'Sign Up',
              style: TextStyle(fontSize: 25.0, fontWeight: FontWeight.bold),
            ),
          ),
          SizedBox(height: 5.0),
          Center(
            child: Text(
              'Register a new accountt',
              style: TextStyle(fontSize: 15.0),
            ),
          ),
          SizedBox(height: 80.0),
          CustomTextField(
            hintText: 'Enter your first name',
            prefix: Icons.email,
            enabled: true,
            textInputAction: TextInputAction.next,
            validateFunction: Validations.validateName,
            onSaved: (String val) {
              // viewModel.setName(val);
            },
            // focusNode: viewModel.nameFN,
            // nextFocusNode: viewModel.emailFN,
          ),
          SizedBox(height: 20.0),
          CustomTextField(
            hintText: 'Password',
            prefix: Icons.lock,
            enabled: true,
            textInputAction: TextInputAction.next,
            validateFunction: Validations.validateName,
            onSaved: (String val) {
              // viewModel.setName(val);
            },
            obscureText: true,
            // focusNode: viewModel.nameFN,
            // nextFocusNode: viewModel.emailFN,
          ),
          SizedBox(height: 20.0),
          CustomButton(
            label: 'Submit',
            onTap: () {
              // Navigate.pushPage(context, SignUp());
            },
          )
        ],
      ),
    );
  }
}
