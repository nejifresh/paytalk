import 'dart:io';

import 'package:country_code_picker/country_code_picker.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:paytalk_mobile/common/firebase.dart';
import 'package:paytalk_mobile/common/navigate.dart';
import 'package:paytalk_mobile/common/validations.dart';
import 'package:paytalk_mobile/services/auth_service.dart';
import 'package:paytalk_mobile/views/pages/auth/enrolment.dart';
import 'package:paytalk_mobile/views/widgets/custom_button.dart';
import 'package:paytalk_mobile/views/widgets/custom_text_field.dart';
import 'package:flutter_svg/svg.dart';
import 'create_profile.dart';

class PhoneSignUp extends StatefulWidget {
  @override
  _PhoneSignUpState createState() => _PhoneSignUpState();
}

class _PhoneSignUpState extends State<PhoneSignUp> {
  String phone, password, code = '+234', sms;
  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  AuthService authService = AuthService();
  bool loading = false;
  bool sent = false;
  String verification;

  submit() async {
    FormState form = formKey.currentState;
    form.save();
    if (!form.validate()) {
      showInSnackBar('Please fix the errors in red before submitting.');
    } else {
      loading = true;
      setState(() {});
      try {
        if (sent) {
          String smsCode = sms;

          PhoneAuthCredential phoneAuthCredential =
              PhoneAuthProvider.credential(
            verificationId: verification,
            smsCode: smsCode,
          );

          var result =
              await firebaseAuth.signInWithCredential(phoneAuthCredential);
          if (result.user.uid != null) {
            showInSnackBar('Verification Successful');
            Navigate.pushPage(context, CreateProfile());
          }
        } else {
          print(' code is $code$phone');
          await firebaseAuth.verifyPhoneNumber(
            phoneNumber: '$code$phone',
            verificationCompleted: (PhoneAuthCredential credential) async {
              if (Platform.isAndroid) {
                var result =
                    await firebaseAuth.signInWithCredential(credential);
                if (result.user.uid != null) {
                  showInSnackBar('Verification Successful');
                  Navigate.pushPage(context, CreateProfile());
                }
              }
            },
            verificationFailed: (FirebaseAuthException e) {
              if (e.code == 'invalid-phone-number') {
                showInSnackBar('The provided phone number is not valid.');
              } else {
                authService.handleFirebaseAuthError(e.toString());
              }
            },
            codeSent: (String verificationId, int resendToken) async {
              sent = true;
              loading = false;
              verification = verificationId;
              setState(() {});
            },
            codeAutoRetrievalTimeout: (String verificationId) {},
          );
        }
      } catch (e) {
        loading = false;
        setState(() {});
        print(e);
        showInSnackBar('${authService.handleFirebaseAuthError(e.toString())}');
      }
      loading = false;
      setState(() {});
    }
  }

  void showInSnackBar(String value) {
    scaffoldKey.currentState.removeCurrentSnackBar();
    scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(value)));
  }

  @override
  Widget build(BuildContext context) {
    return ModalProgressHUD(
      inAsyncCall: loading,
      child: Scaffold(
        key: scaffoldKey,
        body: ListView(
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          children: [
            SizedBox(height: 100.0),
            SvgPicture.asset(
              'assets/images/transfer5.svg',
              height: 130.0,
            ),
            SizedBox(height: 50.0),
            Form(
              key: formKey,
              child: Column(
                children: [
                  SizedBox(height: 30.0),
                  Center(
                    child: Text(
                      'Sign up with your Phone Number',
                      style: TextStyle(
                          fontSize: 20.0, fontWeight: FontWeight.bold),
                    ),
                  ),
                  SizedBox(height: 5.0),
                  Center(
                    child: Text(
                      'Select your country and enter your number below',
                      style: TextStyle(fontSize: 15.0),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  SizedBox(height: 50.0),
                  Visibility(
                    visible: !sent,
                    replacement: buildCodeField(),
                    maintainState: false,
                    child: CustomTextField(
                      widgetPrefix: CountryCodePicker(
                        onChanged: (val) {
                          setState(() {
                            code = val.dialCode;
                          });
                        },
                        initialSelection: 'NG',
                        textStyle: TextStyle(fontSize: 15),
                        showCountryOnly: false,
                        favorite: ['+1', 'US', '+1', 'CA', '+234', 'NG'],
                        showOnlyCountryWhenClosed: false,
                        alignLeft: false,
                      ),
                      hintText: ' Phone Number',
                      textInputType: TextInputType.phone,
                      enabled: true,
                      textStyle: TextStyle(fontSize: 15),
                      textInputAction: TextInputAction.done,
                      validateFunction: Validations.validatePhone,
                      onSaved: (String val) {
                        // viewModel.setName(val);
                        phone = val;
                        setState(() {});
                      },
                      // focusNode: viewModel.nameFN,
                      // nextFocusNode: viewModel.emailFN,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 30.0),
            CustomButton(
              label: sent ? 'Create My Account' : 'Submit',
              onTap: () => submit(),
            ),
          ],
        ),
      ),
    );
  }

  buildCodeField() {
    return CustomTextField(
      hintText: 'Enter SMS Code',
      prefix: Icons.sms,
      enabled: true,
      textInputAction: TextInputAction.done,
      validateFunction: Validations.validateSMSCode,
      onSaved: (String val) {
        // viewModel.setName(val);
        sms = val;
        setState(() {});
      },
      // focusNode: viewModel.nameFN,
      // nextFocusNode: viewModel.emailFN,
    );
  }
}
