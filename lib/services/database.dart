import 'package:paytalk_mobile/common/firebase.dart';
import 'package:paytalk_mobile/models/transaction.dart';
import 'package:paytalk_mobile/models/user.dart';
import 'package:paytalk_mobile/models/appconfig.dart';
import 'package:recase/recase.dart';
import 'package:paytalk_mobile/models/recipient.dart';
import 'package:paytalk_mobile/models/paymentplan.dart';
import 'package:currency_picker/currency_picker.dart';
import 'package:firebase_auth/firebase_auth.dart' as auth;
import 'package:paytalk_mobile/models/subscriptions.dart';
import 'package:paytalk_mobile/models/candidate.dart';

class DatabaseService {
  static Future<User> getUserInfo(String userId) async {
    try {
      //var userD = await usersRef.document(userId).get();

      var userD = await usersRef.where('uuid', isEqualTo: userId).get();
      if (userD.docs.length == 1) {
        var user = User.fromMap(userD.docs[0].data());
        print('user is ${user.email}');
        return user;
      }
    } catch (e) {
      print(e.toString());
      return null;
    }

    return null;
  }

  static updateVoiceEnrolment(String userId) async {
    await usersRef.doc(userId).update({
      'voiceEnrolled': true,
      'dateVoiceEnrolled': DateTime.now(),
      'enableVoiceRecognition': true
    });
  }

  static Future<AppConfig> getAppConfig() async {
    var documents = await configRef.get();
    var doc = documents.docs[0].data();
    return AppConfig.fromJson(doc);
  }

  static createTransaction(Transaction transaction) async {
    await transactionRef.add({
      'sender': transaction.senderId,
      'recipient': transaction.recipientId,
      'recipientName': transaction.recipientName,
      'type': transaction.transactionType,
      'amount': transaction.amount,
      'dateOfTransaction': transaction.dateOfTransaction,
      'tref': transaction.transactionRef,
      'command': transaction.command,
      'currency': transaction.currency,
      'orderRef': transaction.orderRef,
      'transactionId': transaction.transactionId,
      'organizationId': transaction.organizationId,
      'imageUrl': transaction.imageUrl,
      'gateway': transaction.paymentGateWay
    });
  }

  static Future<Candidate> getCandidate(String name) async {
    var candidateInfo = await voteRef
        .doc("UsQMUC7XOI049yMMqwXE")
        .collection('bbn')
        .where('name', isEqualTo: name)
        .get();
    if (candidateInfo.docs.isNotEmpty) {
      return Candidate.fromMap(candidateInfo.docs[0].data());
    }
    return null;
  }

  static Future<List<Transaction>> getTransactions(String userId) async {
    List<Transaction> transactionList = List();
    var trans = await transactionRef
        .where('sender', isEqualTo: userId)
        .orderBy('dateOfTransaction', descending: true)
        .get();

    if (trans.size > 0) {
      trans.docs.forEach((doc) {
        var transaction = Transaction.fromJson(doc.data());
        transactionList.add(transaction);
      });
    }

    return transactionList;
  }

  static Future<List<String>> getOrganizations() async {
    List<String> results = List();
    var orgs = await orgRef.get();
    orgs.docs.forEach((org) {
      results.add(org.data()['name'].toString().titleCase);
    });

    print('total orgs - ${results.length}');

    return results;
  }

  static Future<Organizations> getOrgByName(String name) async {
    var orgs = await orgRef.where('name', isEqualTo: name.toLowerCase()).get();
    return Organizations.fromJson(orgs.docs[0].data());
  }

  static Future<Organizations> getOrgById(String orgId) async {
    var orgs = await orgRef.doc(orgId).get();
    return Organizations.fromJson(orgs.data());
  }

  static Future<void> updateUserImage(String imageUrl, String userId) async {
    await usersRef.doc(userId).update({
      'imageUrl': imageUrl ?? '',
    });
  }

  static Future<void> changeCurrency(Currency currency, String userId) async {
    await usersRef.doc(userId).update({
      'currency': currency.code,
      'symbol': currency.symbol,
    });
  }

  static Future<void> changeVoiceRecognition(bool value) async {
    var uid = auth.FirebaseAuth.instance.currentUser.uid;
    await usersRef.doc(uid).update({
      'enableVoiceRecognition': value,
    });
  }

  static Future<PaymentPlan> getPaymentPlan(
      String frequency, String organizationId) async {
    var planInfo = await orgRef
        .doc(organizationId)
        .collection('paymentplans')
        .where('interval', isEqualTo: frequency.toLowerCase())
        .get();
    if (planInfo.docs.isNotEmpty) {
      return PaymentPlan.fromJson(planInfo.docs[0].data(), organizationId);
    }
    return null;
  }

  static Future createUserSubscription(
      String transactionId,
      String planId,
      String planName,
      String userId,
      num amount,
      String interval,
      String organizationId) async {
    await usersRef.doc(userId).collection('subscriptions').add({
      'transactionId': transactionId,
      'planId': planId,
      'planName': planName,
      'dateSubscribed': DateTime.now(),
      'amount': amount,
      'paymentInterval': interval,
      'organizationId': organizationId,
      'active': true
    });
    print('Created user subscription');
  }

  static Future<List<MySubscriptions>> getMySubscriptions(String userId) async {
    List<MySubscriptions> subsList = List();
    var subs = await usersRef
        .doc(userId)
        .collection('subscriptions')
        .where('active', isEqualTo: true)
        .orderBy('dateSubscribed', descending: true)
        .get();
    for (var i = 0; i < subs.docs.length; i++) {
      var org = await getOrgById(subs.docs[i].data()['organizationId']);
      subsList.add(MySubscriptions.fromMap(
          subs.docs[i].data(), subs.docs[i].id, org.imageUrl));
    }
    // subs.docs.forEach((sub) async {
    //   var org = await getOrgById(sub.data()['organizationId']);
    //   subsList.add(MySubscriptions.fromMap(sub.data(), sub.id, org.imageUrl));
    // });

    return subsList;
  }

  static Future cancelSubscription(String userId, String subscriptionId) async {
    await usersRef
        .doc(userId)
        .collection('subscriptions')
        .doc(subscriptionId)
        .update({'active': false, 'cancelledOn': DateTime.now()});
  }
}
