import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:paytalk_mobile/common/firebase.dart';
import 'package:paytalk_mobile/common/navigate.dart';

class AuthService {
  login() {}

  googleLogin() async {
    final GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
    if (googleSignInAccount != null) {
      final GoogleSignInAuthentication googleSignInAuthentication =
          await googleSignInAccount.authentication;

      final AuthCredential credential = GoogleAuthProvider.credential(
        accessToken: googleSignInAuthentication.accessToken,
        idToken: googleSignInAuthentication.idToken,
      );

      final UserCredential authResult =
          await firebaseAuth.signInWithCredential(credential);
      final User user = authResult.user;

      assert(!user.isAnonymous);
      assert(await user.getIdToken() != null);

      final User currentUser = firebaseAuth.currentUser;
      assert(user.uid == currentUser.uid);

      print('signInWithGoogle succeeded: ${user.displayName}');
      return user;
    }
  }

  saveUserInfo(
      firstName, lastName, userName, country, url, currency, symbol) async {
    User user = firebaseAuth.currentUser;
    await usersRef.doc(user.uid).set({
      'firstName': firstName,
      'lastName': lastName,
      'email': user.email ?? '',
      'username': userName.toString().toLowerCase(),
      'country': country ?? 'Nigeria',
      'currency': currency ?? 'NGN',
      'symbol': symbol,
      'imageUrl': url ?? '',
      'dateJoined': DateTime.now(),
      'enrolled': true,
      'uuid': user.uid
    });
  }

  Future<bool> checkUserName({String userName}) async {
    var result = await usersRef
        .where('username', isEqualTo: userName.toLowerCase())
        .get();
    if (result.size > 0) return true;
    return false;
  }

  Future<bool> createUser({String email, String password}) async {
    var res = await firebaseAuth.createUserWithEmailAndPassword(
      email: '$email',
      password: '$password',
    );
    if (res.user != null) {
      return true;
    } else {
      return false;
    }
  }

  Future<String> signIn({String email, String password}) async {
    var res = await firebaseAuth.signInWithEmailAndPassword(
      email: '$email',
      password: '$password',
    );
    if (res.user != null) {
      return res.user.uid;
    } else {
      return null;
    }
  }

  signOut(context) async {
    await firebaseAuth.signOut();
    Navigate.pushNamed('/login', context);
  }

  String handleFirebaseAuthError(String e) {
    if (e.contains("ERROR_WEAK_PASSWORD")) {
      return "Password is too weak";
    } else if (e.contains("invalid-email")) {
      return "Invalid Email";
    } else if (e.contains("ERROR_EMAIL_ALREADY_IN_USE") ||
        e.contains('email-already-in-use')) {
      return "The email address is already in use by another account.";
    } else if (e.contains("ERROR_NETWORK_REQUEST_FAILED")) {
      return "Network error occured!";
    } else if (e.contains("ERROR_USER_NOT_FOUND") ||
        e.contains('firebase_auth/user-not-found')) {
      return "Invalid credentials.";
    } else if (e.contains("ERROR_WRONG_PASSWORD") ||
        e.contains('wrong-password')) {
      return "Invalid credentials.";
    } else if (e.contains('firebase_auth/requires-recent-login')) {
      return 'This operation is sensitive and requires recent authentication.'
          ' Log in again before retrying this request.';
    } else {
      return e;
    }
  }
}
