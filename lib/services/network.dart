import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:http/http.dart' as http;
import 'package:paytalk_mobile/models/donationresponse.dart';
import 'package:paytalk_mobile/models/voice_enrolment.dart';
import 'package:paytalk_mobile/models/voicepayresponse.dart';
import 'package:paytalk_mobile/models/voiceprofile.dart';
import 'package:paytalk_mobile/models/transfer_recipient.dart';
import 'package:paytalk_mobile/common/strings.dart';
import 'package:paytalk_mobile/models/fulfillment.dart';
import 'package:paytalk_mobile/models/recipient.dart';

class Network {
  static Future<VoiceProfile> createVoiceProfile(
      String url, String token, String locale) async {
    token = await FirebaseAuth.instance.currentUser.getIdToken(false);
    final response = await http.post(url,
        headers: {
          HttpHeaders.authorizationHeader: 'Bearer $token',
          HttpHeaders.contentTypeHeader: 'application/json'
        },
        body: jsonEncode({"locale": locale}));
    if (response.statusCode == 201 || response.statusCode == 200) {
      print('Profile created successfully');
      print(response.body);
      return VoiceProfile.fromJson(jsonDecode(response.body));
    } else {
      print('Error occurred');
      print(response.statusCode);
      print(response.body);
      throw Exception(response.body);
    }
  }

  static Future<VoiceEnrolment> voiceEnrolment(
      String url, String token, List<int> voice64, File file) async {
    print('Received bytes are ${voice64.length}');

    token = await FirebaseAuth.instance.currentUser.getIdToken(false);

    final response = await http.post(url,
        headers: {
          HttpHeaders.authorizationHeader: 'Bearer $token',
          HttpHeaders.contentTypeHeader: 'audio/wave'
        },
        body: (voice64));

    if (response.statusCode == 201 || response.statusCode == 200) {
      print('Voice enrolled successfully');
      print(response.body);
      return VoiceEnrolment.fromJson(jsonDecode(response.body.toString()));
    } else {
      print('Error occurred');
      print(response.statusCode);
      print(response.body.toString());
      throw Exception(response.body.toString());
    }
  }

  static Future<dynamic> voicePayment(
      String url, String token, List<int> voice64, File file) async {
    print('Received bytes are ${voice64.length}');
    token = await FirebaseAuth.instance.currentUser.getIdToken(false);
    final response = await http.post(url,
        headers: {
          HttpHeaders.authorizationHeader: 'Bearer $token',
          HttpHeaders.contentTypeHeader: 'audio/wave'
        },
        body: (voice64));

    if (response.statusCode == 201 || response.statusCode == 200) {
      print('Voice commands processed successfully');
      print(response.body);
      if (response.body.toString().contains('charity')) {
        print('Donation response');
        return DonationResponse.fromJson(jsonDecode(response.body));
      }
      return VoicePayResponse.fromJson(jsonDecode(response.body.toString()));
    } else {
      print('Error occurred');
      print(response.statusCode);
      print(response.body.toString());
      throw Exception(response.body.toString());
    }
  }

  static Future<dynamic> verifyVoice(
      String url, String token, List<int> voice64, File file) async {
    print('voice verify Received bytes are ${voice64.length}');
    token = await FirebaseAuth.instance.currentUser.getIdToken(false);
    // var headers = {
    //   'Authorization': 'Bearer $token',
    //   'Content-Type': 'audio/wave'
    // };
    // var request = http.Request(
    //     'POST',
    //     Uri.parse(
    //         'https://us-central1-paytalk-429bf.cloudfunctions.net/app/speaker/independent/verify-profile'));
    // request.bodyBytes = (voice64);

    // request.headers.addAll(headers);

    // http.StreamedResponse response = await request.send();

    // if (response.statusCode == 200) {
    //   print(await response.stream.bytesToString());
    // } else {
    //   print(response.reasonPhrase);
    // }
    final response = await http.post(url,
        headers: {
          HttpHeaders.authorizationHeader: 'Bearer $token',
          HttpHeaders.contentTypeHeader: 'audio/wave'
        },
        body: (voice64));

    // Dio dio = new Dio();
    // dio.options.headers["Authorization"] = "Bearer ${token}";
    // dio.options.headers['Content-Type'] = 'audio/wave';

    // // FormData formData =
    // //     FormData.fromMap({"audio": await MultipartFile.fromFile(file.path)});
    // final response = await dio.post(url, data: voice64);

    if (response.statusCode == 201 || response.statusCode == 200) {
      print('Voice verification processed successfully');
      print((response.body));
    } else {
      print('Error occurred');
      print(response.statusCode);
      print(response.body.toString());

      // throw Exception(response.body.toString());
    }

    return (response.body);
  }

  static Future<Fulfillment> sendGoogleSpeech(String url, String token,
      List<int> voice64, File file, String sessionId) async {
    print('Received bytes are ${voice64.length}');
    token = await FirebaseAuth.instance.currentUser.getIdToken(false);
    Dio dio = new Dio();
    FormData formData = FormData.fromMap({
      "languageCode": "en-NG",
      "audio": await MultipartFile.fromFile(file.path)
    });
    final response = await dio.post(url, data: formData);

    // final response = await http.post(url,
    //     headers: {
    //       HttpHeaders.authorizationHeader: 'Bearer $token',
    //       HttpHeaders.contentTypeHeader: 'audio/wave'
    //     },
    //     body: (voice64));

    if (response.statusCode == 201 || response.statusCode == 200) {
      print('Text interpreted and processed successfully as ${response.data}');

      //now send to dialogflow
      var query = {
        "sessionId": "$sessionId",
        "queryInput": {
          "text": {
            "text": "${response.data.toString()}",
            "languageCode": "en-US"
          }
        }
      };

      var result = await getDialogResponse(
          query, AppStrings.dialogFlowUrl, response.data.toString());

      //now return dialogflow object params

      return result;
    } else {
      print('Error occurred');
      print(response.statusCode);
      print(response.data.toString());
      throw Exception(response.data.toString());
    }
  }

  static Future<dynamic> speechPayment(
      String url, String token, String text) async {
    token = await FirebaseAuth.instance.currentUser.getIdToken(false);
    final response = await http.post(url,
        headers: {
          HttpHeaders.authorizationHeader: 'Bearer $token',
          HttpHeaders.contentTypeHeader: 'application/json'
        },
        body: jsonEncode({"command": text}));

    if (response.statusCode == 201 || response.statusCode == 200) {
      print('Voice commands processed successfully');
      print(response.body);
      if (response.body.toString().contains('charity')) {
        print('Donation response');
        // return TransferRecipient(
        //     command:
        //         DonationResponse.fromJson(jsonDecode(response.body)).command,
        //     donationResponse:
        //         DonationResponse.fromJson(jsonDecode(response.body)));
        return DonationResponse.fromJson(jsonDecode(response.body));
      }

      // return TransferRecipient(
      //     command: VoicePayResponse.fromJson(jsonDecode(response.body)).command,
      //     merchantResponse:
      //         VoicePayResponse.fromJson(jsonDecode(response.body)));
      return VoicePayResponse.fromJson(jsonDecode(response.body.toString()));
    } else {
      print('Error occurred');
      print(response.statusCode);
      print(response.body.toString());
      throw Exception(response.body.toString());
    }
  }

  static Future<Fulfillment> getDialogResponse(
      Map<String, dynamic> query, String url, String text) async {
    final response = await http.post(url,
        headers: {
          // HttpHeaders.authorizationHeader: 'Bearer $token',
          HttpHeaders.contentTypeHeader: 'application/json'
        },
        body: jsonEncode(query));

    if (response.statusCode == 200) {
      var result = jsonDecode(response.body);
      print('Fullfilment response recieved \n ${response.body}');

      bool allRequiredParamsPresent = result['allRequiredParamsPresent'];
      String intent = result['intent']['displayName'];

      if (allRequiredParamsPresent) {
        switch (intent) {
          case 'bbn-voting':
            {
              String userActions =
                  result["parameters"]["fields"]["user_actions"]["stringValue"];
              String candidate = result["parameters"]["fields"]
                  ["bbn-contestant"]["stringValue"];

              return Fulfillment(
                allRequiredParamsPresent: allRequiredParamsPresent,
                candidate: candidate,
                isPurchase: false,
                isFlight: false,
                isVote: true,
                gameShow: 'bbn',
              );
            }
            break;
          case 'buy-flights':
            {
              String userActions =
                  result["parameters"]["fields"]["user_actions"]["stringValue"];
              String geoCity =
                  result["parameters"]["fields"]["geo-city"]["stringValue"];

              String geoCity2 =
                  result["parameters"]["fields"]["geo-city1"]["stringValue"];

              String assets =
                  result["parameters"]["fields"]["assets"]["stringValue"];

              return Fulfillment(
                allRequiredParamsPresent: allRequiredParamsPresent,
                startCity: geoCity,
                destinationCity: geoCity2,
                isPurchase: false,
                isVote: false,
                isFlight: true,
              );
            }
            break;
          case 'buy-shares':
            {
              //are we buying shares?
              String userActions =
                  result["parameters"]["fields"]["user_actions"]["stringValue"];
              if (userActions == 'buy') {
                //this is a purchase action
                String assets =
                    result["parameters"]["fields"]["assets"]["stringValue"];
                switch (assets) {
                  case 'shares':
                    {
                      //now get params
                      num numberValue = result["parameters"]["fields"]["number"]
                          ["numberValue"];
                      String stock = result["parameters"]["fields"]["stocks"]
                          ["stringValue"];
                      return Fulfillment(
                          allRequiredParamsPresent: allRequiredParamsPresent,
                          stock: stock,
                          isPurchase: true,
                          isVote: false,
                          isFlight: false,
                          number: numberValue,
                          asset: assets);
                    }
                    break;
                  case 'airtime':
                    {}
                    break;
                }
              } else {
                //this is something else
                return null;
              }
            }
            break;
          default:
            {
              String action =
                  result["parameters"]["fields"]["action"]["stringValue"];
              if (action != null) {
                String languageCode = result['languageCode'];

                num amount = result["parameters"]["fields"]["currency"]
                    ["structValue"]["fields"]["amount"]["numberValue"];
                String currency = result["parameters"]["fields"]["currency"]
                    ["structValue"]["fields"]["currency"]["stringValue"];

                var recipient = result["parameters"]["fields"]["recipient"]
                    ["structValue"]["fields"]["name"]["stringValue"];

                String interval = result["parameters"]["fields"]
                    ["pay_intervals"]["stringValue"];

                print(
                    'Amount is $amount, Currency is $currency, Recipient is $recipient, interval is $interval');
                return Fulfillment(
                    languageCode: languageCode,
                    amount: amount,
                    payInterval: interval,
                    action: action,
                    text: text, //this is the translated text from speech
                    currency: currency,
                    allRequiredParamsPresent: allRequiredParamsPresent,
                    isPurchase: false,
                    isVote: false,
                    recipient: recipient);
              }
            }
        }
        //now check if this action is a purchase

      } else {
        print(
            "missing details: ${result['fulfillmentMessages'][0]['text']['text'][0]}");
        return Fulfillment(
            allRequiredParamsPresent: allRequiredParamsPresent,
            text: result['fulfillmentMessages'][0]['text']['text'][0]);
      }

      // return Fulfillment(amount: (result[]));
    } else {
      //throw error message
      print('Error occurred');
      print(response.statusCode);
      print(response.body.toString());
      throw Exception(response.body.toString());
    }
  }

  static Future<Recipients> getRecipients(String url, String keyword,
      num amount, String currency, String action) async {
    final response = await http.post(url,
        headers: {
          // HttpHeaders.authorizationHeader: 'Bearer $token',
          HttpHeaders.contentTypeHeader: 'application/json'
        },
        body: jsonEncode({'name': '${keyword.toLowerCase()}'}));

    if (response.statusCode == 200) {
      print('Keyword is $keyword and search results are ${response.body}');
      return Recipients.fromJson(
          jsonDecode(response.body), amount, currency, action);
    } else {
      print(response.statusCode);
      print(response.body.toString());
      throw Exception(response.body.toString());
    }
  }
}
